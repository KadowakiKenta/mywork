#include <time.h>
#include "SplashScene.h"
#include "Engine/PictDisplay.h"
#include "Engine/Image.h"


//コンストラクタ
//clock()関数はプログラムが始まってから何ミリ秒経ったか返す
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene")	//親のコンストラクタを引数ありで呼ぶ
	,alphaValue_(0), state_(STATE_FRONT), startTime_(NULL), endTime_(NULL)
{
}

//初期化
void SplashScene::Initialize()
{
	//ファイル名
	const std::string fileName[MAX]{
		"data/SchoolLogoFront.png",
		"data/SchoolLogoBack.png",
		"data/TeamLogo.png"
	};

	//インスタンス作成
	for (int i = 0; i < MAX; i++)
	{
		pPictArray_[i] = CreateGameObject<PictDisplay>(this, fileName[i]);
	}

	//初期位置変更
	pPictArray_[SCHOOL_LOGO_FRONT]->SetPosition(D3DXVECTOR3(-1100, ORIGIN, ORIGIN));
	pPictArray_[SCHOOL_LOGO_BACK]->SetPosition(D3DXVECTOR3(700, ORIGIN, ORIGIN));

	//アルファ値を0にする
	pPictArray_[TEAM_LOGO]->SetAlphaValue(alphaValue_);

}

//更新
void SplashScene::Update()
{
	//二つのロゴの現在位置を取得
	D3DXVECTOR3 frontPos, backPos;
	LogoPosition(frontPos, backPos);

	//状態の種類
	switch (state_)
	{
	//最初に「東北電子」のロゴを動かす
	case STATE_FRONT:
		//「東北電子」のロゴ移動
		FrontLogoMove(frontPos);
		break;

	//次に「専門学校」のロゴを動かす
	case STATE_BACK:
		//「専門学校」のロゴ移動
		BackLogoMove(backPos);
		break;

	//ロゴの合体後のタイム計測
	case STATE_UNION:
		//二秒間のタイム計測
		TwoSecondTime();
		break;

	//チームロゴが明るくなっていく
	case STATE_TEAM_LOGO_LIGHT:
		//チームロゴが明るくなる処理
		TeamLogoLight();
		break;

	//チームロゴが暗くなる
	case STATE_TEAM_LOGO_DARK:
		//チームロゴが暗くなる処理
		TeamLogoDark();
		break;
	}
	
	//エンターキーでタイトル画面
	TitleAtProcess();
}

//エンターキーでタイトル画面
void SplashScene::TitleAtProcess()
{
	if (Input::IsKeyDown(DIK_RETURN) ||
		//パッド操作　BACKボタン
		Input::IsPadButtonDown(XINPUT_GAMEPAD_START, 0) ||
		Input::IsPadButtonDown(XINPUT_GAMEPAD_START, 1))
	{
		//「東北電子」ロゴ状態時
		if (state_ == STATE_FRONT || state_ == STATE_BACK || state_ == STATE_UNION)
		{
			//そのロゴを移動させる
			pPictArray_[SCHOOL_LOGO_FRONT]->SetPosition(D3DXVECTOR3(-2000.0f, -2000.0f, ORIGIN));
			pPictArray_[SCHOOL_LOGO_BACK]->SetPosition(D3DXVECTOR3(-2000.0f, -2000.0f, ORIGIN));

			//タイムリセット。そして状態をチームロゴへ
			startTime_ = NULL;
			state_ = STATE_TEAM_LOGO_LIGHT;
		}
		//チームロゴ状態時
		else
		{
			//タイトルシーンへ
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_TITLE);
		}
	}
}

//チームロゴが暗くなる処理
void SplashScene::TeamLogoDark()
{
	//スタートタイムだけ更新は一回だけ
	if (startTime_ == NULL)
	{
		time(&startTime_);
	}
	time(&endTime_);

	//１秒後に暗くなる
	if (endTime_ - startTime_ >= 2.0f)
	{
		alphaValue_ -= 0.02f;
		pPictArray_[TEAM_LOGO]->SetAlphaValue(alphaValue_);
	}

	//最大まで暗くなったらタイトル画面へ移行
	if (alphaValue_ < 0)
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//チームロゴが明るくなる処理
void SplashScene::TeamLogoLight()
{
	alphaValue_ += 0.02f;
	pPictArray_[TEAM_LOGO]->SetAlphaValue(alphaValue_);

	//MAXの明るさまで来たら暗くなる
	if (alphaValue_ >= 1)
	{
		state_ = STATE_TEAM_LOGO_DARK;
	}
}

//二秒間のタイム計測
void SplashScene::TwoSecondTime()
{
	//スタートタイムだけ更新は一回だけ
	if (startTime_ == NULL)
	{
		time(&startTime_);
	}
	time(&endTime_);

	//二秒たったらチームロゴを表示
	if (endTime_ - startTime_ >= 3.0f)
	{
		pPictArray_[SCHOOL_LOGO_FRONT]->SetPosition(D3DXVECTOR3(-2000.0f, -2000.0f, ORIGIN));
		pPictArray_[SCHOOL_LOGO_BACK]->SetPosition(D3DXVECTOR3(-2000.0f, -2000.0f, ORIGIN));

		startTime_ = NULL;
		state_ = STATE_TEAM_LOGO_LIGHT;
	}
}

//「専門学校」のロゴ移動
void SplashScene::BackLogoMove(D3DXVECTOR3 &backPos)
{
	backPos.x -= 10.0f;
	pPictArray_[SCHOOL_LOGO_BACK]->SetPosition(D3DXVECTOR3(backPos.x, backPos.y, backPos.z));

	//一定位置まで行ったらタイムの計測開始
	if (backPos.x <= 0)
	{
		state_ = STATE_UNION;
	}
}

//「東北電子」のロゴ移動
void SplashScene::FrontLogoMove(D3DXVECTOR3 &frontPos)
{
	frontPos.x += 10.0f;
	pPictArray_[SCHOOL_LOGO_FRONT]->SetPosition(D3DXVECTOR3(frontPos.x, frontPos.y, frontPos.z));

	//一定位置まで動かしたら「専門学校」のロゴを動かす状態にする
	if (frontPos.x >= 0)
	{
		state_ = STATE_BACK;
	}
}

//二つのロゴの現在位置を取得
void SplashScene::LogoPosition(D3DXVECTOR3& frontPos, D3DXVECTOR3& backPos)
{
	frontPos = pPictArray_[SCHOOL_LOGO_FRONT]->GetPosition();
	backPos = pPictArray_[SCHOOL_LOGO_BACK]->GetPosition();
}

//描画
void SplashScene::Draw()
{
}

//開放
void SplashScene::Release()
{
}