#pragma once
#include "Engine/IGameObject.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Engine/Input.h"

//◆◆◆を管理するクラス
class Player : public IGameObject
{

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;
};
