#pragma once
#include "Engine/IGameObject.h"
#include "Character.h"
#include "Engine/Camera.h"

class PictDisplay;
//プレイヤーキャラクターを管理するクラス
class PlayerCharacter : public Character
{
	int hPict_;    //画像番号
	//enum PICTURE {
	//	HP,
	//	MAX
	//};
	//PictDisplay* pPictArray_[MAX];

public:
	//コンストラクタ
	PlayerCharacter(IGameObject* parent);

	//デストラクタ
	~PlayerCharacter();

	//初期化
	void Initialize() override;

	//更新
	//void Update() override;

	////描画
	//void Draw() override;
	void ChildDraw() override;

	////開放
	//void Release() override;

	//void OnCollision(IGameObject* pTarget) override;

	void PunchCommand() override;		//パンチを発動する条件　プレイヤーならキーボードでの処理になるしNPCならAIが処理する
	void StrongPunchCommand() override;	//強パンチを発動する条件　パンチ同様
	void CounterCommand() override;		//カウンターを発動する条件　パンチ同様
	void StanceChangeCommand() override;		//構え変更条件　パンチ同様
	void MoveCommand() override;				//移動　パンチ同様

	int Gethp()
	{
		return hp_;
	}

};