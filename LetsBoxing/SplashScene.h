#pragma once
#include "Engine/Global.h"

//クラスのプロトタイプ
class PictDisplay;

//スプラッシュシーンを管理するクラス
class SplashScene : public IGameObject
{
	//ロゴ画像
	enum PICTURE {
		SCHOOL_LOGO_FRONT,	//「東北電子」
		SCHOOL_LOGO_BACK,	//「専門学校」
		TEAM_LOGO,			//アフロボンバーのチームロゴ
		MAX
	};
	PictDisplay* pPictArray_[MAX]; //画像一覧

	//ロゴの状態
	enum STATE {
		STATE_FRONT,			//「東北電子」ロゴ移動中
		STATE_BACK,				//「専門学校」ロゴ移動中
		STATE_UNION,			//合体後の時間計測
		STATE_TEAM_LOGO_LIGHT,	//チームロゴが明るく登場
		STATE_TEAM_LOGO_DARK	//チームロゴが暗く登場
	} state_;

	float alphaValue_;	//画像の透明度
	time_t startTime_;	//ロゴ合体後から始まった時間
	time_t endTime_;	//ロゴ合体後、終わった時間


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SplashScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//エンターキーでタイトル画面
	void TitleAtProcess();

	//チームロゴが暗くなる処理
	void TeamLogoDark();

	//チームロゴが明るくなる処理
	void TeamLogoLight();

	//二秒間のタイム計測
	void TwoSecondTime();

	//「専門学校」のロゴ移動
	void BackLogoMove(D3DXVECTOR3 &backPos);

	//「東北電子」のロゴ移動
	void FrontLogoMove(D3DXVECTOR3 &frontPos);

	//二つのロゴの現在位置を取得
	void LogoPosition(D3DXVECTOR3& frontPos, D3DXVECTOR3& backPos);

	//描画
	void Draw() override;

	//開放
	void Release() override;

};