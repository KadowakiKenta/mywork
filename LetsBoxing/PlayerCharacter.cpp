#include "PlayerCharacter.h"
#include "Engine/Input.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"
#include "Engine/Direct3D.h"
#include "Engine/Model.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "PlayScene.h"
#include "NonPlayerCharacter.h"
#include "Engine/Audio.h"

//コンストラクタ
PlayerCharacter::PlayerCharacter(IGameObject * parent)
	:Character(parent, "PlayerCharacter"), hPict_(-1)
{
}

//デストラクタ
PlayerCharacter::~PlayerCharacter()
{
}

//初期化
void PlayerCharacter::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/HPBar.png");
	assert(hPict_ >= 0);

	//胴体コライダーを作成
	bodyCollider_ = new BoxCollider("bodyCollider", D3DXVECTOR3(0, 10/*2*/, 0), D3DXVECTOR3(4/*2*/, 10/*4*/, 2/*1*/));
	AddCollider(bodyCollider_);

	//対戦相手の名前
	opponent_ = "NonPlayerCharacter";

	position_.z *= -1;

	hp_ = HP;


	//LifeGauge* pLifeGauge = (LifeGauge*)CreateGameObject<LifeGauge>(pParent_);
	//pLifeGauge->SetPosition(D3DXVECTOR3(10, 50, 0));

}

//更新
//void PlayerCharacter::Update()
//{
//	PunchCommand();
//	StrongPunchCommand();
//	CounterCommand();
//	StanceChangeCommand();
//	MoveCommand();
//
//	MovementRestrictions();
//}

////描画
//void PlayerCharacter::Draw()
//{
//}

void PlayerCharacter::ChildDraw()
{
	//pPictArray_[HP]->SetScale(D3DXVECTOR3((hp_ / 10), 1, 1));

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, 0, 0, 0);

	D3DXMATRIX s;
	D3DXMatrixScaling(&s, ((float)hp_ / HP), 1, 1);

	m = s * m;

	if (hp_ > HP_YELLOW)
	{
		Image::SetMatrix(hPict_, m);
		Image::Draw(hPict_, D3DXCOLOR(0, 0.6f, 0.9f, 1));
	}
	else if (hp_ > HP_RED)
	{
		Image::SetMatrix(hPict_, m);
		Image::Draw(hPict_, D3DXCOLOR(0.9f, 0.9f, 0, 1));
	}
	else
	{
		Image::SetMatrix(hPict_, m);
		Image::Draw(hPict_, D3DXCOLOR(1, 0, 0, 1));
	}
}

////開放
//void PlayerCharacter::Release()
//{
//}

//void PlayerCharacter::OnCollision(IGameObject * pTarget)
//{
//	SubOnCollision(pTarget);
//}

void PlayerCharacter::PunchCommand()
{
	if (Input::IsKeyDown(DIK_C))
	{
		nextBehState_ = PUNCH;
		//再生
		Audio::Play("Punch");
	}

	//パッド
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_B, 0))
	{
		nextBehState_ = PUNCH;
		//再生
		Audio::Play("Punch");
	}
}

void PlayerCharacter::StrongPunchCommand()
{
	if (Input::IsKeyDown(DIK_V))
	{
		nextBehState_ = CHARG;
		//再生
		Audio::Play("Charge");
	}

	//パッド
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_Y, 0))
	{
		nextBehState_ = CHARG;
		//再生
		Audio::Play("Charge");
	}
}

void PlayerCharacter::CounterCommand()
{
	if (Input::IsKeyDown(DIK_B))
	{
		nextBehState_ = COUNTER;
	}

	//パッド
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0))
	{
		nextBehState_ = COUNTER;
	}
}

void PlayerCharacter::StanceChangeCommand()
{
	if (Input::IsKeyDown(DIK_R))
	{
		nextStance_ = UP;
	}

	if (Input::IsKeyDown(DIK_F))
	{
		nextStance_ = DOWN;
	}

	//パッド
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_LEFT_SHOULDER, 0))
	{
		nextStance_ = DOWN;
	}
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_RIGHT_SHOULDER, 0))
	{
		nextStance_ = UP;
	}
}



void PlayerCharacter::MoveCommand()
{
	D3DXVECTOR3 move;
	//ベクトル変形させるために行列を作る
	D3DXMATRIX ym;
	D3DXMatrixRotationY(&ym, D3DXToRadian(rotate_.y));
	D3DXMATRIX xm;
	D3DXMatrixRotationX(&xm, D3DXToRadian(rotate_.x));
	D3DXMATRIX zm;
	D3DXMatrixRotationZ(&zm, D3DXToRadian(rotate_.z));
	D3DXMATRIX wm;
	wm = ym * xm * zm;

	////前回の位置を記憶しておく
	//D3DXVECTOR3 prevPos = position_;

	//移動ステートの初期
	nextMoveState_ = MOVE_STANDARD;

	//キーボード
	//移動右
	if (Input::IsKey(DIK_D))
	{
		moveFlag_ = true;
		//TPS
		//position_.x += 0.2;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(SPEED, 0, 0), &wm);
		position_ += move;

		//移動ステート変更
		nextMoveState_ = MOVE_RIGHT;
	}
	//移動左
	if (Input::IsKey(DIK_A))
	{
		moveFlag_ = true;
		//TPS
		//position_.x -= 0.2;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(-SPEED, 0, 0), &wm);
		position_ += move;

		//移動ステート変更
		nextMoveState_ = MOVE_LEFT;
	}
	//移動前
	if (Input::IsKey(DIK_W))
	{
		moveFlag_ = true;
		//TPS
		//position_.z += 0.2;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, SPEED), &wm);
		position_ += move;

		//移動ステート変更
		nextMoveState_ = MOVE_FRONT;
	}
	//移動後ろ
	if (Input::IsKey(DIK_S))
	{
		moveFlag_ = true;
		//TPS
		//position_.z -= 0.2;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, -SPEED), &wm);
		position_ += move;

		//移動ステート変更
		nextMoveState_ = MOVE_BACK;
	}

	//パッド
	//コントローラー左スティック
	{
		moveFlag_ = true;
		//TPS
		//position_.x += Input::GetPadStickL(0).x * SPEED;
		//position_.z += Input::GetPadStickL(0).y * SPEED;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(Input::GetPadStickL(0).x * SPEED, 0, 0), &wm);
		position_ += move;
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, Input::GetPadStickL(0).y * SPEED), &wm);
		position_ += move;

		//左に移動
		if (Input::GetPadStickL(0).x > 0)
		{
			//移動ステート変更
			nextMoveState_ = MOVE_LEFT;
		}
		//右に移動
		else if (Input::GetPadStickL(0).x < 0)
		{
			//移動ステート変更
			nextMoveState_ = MOVE_RIGHT;
		}
		//前に移動
		else if (Input::GetPadStickL(0).y > 0)
		{
			//移動ステート変更
			nextMoveState_ = MOVE_FRONT;
		}
		//後に移動
		else if (Input::GetPadStickL(0).y < 0)
		{
			//移動ステート変更
			nextMoveState_ = MOVE_BACK;
		}
	}

	////前回の位置と現在の位置の差分から
	////移動ベクトルを求める
	//D3DXVECTOR3 vecMove = position_ - prevPos;

	////進行方向を向ける
	//TurnForward(vecMove);

	////敵の方向を見る
	//if (enemyCharacter_ != NULL)
	//{
	//	TurnForward((enemyCharacter_->GetPosition() - position_));
	//}

	//壁にめり込まないようにする処理
	MovementRestrictions();
}


