#include <string>
#include <time.h>
#include "PlayScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "PlayScene.h"
#include "Character.h"
#include "PlayerCharacter.h"
#include "NonPlayerCharacter.h"
#include "GiveUpScene.h"
#include "ResultScene.h"
#include "Engine/Camera.h"
#include "Engine/Model.h"
#include "Engine/Direct3D.h"
#include "Engine/Audio.h"
#include "Engine/Image.h"

const float READY_CAMERA_MOVE = 0.05f;	//カウントダウン時にカメラを動かす値
const float CAMERA_INIT_Z = 9.0f;		//カメラのZ軸初期位置
const int READY_NUMBER_POS_X = 450;		//試合開始時のカウントダウン数字の横位置
const int READY_NUMBER_POS_Y = 250;		//試合開始時のカウントダウン数字の縦位置
const int CAMERA_LENGTH = 20;			//原点からカメラまでの横位置（半径）

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")	//親のコンストラクタを引数ありで呼ぶ
	, hModel_(-1), pCamera_(nullptr), winJud_(WIN_EMPTY), sceneState_(STATE_START), startCount_(START_THREE)
	, pText_(nullptr), sec_(0), tensec_(0), min_(8), frame_(0), timeFlag_(false), time_(0), angle_(0), cameraResetFlag_(false), pointPlusFlag_(false)
	, cameraPosX_(3.0f), cameraPosY_(12.5f), cameraPosZ_(-CAMERA_INIT_Z), cameraTarX_(0), cameraTarY_(12.0f), cameraTarZ_(-CAMERA_INIT_Z)

{
}

//初期化
void PlayScene::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Stage.fbx");
	assert(hModel_ >= 0);
	
	//画像データのロード
	hPict_ = Image::Load("data/KO.png");
	assert(hPict_ >= 0);

	//テキスト作成
	pText_ = new Text("ゴシック", 80);

	//タイマーとラウンド数の配列中身初期化
	TimerRoundInitialize();

	//カメラ
	pCamera_ = CreateGameObject<Camera>(this);
	pCamera_->SetPosition(D3DXVECTOR3(cameraPosX_, cameraPosY_, cameraPosZ_));
	pCamera_->SetTarget(D3DXVECTOR3(cameraTarX_, cameraTarY_, cameraTarZ_));

	//プレイヤーたち
	pPlayerCharacter_ = CreateGameObject<PlayerCharacter>(this);
	pNonPlayerCharacter_ = CreateGameObject<NonPlayerCharacter>(this);

	//敵ポインタ代入
	pPlayerCharacter_->SetEnemyCharacter(pNonPlayerCharacter_);
	pNonPlayerCharacter_->SetEnemyCharacter(pPlayerCharacter_);

	//再生
	Audio::Play("BGM");
}

//タイマーとラウンド数の配列中身初期化
void PlayScene::TimerRoundInitialize()
{
	//ラウンド数
	round_[0] = "1";
	round_[1] = "2";
	round_[2] = "3";

	//取ったポイント
	point_[0] = "0";
	point_[1] = "1";
	point_[2] = "2";

	//秒
	second_[0] = "0";
	second_[1] = "9";
	second_[2] = "8";
	second_[3] = "7";
	second_[4] = "6";
	second_[5] = "5";
	second_[6] = "4";
	second_[7] = "3";
	second_[8] = "2";
	second_[9] = "1";
	second_[10] = "0";

	//分
	minute_[0] = "0";
	minute_[1] = "9";
	minute_[2] = "8";
	minute_[3] = "7";
	minute_[4] = "6";
	minute_[5] = "5";
	minute_[6] = "4";
	minute_[7] = "3";
	minute_[8] = "2";
	minute_[9] = "1";
	minute_[10] = "0";
}

//更新
void PlayScene::Update()
{
	//シーンの状態で処理が変わる
	switch (sceneState_)
	{
	case STATE_START:		//レディーゴー！

		//クロック関数を使ってタイム変数を初期化する
		TimeClockReset();

		//秒数によってカメラの位置が変わる
		StartCameraMove();
		break;

	case STATE_PLAY:		//試合中！

		//勝ったときの処理
		WinProcess();
			   		
		//Escキーでギブアップ画面
		EscGiveUp();

		//タイマーのカウント
		timecnt();
		break;

	case STATE_GIVEUP_SCENE: {	//ギブアップする？

		//ギブアップシーンが存在するか確かめる用
		GiveUpScene* pGiveUpScene = (GiveUpScene*)FindObject("GiveUpScene");

		//一度だけカメラの位置をリセット
		CameraInitlaize();

		//カメラを原点を主軸に回転させる
		CircleRotate();

		//ギブアップシーンがなくなったら
		if (pGiveUpScene == nullptr)
		{
			//試合再開！
			sceneState_ = STATE_PLAY;
		}
	}
		break;

	case STATE_RESULT_SCENE:	//リザルトシーン表示中
		//何もしない
		break;

	case STATE_KO:				//KO!

		//予めカメラの位置を移動
		pCamera_->SetPosition(D3DXVECTOR3(0, 13, -15));
		pCamera_->SetTarget(D3DXVECTOR3(0, 13, 0));
		
		//停止
		Audio::Stop("BGM");

		//ポイントがプラスされていない時
		if (!pointPlusFlag_)
		{
			//念のため両者画面外へ
			pPlayerCharacter_->SetPosition(D3DXVECTOR3(200, 0, 0));
			pNonPlayerCharacter_->SetPosition(D3DXVECTOR3(200, 0, 0));

			//立ち姿
			pPlayerCharacter_->SetMoveState(MOVE_STANDARD);
			pNonPlayerCharacter_->SetMoveState(MOVE_STANDARD);
		
			//どちらも消えている場合（引き分け）
			if (pPlayerCharacter_->GetHp() <= 0 && pNonPlayerCharacter_->GetHp() <= 0)
			{
				//引き分けで勝者無し！
				winJud_ = WIN_EMPTY;
			}
			//2Pが消えた（1P勝利）
			else if (pNonPlayerCharacter_->GetHp() <= 0 || winJud_ == WIN_PLAYER_ONE)
			{
				//1Pの勝ち！
				winJud_ = WIN_PLAYER_ONE;
			
				//勝者はカメラ前へ
				pPlayerCharacter_->SetPosition(D3DXVECTOR3(5, POSITION_Y, 0));
				pPlayerCharacter_->SetRotate(D3DXVECTOR3(0, 180, 0));

				//ラウンド取ったぞアニメーション
				pPlayerCharacter_->SetBehaviorState(ROUND_GET);
				
				//1Pに1ポイント！
				g.playerOneWinNum++;
			}
			//1Pが消えた（2P勝利）
			else if (pPlayerCharacter_->GetHp() <= 0 || winJud_ == WIN_PLAYER_TWO)
			{
				//2Pの勝ち！
				winJud_ = WIN_PLAYER_TWO;

				//勝者はカメラ前へ
				pNonPlayerCharacter_->SetPosition(D3DXVECTOR3(5, POSITION_Y, 0));
				pNonPlayerCharacter_->SetRotate(D3DXVECTOR3(0, 180, 0));

				//ラウンド取ったぞアニメーション
				pNonPlayerCharacter_->SetBehaviorState(ROUND_GET);

				//2Pに1ポイント！
				g.playerTwoWinNum++;
			}

			//ポイントのプラス完了！もうここは通りませんよ！
			pointPlusFlag_ = true;
		}

		//クロック関数を使ってタイム変数を初期化する
		TimeClockReset();

		//1.5秒くらい経った時
		if (clock() - time_ >= 1500)
		{
			//タイム変数とタイムフラグを一度初期化
			time_ = NULL;
			timeFlag_ = false;

			//1Pが2ポイント先取！
			if (g.playerOneWinNum >= 2)
			{
				//勝者は真ん中！
				pPlayerCharacter_->SetPosition(D3DXVECTOR3(0, POSITION_Y, 0));
				pPlayerCharacter_->SetRotate(D3DXVECTOR3(0, 180, 0));

				//ゲームに勝ったぞアニメーション
				pPlayerCharacter_->SetBehaviorState(GAME_WIN);

				//リザルト画面作成
				ResultScene* pResultScene = CreateGameObject<ResultScene>(pParent_);
				pResultScene->SetWinFlag(true);
				
				//リザルトシーンへ！
				sceneState_ = STATE_RESULT_SCENE;

			}
			//2Pが2ポイント先取！
			else if (g.playerTwoWinNum >= 2)
			{
				//勝者は真ん中！
				pNonPlayerCharacter_->SetPosition(D3DXVECTOR3(0, POSITION_Y, 0));
				pNonPlayerCharacter_->SetRotate(D3DXVECTOR3(0, 180, 0));

				//ゲームに勝ったぞアニメーション
				pNonPlayerCharacter_->SetBehaviorState(GAME_WIN);

				//リザルト画面作成
				ResultScene* pResultScene = CreateGameObject<ResultScene>(pParent_);
				pResultScene->SetWinFlag(false);

				//リザルトシーンへ！
				sceneState_ = STATE_RESULT_SCENE;
			}
			else
			{
				//Winに移行
				sceneState_ = STATE_JUDGMENT_WIN;
			}
		}
		break;
	
	case STATE_JUDGMENT_WIN:	//Win!

		//クロック関数を使ってタイム変数を初期化する
		TimeClockReset();

		//1.5秒くらい経った時
		if (clock() - time_ >= 1500)
		{
			//シーンをリロード
			SceneManager* pSceneManager;
			pSceneManager->ReloadScene();
		}
		break;
	}
}

//一度だけカメラの位置をリセット
void PlayScene::CameraInitlaize()
{
	//まだリセットされてない時
	if (!cameraResetFlag_)
	{
		//カメラの位置、向きをそれぞれ設定
		cameraPosX_ = CAMERA_LENGTH;
		cameraPosY_ = 15;
		cameraPosZ_ = 0;
		cameraTarX_ = 0;
		cameraTarY_ = 4;
		cameraPosZ_ = 0;
		pCamera_->SetPosition(D3DXVECTOR3(cameraPosX_, cameraPosY_, cameraPosZ_));
		pCamera_->SetTarget(D3DXVECTOR3(cameraTarX_, cameraTarY_, cameraTarZ_));
		
		//リセットしたよ！もう通らないよ！
		cameraResetFlag_ = true;
	}
}

//Escキーでギブアップ画面
void PlayScene::EscGiveUp()
{
	//ESCキーでギブアップ画面
	if (Input::IsKeyDown(DIK_ESCAPE) ||
		//パッド操作　BACKボタン
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 0) ||
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 1))
	{
		CreateGameObject<GiveUpScene>(pParent_);

		//状態をギブアップのものに変える
		sceneState_ = STATE_GIVEUP_SCENE;
	}
}

//勝ったときの処理
void PlayScene::WinProcess()
{
	//どちらかが消えた時
	if (pPlayerCharacter_->GetHp() <= 0 || pNonPlayerCharacter_->GetHp() <= 0)
	{
		//KO!
		sceneState_ = STATE_KO;
	}
}

//クロック関数を使ってタイム変数を初期化する
void PlayScene::TimeClockReset()
{
	//リセットされてないなら
	if (!timeFlag_)
	{
		//タイムをリセット
		time_ = clock();
		//フラグをオンで二回目の初期化をしない
		timeFlag_ = true;
	}
}

//秒数によってカメラの位置が変わる
void PlayScene::StartCameraMove()
{
	//カウントの状態によってカメラを移動させる
	switch (startCount_)
	{
	case START_THREE:	//3...
		//カメラ移動
		cameraPosZ_ += READY_CAMERA_MOVE;
		cameraTarZ_ += READY_CAMERA_MOVE;

		//1秒くらい経ったら
		if (clock() - time_ >= 1000)
		{
			//カメラの位置をリセットして次の状態にする
			cameraPosZ_ = CAMERA_INIT_Z;
			cameraTarZ_ = CAMERA_INIT_Z;
			startCount_ = START_TWO;
		}
		break;

	case START_TWO:		//2...
		//カメラ移動
		cameraPosZ_ -= READY_CAMERA_MOVE;
		cameraTarZ_ -= READY_CAMERA_MOVE;
		
		//2秒くらい経ったら
		if (clock() - time_ >= 2000)
		{
			//カメラの位置をリセットして次の状態にする
			cameraPosZ_ = 0;
			cameraTarZ_ = 0;
			cameraPosX_ = 10.0f;
			startCount_ = START_ONE;
		}
		break;

	case START_ONE:		//1...
		//カメラ移動
		cameraPosX_ += 0.5f;

		//3秒くらい経ったら
		if (clock() - time_ >= 3000)
		{
			//タイム変数とフラグをリセットして、試合開始ー！
			time_ = NULL;
			timeFlag_ = false;
			startCount_ = START_ZERO;
			sceneState_ = STATE_PLAY;
		}
		break;
	}
	//カメラの位置をセット
	pCamera_->SetPosition(D3DXVECTOR3(cameraPosX_, cameraPosY_, cameraPosZ_));
	pCamera_->SetTarget(D3DXVECTOR3(cameraTarX_, cameraTarY_, cameraTarZ_));
}

//描画
void PlayScene::Draw()
{
	//シーンの状態で処理が変わる
	switch (sceneState_)
	{
	case STATE_START:	//レディーゴー！

		//現在のラウンド数を表示
		pText_->Draw(g.screenWidth/5, 160, round_[g.playerOneWinNum + g.playerTwoWinNum] + "ROUND  READY..." );
		
		//0秒くらい経ったら
		if (clock() - time_ <= 1000)
		{
			//3...
			pText_->Draw(READY_NUMBER_POS_X, READY_NUMBER_POS_Y, "３");
		}
		//1秒くらい経ったら
		else if (clock() - time_ <= 2000)
		{
			//2...
			pText_->Draw(READY_NUMBER_POS_X, READY_NUMBER_POS_Y, "２");
		}
		//2秒くらい経ったら
		else if (clock() - time_ <= 3000)
		{
			//1...
			pText_->Draw(READY_NUMBER_POS_X, READY_NUMBER_POS_Y, "１");
		}
		//それ以降
		else
		{
			//0...
			pText_->Draw(READY_NUMBER_POS_X, READY_NUMBER_POS_Y, "０");
		}
		break;

	case STATE_PLAY:	//試合中

		//第一、第二引数は位置情報,ラウンドの表示
		pText_->Draw(450, 160, round_[g.playerOneWinNum + g.playerTwoWinNum] + "/" + "3");
		//勝利ポイントの表示
		pText_->Draw(20, 70, point_[g.playerOneWinNum]);
		//敗北ポイントの表示
		pText_->Draw(900, 70, point_[g.playerTwoWinNum]);
		//第一、第二引数は位置情報
		pText_->Draw(430, 60, minute_[min_] + ":" + second_[tensec_] + second_[sec_]);
		break;

	case STATE_KO:		//KO!

		//KO画像を表示
		Image::SetMatrix(hPict_, worldMatrix_);
		Image::Draw(hPict_);
		break;

	case STATE_JUDGMENT_WIN:	//Win!
		//1Pの勝ち
		if (winJud_ == WIN_PLAYER_ONE)
		{
			//と表示
			pText_->Draw(g.screenWidth / 5, 160, "ROUND" + round_[g.playerOneWinNum + g.playerTwoWinNum - 1] + "\n	1PWIN");
		}
		//2Pの勝ち
		else if (winJud_ == WIN_PLAYER_TWO)
		{
			//と表示
			pText_->Draw(g.screenWidth / 5, 160, "ROUND" + round_[g.playerOneWinNum + g.playerTwoWinNum - 1] + "\n	2PWIN");
		}
		else
		{
			pText_->Draw(g.screenWidth / 5, 160, "ROUND" + round_[g.playerOneWinNum + g.playerTwoWinNum] + "\n	DRAW!");

		}
		break;

	default:	//それ以外
		//描画の処理をしない
		break;
	}
}

//開放
void PlayScene::Release()
{
	SAFE_DELETE(pText_);
}

void PlayScene::DrawSub()
{
	//プレイシーン自体描画
	Draw();

	//試合中のときに2画面にする
	if (sceneState_ == STATE_PLAY || startCount_ == START_ONE)
	{
		//左画面
		{
			//ビューポート矩形
			D3DVIEWPORT9 vp;
			vp.X = 0;
			vp.Y = 0;
			vp.Width = g.screenWidth / 2;
			vp.Height = g.screenHeight;
			vp.MinZ = 0;
			vp.MaxZ = 1;
			Direct3D::pDevice->SetViewport(&vp);

			//ビュー行列
			Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pPlayerCharacter_->GetView());

			//ステージ
			Model::SetMatrix(hModel_, worldMatrix_);
			Model::Draw(hModel_);

			//シーンのオブジェクトを表示
			for (auto it = childList_.begin(); it != childList_.end(); it++)
			{
				(*it)->DrawSub();
			}
		}

		//右画面
		{
			//ビューポート矩形
			D3DVIEWPORT9 vp;
			vp.X = g.screenWidth / 2;
			vp.Y = 0;
			vp.Width = g.screenWidth / 2;
			vp.Height = g.screenHeight;
			vp.MinZ = 0;
			vp.MaxZ = 1;
			//座標指定
			Direct3D::pDevice->SetViewport(&vp);


			//ビュー行列
			Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pNonPlayerCharacter_->GetView());

			//ステージ
			Model::SetMatrix(hModel_, worldMatrix_);
			Model::Draw(hModel_);

			//シーンのオブジェクトを表示
			for (auto it = childList_.begin(); it != childList_.end(); it++)
			{
				(*it)->DrawSub();
			}
		}

		//戻す
		{
			//ビューポート矩形
			D3DVIEWPORT9 vp;
			vp.X = 0;
			vp.Y = 0;
			vp.Width = g.screenWidth;
			vp.Height = g.screenHeight;
			vp.MinZ = 0;
			vp.MaxZ = 1;
			Direct3D::pDevice->SetViewport(&vp);
		}
	}
	//それ以外は普通
	else
	{
		//ステージ
		Model::SetMatrix(hModel_, worldMatrix_);
		Model::Draw(hModel_);

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}
	}
}

//カメラを原点を主軸に回転させる
void PlayScene::CircleRotate()
{
	//中心座標に角度と長さを使用した円の位置を加算する
	//度数法の角度を弧度法に変換
	float radius = angle_ * 3.14f / 180.0f;

	//三角関数を使用し、円の位置を割り出す。
	float addX = cos(radius) * CAMERA_LENGTH;
	float addZ = sin(radius) * CAMERA_LENGTH;

	//結果ででた位置を中心位置に加算し、それを描画位置とする
	//中心位置は0のため、ここには書いていない
	cameraPosX_ = addX;
	cameraPosZ_ = addZ;

	//Y座標（カメラの高さ）
	cameraPosY_ = 40.0f;

	//カメラにセット
	pCamera_->SetPosition(D3DXVECTOR3(cameraPosX_, cameraPosY_, cameraPosZ_));
	pCamera_->SetTarget(D3DXVECTOR3(cameraTarX_, cameraTarY_, cameraTarZ_));

	//角度更新
	angle_ += 1.0f;
}

void PlayScene::timecnt()
{
	//フレームのカウントが60でちょうど1秒
	frame_++;
	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (min_ <= 10)
		{

			//最初3:00から2:59にする
			if (tensec_ == 0 && sec_ == 0)
			{
				tensec_ = 5;
				sec_ = 0;
				min_++;
			}



			//tensec_は10の位のカウント
			if (tensec_ <= 10)
			{
				if (tensec_ == 10 && sec_ == 10)
				{
					tensec_ = 5;
					sec_ = 1;
					min_++;
				}

				//1の位のカウント
				else if (sec_ < 10)
				{
					sec_++;
				}

				else if (sec_ == 10)
				{
					sec_ = 1;
					tensec_++;
				}

			}
			//タイムが0
			if (min_ == 10 && tensec_ == 10 && sec_ == 10)
			{
				//KO!
				sceneState_ = STATE_KO;
				
				//1Pの方が体力高い
				if (pPlayerCharacter_->GetHp() > pNonPlayerCharacter_->GetHp())
				{
					//1Pの勝ち
					winJud_ = WIN_PLAYER_ONE;

				}
				//2Pの方が体力高い
				else if (pNonPlayerCharacter_->GetHp() > pPlayerCharacter_->GetHp())
				{
					//2Pの勝ち
					winJud_ = WIN_PLAYER_TWO;
				}
				//HPが同じ
				else
				{
					//引き分け
					winJud_ = WIN_EMPTY;
				}
			}
		}
	}
}
