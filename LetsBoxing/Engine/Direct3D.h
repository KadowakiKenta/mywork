#pragma once
#include "Global.h"

namespace Direct3D
{
	extern LPDIRECT3D9 pD3d;    //Direct3Dオブジェクト
	extern LPDIRECT3DDEVICE9 pDevice; //Direct3Dデバイスオブジェクト

	//Direct3Dの初期化
	//引数hWnd	ウィンドウハンドル
	void Initialize(HWND hWnd);	//初期化処理

	void BeginDraw();	//描画を始める

	void EndDraw();		//描画を終わる

	void Release();		//開放処理
}