#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "IGameObject.h"
#include "SceneManager.h"
#include "Input.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete [] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

//画像の原点　という意味
#define ORIGIN 0

//非表示
#define HIDDEN 2000.0f

//違うcppで実装しても値を保持するための構造体
struct Global
{
	//ウィンドウの背景サイズ
	double screenWidth;
	double screenHeight;

	//プレイシーンで使う勝ち数
	int playerOneWinNum = 0;
	int playerTwoWinNum = 0;

};
extern Global g;
