#pragma once
#include <xact3.h>
#include <string>

namespace Audio
{
	void Initialize();

	void LoadWaveBank(std::string wFileName);

	void LoadSoundBank(std::string sFileName);

	void Play(std::string fileName);

	void Stop(std::string fileName);

	void Release();
}