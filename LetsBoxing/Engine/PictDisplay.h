#pragma once
#include "IGameObject.h"

//画像表示を管理するクラス
class PictDisplay : public IGameObject
{
	//画像番号
	enum STATE {
		NORMAL,	//ボタン
		SELECT,	//セレクト
		MAX
	} display_;

	int hPict_[MAX];				//画像番号
	std::string fileName_;			//ファイルの名前

	D3DXMATRIX selectMatrix_;		//セレクト画像の行列
	float alphaValue_;				//画像の透明度

public:
	//コンストラクタ
	PictDisplay(IGameObject* parent, std::string fileName);

	//デストラクタ
	~PictDisplay();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//選択時の画像差し替え
	void Selection()
	{
		display_ = SELECT;
	};

	//未選択の時の画像差し替え
	void UnSelection()
	{
		display_ = NORMAL;
	};

	//アルファ値を変更させる
	//引数：alphaValue アルファ値
	void SetAlphaValue(float alphaValue)
	{
		alphaValue_ = alphaValue;
	}
};