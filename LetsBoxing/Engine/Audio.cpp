#include "Audio.h"

namespace Audio
{
	IXACT3Engine* pXactEngine;
	IXACT3WaveBank* pWaveBank;
	IXACT3SoundBank* pSoundBank;

	void Initialize()
	{
		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		XACT3CreateEngine(0, &pXactEngine);
		XACT_RUNTIME_PARAMETERS xactParam = { 0 };
		xactParam.lookAheadTime = XACT_ENGINE_LOOKAHEAD_DEFAULT;
		pXactEngine->Initialize(&xactParam);
	}


	void LoadWaveBank(std::string wFileName)
	{
		//ファイルを開く
		HANDLE hFile = CreateFile(wFileName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		//開いたファイルのサイズ
		DWORD fileSize = GetFileSize(hFile, NULL);


		//ファイルをマッピングする
		HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, fileSize, NULL);
		void* mapWaveBank;
		mapWaveBank = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);

		//WAVEバンク作成
		pXactEngine->CreateInMemoryWaveBank(mapWaveBank, fileSize, 0, 0, &pWaveBank);

		//ファイルを閉じる
		CloseHandle(hMapFile);
		CloseHandle(hFile);

	}

	void LoadSoundBank(std::string sFileName)
	{
		//ファイルを開く
		HANDLE hFile = CreateFile(sFileName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		//ファイルのサイズを調べる
		DWORD fileSize = GetFileSize(hFile, NULL);

		//ファイルの中身をいったん配列に入れる
		void* soundBankData;
		soundBankData = new BYTE[fileSize];
		DWORD byteRead;
		ReadFile(hFile, soundBankData, fileSize, &byteRead, NULL);

		//サウンドバンク作成
		pXactEngine->CreateSoundBank(soundBankData, fileSize, 0, 0, &pSoundBank);

		//ファイルを閉じる
		CloseHandle(hFile);
	}

	//再生
	void Play(std::string fileName)
	{
		XACTINDEX cueIndex = pSoundBank->GetCueIndex(fileName.c_str());
		pSoundBank->Play(cueIndex, 0, 0, NULL);
	}

	//停止
	void Stop(std::string fileName)
	{
	XACTINDEX cueIndex = pSoundBank->GetCueIndex(fileName.c_str());
	pSoundBank->Stop(cueIndex, XACT_FLAG_CUE_STOP_IMMEDIATE);
	}

	void Release()
	{
		pSoundBank->Destroy();
		pWaveBank->Destroy();
		pXactEngine->ShutDown();
		CoUninitialize();
	}

}


