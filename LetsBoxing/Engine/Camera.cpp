#include "Camera.h"
#include "Direct3D.h"

//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_(0, 0, 0)
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	D3DXMATRIX proj; //プロジェクション行列を作るための変数
	//プロジェクション行列を作るための行列 (行列を保存するための変数,画角,アスペクト比ウィンドウの比率,描画し始める位置を面にしないといけないため少し離す,どこまで表示するか)
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (FLOAT)g.screenWidth / (FLOAT)g.screenHeight, 0.5f, 1000.0f);
	//どの座標として使うかを指定
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	Transform();

	//ビュー行列
	D3DXVECTOR3 worldPosition, worldTarget;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	D3DXMATRIX view;
	//ビュー行列を作るための関数 (カメラの情報を保存する変数, &変数に入れるカメラの位置,どの「位置」見るか,カメラそのものを傾ける
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	//どの座標として使うかを指定
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}

