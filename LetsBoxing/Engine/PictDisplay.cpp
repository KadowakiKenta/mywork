#include <string>
#include "PictDisplay.h"
#include "Image.h"

const float alphaValueSpeed_ = 0.03f;

//コンストラクタ
PictDisplay::PictDisplay(IGameObject * parent, std::string fileName)
	:IGameObject(parent, "PictDisplay")
	, display_(NORMAL), fileName_(fileName), alphaValue_(1.0f)
{
	for (int i = 0; i < MAX; i++)
	{
		hPict_[i] = -1;
	}

	D3DXMatrixIdentity(&selectMatrix_);
}

//デストラクタ
PictDisplay::~PictDisplay()
{
}

//初期化
void PictDisplay::Initialize()
{
	//画像データのロード
	hPict_[NORMAL] = Image::Load(fileName_);
	assert(hPict_ >= 0);

	//セレクト画像のロード
	hPict_[SELECT] = Image::Load("Data/SELECT.png");
	assert(hPict_ >= 0);

}

//更新
void PictDisplay::Update()
{
	//セレクト画像の行列変更
	selectMatrix_ = worldMatrix_;
	D3DXMatrixTranslation(&selectMatrix_, position_.x, position_.y - 150, position_.z);
}

//描画
void PictDisplay::Draw()
{
	//ボタン画像描画
	Image::SetMatrix(hPict_[NORMAL], worldMatrix_);
	Image::Draw(hPict_[NORMAL], alphaValue_);

	//選択されてる時に表示
	if (display_ == SELECT)
	{
		Image::SetMatrix(hPict_[SELECT], selectMatrix_);
		Image::Draw(hPict_[SELECT]);
	}
}

//開放
void PictDisplay::Release()
{
}