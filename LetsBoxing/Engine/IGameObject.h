#pragma once
#include <d3dx9.h>
#include <list>
#include <string.h>

class Collider;
//class Player;	//クラスのプロトタイプ宣言 オブジェクトは作れないが戻り値やポインタとして使える

class IGameObject
{
protected:
	IGameObject* pParent_;	//親の情報
	std::list < IGameObject* > childList_;
	std::string name_;

	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	D3DXMATRIX localMatrix_;	//自分視点の行列親のworldMatrix_をかけ入れる
	D3DXMATRIX worldMatrix_;	//世界からみた行列

	//Collider* pHitCollider_;	//当たったコライダーのポインタ

	enum SIDE
	{
		ENEMY_SIDE,
		PLAYER_SIDE,
		NO_SIDE,
	}side_;

	D3DXVECTOR3 front_;

	bool dead_;

	std::list<Collider*>	ColliderList_;	//衝突判定リスト

	void Transform();

public:
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);

	//仮想デストラクタにしないと子クラスのデストラクタが呼ばれず親クラスのデストラクタが呼ばれる
	virtual ~IGameObject();

	//初期化
	virtual void Initialize() = 0;
	//更新
	virtual void Update() = 0;
	//描写
	virtual void Draw() = 0;
	//解放
	virtual void Release() = 0;

	virtual void DrawSub();

	//更新
	void UpdateSub();
	//描写
	
	//解放
	void ReleaseSub();

	void KillMe();

	//名前でオブジェクトを検索
	//引数：name 検索する名前
	//戻値：IGameObject 見つけたオブジェクトのアドレス
	IGameObject* FindGameObject(const std::string& name);

	//名前でオブジェクトを検索（シーンマネージャー直下から検索）
	IGameObject* FindObject(const std::string& name);

	//シーンマネージャーを探す
	IGameObject* FindSceneManager();
	
	//テンプレートクラスを使うことで汎用的にできる
	template<class T>
	//<>の中に書いたクラスに置き換える
	T* CreateGameObject(IGameObject* parent)	//ゲームオブジェクトをvectorに入れる
	{
		T* p = new T(parent);
		parent->PushBackChild(p);
		p->Initialize();
		
		return p;
	};

	template <class T>
	//<>の中に書いたクラスに置き換える
	//第一引数：parent	親のゲームオブジェクトを指定
	//第二引数：fileName ボタンのロゴの名前
	//戻値：T*	オブジェクト本体を返す
	T* CreateGameObject(IGameObject* parent, std::string fileName)
	{
		//テンプレートクラスはインライン関数
		//オブジェクトを動的作成
		T* p = new T(parent, fileName);

		//親に子供（自分）を追加する
		parent->PushBackChild(p);

		//オブジェクト本体を初期化する
		p->Initialize();

		return p;
	}

	void IGameObject::PushBackChild(IGameObject* pObj);

	void SetPosition(D3DXVECTOR3 position)
	{
		position_ = position;
	};

	void SetScale(D3DXVECTOR3 scale)
	{
		scale_ = scale;
	};

	void SetRotate(D3DXVECTOR3 rotate)
	{
		rotate_ = rotate;
	};

	D3DXVECTOR3 GetPosition()
	{
		return position_;
	};

	D3DXVECTOR3 GetRotate()
	{
		return rotate_;
	};

	D3DXVECTOR3 GetScale()
	{
		return scale_;
	};


	std::string GetName()
	{
		return name_;
	}

	void SetSide(SIDE side)
	{
		side_ = side;
	}

	SIDE GetSide()
	{
		return side_;
	}

	IGameObject* GetChild(int ID)
	{
		auto it = childList_.begin();
		for (int i = 0; i < ID; i++)
		{
			it++;
		}
		return (*it);
	}

	void SetFront(D3DXMATRIX f, D3DXVECTOR3 v);

	//コライダー（衝突判定）を追加する
	void AddCollider(Collider * collider);

	//何かと衝突した場合に呼ばれる（オーバーライド用）
	//引数：pTarget	衝突した相手
	virtual void OnCollision(IGameObject* pTarget) {};

	//衝突判定
	//引数：pTarget	衝突してるか調べる相手
	void Collision(IGameObject* pTarget);

	//テスト用の衝突判定枠を表示
	void CollisionDraw();

	Collider* GetColliderList(int i)
	{
		auto it = ColliderList_.begin();
		for(int j = 0; j < i; j++)
		{
			it++;
		}
		return (*it);
	}
};				 


