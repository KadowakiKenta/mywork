#pragma once
#include "Global.h"

class Quad
{

	struct Vertex
	{
		D3DXVECTOR3 pos;	//位置
		D3DXVECTOR3 normal;	//法線(面の表)
		D3DXVECTOR2 uv;		//貼り付けるテクスチャの座標
	};
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;
	LPDIRECT3DTEXTURE9 pTexture_; //テクスチャ
	D3DMATERIAL9         material_;


public:
	Quad();
	~Quad();

	void Load(const char* imagePass);
	void Draw(const D3DXMATRIX &matrix);
};