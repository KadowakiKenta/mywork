#pragma once
#include "Engine/Global.h"

class PictDisplay;

//難易度選択シーンを管理するクラス
class DifficultyScene : public IGameObject
{
	//ロゴ画像
	enum PICTURE {
		EASY_LOGO,
		NORMAL_LOGO,
		HARD_LOGO,
		MAX
	};
	PictDisplay* pPictArray_[MAX];

	//状態の種類
	enum STATE {
		STATE_EASY,		//弱い
		STATE_NORMAL,	//普通
		STATE_HARD		//強い
	} state_;

	int hPict_;					//背景画像番号	

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	DifficultyScene(IGameObject* parent);


	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};