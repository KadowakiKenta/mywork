#pragma once
#include "RacketState.h"

//下回転の状態を管理するクラス
class BackSpinState : public RacketState
{
public:
	~BackSpinState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};