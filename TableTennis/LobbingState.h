#pragma once
#include "RacketState.h"

//ロビング（打ち上げ）の状態を管理するクラス
class LobbingState : public RacketState
{
public:
	~LobbingState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};