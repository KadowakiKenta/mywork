#pragma once
#include "RefereeState.h"

//試合開始時の状態を管理するクラス
class GameStartState : public RefereeState
{
	unsigned int countTimer_;

public:
	GameStartState();
	~GameStartState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;
};