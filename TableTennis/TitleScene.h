#pragma once
#include "Engine/Global.h"

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
public:
	TitleScene(IGameObject *parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};