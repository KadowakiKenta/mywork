#include "GameFinishState.h"
#include "Engine/SceneManager.h"
#include "Referee.h"

const unsigned int GAME_START_PROCESS = 180;

GameFinishState::GameFinishState() :
	countTimer_(0)
{
}

unique_ptr<RefereeState> GameFinishState::HandleInput(Referee *thisptr) const
{
	if (countTimer_ == GAME_START_PROCESS)
	{
		thisptr->ReloadPlayScene();
	}

	return nullptr;
}

void GameFinishState::Update(Referee *thisptr)
{
	countTimer_++;
}

void GameFinishState::Enter(Referee *thisptr)
{
	thisptr->SetRefereeState(STATE_GAME_FINISH);
}