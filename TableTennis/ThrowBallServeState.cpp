#include "ThrowBallServeState.h"
#include "Racket.h"
#include "Engine/Model.h"
#include "HitBallServeState.h"

const float FIRST_ANIMATION = 60.0f;
const float LAST_ANIMATION = 69.0f;

unique_ptr<RacketState> ThrowBallServeState::HandleInput(Racket *thisptr) const
{
	if (ballHit_)
	{
		return make_unique<HitBallServeState>();
	}

	return nullptr;
}

void ThrowBallServeState::Update(Racket *thisptr)
{
	//TODO:技ごとにサーブの威力を分ける予定
	if (thisptr->ButtonTopSpin() || thisptr->ButtonBackSpin() ||
		thisptr->ButtonRightSideSpin() || thisptr->ButtonLeftSideSpin() ||
		thisptr->ButtonLobbing() || thisptr->ButtonSmash())
	{
		ballHit_ = true;
	}

	float frame = Model::GetAnimFrame(thisptr->GetModelHandle());
	if ((LAST_ANIMATION < frame) && (frame < LAST_ANIMATION + ANIM_WHITESPACE_FRAME))
	{
		Model::SetAnimFrame(thisptr->GetModelHandle(), LAST_ANIMATION, LAST_ANIMATION, ANIM_SPEED);
	}
}

void ThrowBallServeState::Enter(Racket *thisptr)
{
	Model::SetAnimFrame(thisptr->GetModelHandle(), FIRST_ANIMATION, LAST_ANIMATION + ANIM_WHITESPACE_FRAME, ANIM_SPEED);

	thisptr->BallServe();

	thisptr->SetSavePosition();
}
