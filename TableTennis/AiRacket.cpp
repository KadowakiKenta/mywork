#include <time.h>
#include <stdlib.h>
#include "PlayScene.h"
#include "AiRacket.h"
#include "UserRacket.h"
#include "Ball.h"
#include "Referee.h"

//AIをどのように作ろうか思案中
AiRacket::AiRacket(IGameObject *parent):
	Racket(parent, "AiRacket", COURT_RECEIVER)
{
}

AiRacket::~AiRacket()
{
}