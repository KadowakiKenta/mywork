#include "Referee.h"
#include <list>
#include "RefereeState.h"
#include "Engine/Text.h"
#include "Engine/Image.h"
#include "UserRacket.h"
#include "UserRacket2.h"
#include "Ball.h"
#include "GameStartState.h"

const unsigned int FINISH_POINT = 11;
const unsigned int DEUCE_POINT = 10;
const unsigned int DEUCE_DIFF_FINISH_POINT = 2;

//ここの定数はUIの位置だらけだけど
//UIクラスとして別のクラスを作ってもよかったかなぁ…。
const unsigned int PICT_CD_DRAW_TIME = 210;

const float UI_POS_ORIGIN_X = 80.0f;
const float UI_POS_ORIGIN_Y = 30.0f;
const float HYPHEN_POS_X = 105.0f;
const float HYPHEN_POS_Y = 70.0f;
const float NUMBER_POS_Y = 10.0f;
const float COUNTDOWN_POINTGET_POS_X = 120.0f;

//数字の位置調整は、割り算のため値を大きくすると左側に寄る
const unsigned int DIGIT_ONE_USER_POS_X = 8;
const unsigned int DIGIT_TWO_USER_POS_X_ONE = 4;
const unsigned int DIGIT_TWO_USER_POS_X_TWO = 9;

const float DIGIT_ONE_AI_POS_X = 1.3f;
const float DIGIT_TWO_AI_POS_X_ONE = 1.2f;
const float DIGIT_TWO_AI_POS_X_TWO = 1.45f;

const unsigned int ONE_DIGIT_BORDER = 10;

Referee::Referee(IGameObject *parent):
	IGameObject(parent, "Referee"),
	pRefereeState_(make_unique<GameStartState>()),
	toServe_(TO_SERVE_SERVER_COURT),
	refereeState_(STATE_PLAY_START),
	gameMode_(GAME_MODE_NORMAL),
	pictJudg_(PICT_JUDG_MAX),
	afterScoreState_(AFTER_SCORE_NORMAL),
	scorePlayerTo_(SCORE_PLAYER_TO_SERVER_COURT),
	serverCourtPoint_(0),
	receiverCourtPoint_(0),
	pictCD_(PICT_CD_THREE),
	countTimer_(0),
	ballKillFlag_(false)
{
	for (int init = 0; init < PICT_JUDG_MAX; init++)
	{
		hPictJdge_[init] = ASSERT_ERROR;
	}
	for (int init = 0; init < PICT_CD_MAX; init++)
	{
		hPictCountDown_[init] = ASSERT_ERROR;
	}
	for (int init = 0; init < PICT_PG_MAX; init++)
	{
		hPictPointGet_[init] = ASSERT_ERROR;
	}
	for (int init = 0; init < PICT_NUM_MAX; init++)
	{
		hPictNumber_[init] = ASSERT_ERROR;
	}
	for (int init = 0; init < PICT_PUI_MAX; init++)
	{
		hPictPlayUI_[init] = ASSERT_ERROR;
	}
}

Referee::~Referee()
{
}

void Referee::Initialize()
{
	pRacketArray_.push_back(CreateGameObject<UserRacket>(this));
	pRacketArray_.push_back(CreateGameObject<UserRacket2>(this));

	LoadToUI();
}

void Referee::Update()
{
	if (ballKillFlag_)
	{
		//多分消えてるはずだから、falseにする
		//trueのままだとボール再生成時に、すぐ消えてしまう
		ballKillFlag_ = false;
	}

	pRefereeState_->Update(this);
	HandleInput();
}

void Referee::Draw()
{
	PlayUITopDraw();

	PlayUICenterDraw();

	if (refereeState_ == STATE_GAME_FINISH)
	{
		Image::SetMatrix(hPictJdge_[pictJudg_], worldMatrix_);
		Image::Draw(hPictJdge_[pictJudg_]);
	}
}

void Referee::Release()
{
}

void Referee::LoadToUI()
{
	const string jdgeFileName[PICT_JUDG_MAX] =
	{
		"WIN",
		"LOSE"
	};
	const string countDownFileName[PICT_CD_MAX] =
	{
		"3_second",
		"2_second",
		"1_second",
		"GameStart"
	};
	const string pointGetFileName[PICT_PG_MAX] =
	{
		"PlayerPoint",
		"EnemyPoint"
	};
	const string numberFileName[PICT_NUM_MAX] =
	{
		"Zero",
		"One",
		"Two",
		"Three",
		"Four",
		"Five",
		"Six",
		"Seven",
		"Eight",
		"Nine"
	};
	const string playUIFileName[PICT_PUI_MAX] =
	{
		"Player",
		"Enemy",
		"Server",
		"Border",
		"Hyphen"
	};

	for (int load = 0; load < PICT_JUDG_MAX; load++)
	{
		hPictJdge_[load] = Image::Load("data/Image/Judgment/" + jdgeFileName[load] + ".png");
		assert(hPictJdge_[load] > ASSERT_ERROR);
	}
	for (int load = 0; load < PICT_CD_MAX; load++)
	{
		hPictCountDown_[load] = Image::Load("data/Image/CountDown/" + countDownFileName[load] + ".png");
		assert(hPictCountDown_[load] > ASSERT_ERROR);
	}
	for (int load = 0; load < PICT_PG_MAX; load++)
	{
		hPictPointGet_[load] = Image::Load("data/Image/PointGet/" + pointGetFileName[load] + ".png");
		assert(hPictPointGet_[load] > ASSERT_ERROR);
	}
	for (int load = 0; load < PICT_NUM_MAX; load++)
	{
		hPictNumber_[load] = Image::Load("data/Image/Number/" + numberFileName[load] + ".png");
		assert(hPictNumber_[load] > ASSERT_ERROR);
	}
	for (int load = 0; load < PICT_PUI_MAX; load++)
	{
		hPictPlayUI_[load] = Image::Load("data/Image/PlayUI/" + playUIFileName[load] + ".png");
		assert(hPictPlayUI_[load] > ASSERT_ERROR);
	}
}

void Referee::HandleInput()
{
	unique_ptr<RefereeState> pRefereeState = pRefereeState_->HandleInput(this);

	if (pRefereeState != nullptr)
	{
		pRefereeState_ = move(pRefereeState);

		pRefereeState_->Enter(this);
	}
}

void Referee::WinLoseChangePict()
{
	if (serverCourtPoint_ > receiverCourtPoint_)
	{
		pictJudg_ = PICT_JUDG_WIN;
	}
	else
	{
		pictJudg_ = PICT_JUDG_LOSE;
	}
}

void Referee::ResetRacketOfPosToServe()
{
	pRacketShouldHitArray_.clear();
	for (auto it = pRacketArray_.begin(); it < pRacketArray_.end(); it++)
	{
		if (toServe_ == TO_SERVE_SERVER_COURT)
		{
			if ((*it)->GetMyCourt() == COURT_SERVER)
			{
				(*it)->SetServerRacketState();
				pRacketShouldHitArray_.push_back((*it));
			}
			else
			{
				(*it)->SetReceiverRacketState();
				pRacketShouldHitArray_.push_front((*it));
			}
		}
		else if (toServe_ == TO_SERVE_RECEIVER_COURT)
		{
			if ((*it)->GetMyCourt() == COURT_RECEIVER)
			{
				(*it)->SetServerRacketState();
				pRacketShouldHitArray_.push_back((*it));
			}
			else
			{
				(*it)->SetReceiverRacketState();
				pRacketShouldHitArray_.push_front((*it));
			}
		}
	}
}

void Referee::ScorePlayerToProcess(const SCORE_PLAYER_TO scorePlayerTo)
{
	switch (scorePlayerTo)
	{
	case SCORE_PLAYER_TO_SERVER_COURT:
		serverCourtPoint_++;
		break;
	case SCORE_PLAYER_TO_RECEIVER_COURT:
		receiverCourtPoint_++;
		break;
	}

	ChangeServeOfProcess();

	//描画処理で使う為に、クラス変数に代入しておく
	scorePlayerTo_ = scorePlayerTo;

	ballKillFlag_ = true;
}

void Referee::FromFrontToBackRacketShouldHitArray()
{
	Racket* pRacket = pRacketShouldHitArray_.front();
	pRacketShouldHitArray_.pop_front();
	pRacketShouldHitArray_.push_back(pRacket);
}

void Referee::ReloadPlayScene() const
{
	SceneManager* pSceneManager = (SceneManager*)FindSceneManager();
	pSceneManager->ReloadScene();
}

void Referee::ChangeServe()
{
	if (toServe_ == TO_SERVE_SERVER_COURT)
	{
		toServe_ = TO_SERVE_RECEIVER_COURT;
	}
	else if (toServe_ == TO_SERVE_RECEIVER_COURT)
	{
		toServe_ = TO_SERVE_SERVER_COURT;
	}
}

void Referee::ChangeServeOfProcess()
{
	if (gameMode_ == GAME_MODE_NORMAL)
	{
		ServeModeNormalProcess();
	}
	else if (gameMode_ == GAME_MODE_DEUCE)
	{
		ServeModeDeuceProcess();
	}
	else
	{
		assert(false);
	}
}

void Referee::ServeModeDeuceProcess()
{
	ChangeServe();

	if ((serverCourtPoint_ - receiverCourtPoint_ == DEUCE_DIFF_FINISH_POINT) ||
		(receiverCourtPoint_ - serverCourtPoint_ == DEUCE_DIFF_FINISH_POINT))
	{
		GameFinishProcess();
	}
	else
	{
		RetryTableTennis();
	}
}

void Referee::ServeModeNormalProcess()
{
	if ((serverCourtPoint_ + receiverCourtPoint_) % 2 == 0)
	{
		ChangeServe();
	}

	if ((serverCourtPoint_ == FINISH_POINT) || (receiverCourtPoint_ == FINISH_POINT))
	{
		GameFinishProcess();
	}
	else
	{
		if ((serverCourtPoint_ == DEUCE_POINT) && (receiverCourtPoint_ == DEUCE_POINT))
		{
			gameMode_ = GAME_MODE_DEUCE;
		}

		RetryTableTennis();
	}
}

void Referee::RetryTableTennis()
{
	afterScoreState_ = AFTER_SCORE_CONTINUE;
}

void Referee::GameFinishProcess()
{
	for (auto it = pRacketArray_.begin(); it < pRacketArray_.end(); it++)
	{
		(*it)->KillMe();
	}

	afterScoreState_ = AFTER_SCORE_FINISH;

	WinLoseChangePict();
}


void Referee::PlayUICenterDraw()
{
	D3DXMATRIX countDownMat;
	D3DXMatrixTranslation(&countDownMat, ((FLOAT)g.screenWidth - Image::GetImageSizeWidth(hPictCountDown_[0])) / 2, COUNTDOWN_POINTGET_POS_X, 0);

	if (countTimer_ < PICT_CD_DRAW_TIME)
	{
		countTimer_++;

		Image::SetMatrix(hPictCountDown_[pictCD_], countDownMat);
		Image::Draw(hPictCountDown_[pictCD_]);
	}

	D3DXMATRIX pointGetMat;
	D3DXMatrixTranslation(&pointGetMat, ((FLOAT)g.screenWidth - Image::GetImageSizeWidth(hPictPointGet_[0])) / 2, COUNTDOWN_POINTGET_POS_X, 0);

	if (refereeState_ == STATE_POINT_GET)
	{
		switch (scorePlayerTo_)
		{
		case SCORE_PLAYER_TO_SERVER_COURT:
			Image::SetMatrix(hPictPointGet_[PICT_PG_USER], pointGetMat);
			Image::Draw(hPictPointGet_[PICT_PG_USER]);
			break;
		case SCORE_PLAYER_TO_RECEIVER_COURT:
			Image::SetMatrix(hPictPointGet_[PICT_PG_AI], pointGetMat);
			Image::Draw(hPictPointGet_[PICT_PG_AI]);
			break;
		}
	}
}

void Referee::PlayUITopDraw()
{
	D3DXMATRIX uiMatrix[PICT_PUI_MAX];
	float uiPosX[PICT_PUI_MAX], uiPosY[PICT_PUI_MAX];

	//位置設定
	uiPosX[PICT_PUI_PLAYER] = UI_POS_ORIGIN_X;
	uiPosY[PICT_PUI_PLAYER] = UI_POS_ORIGIN_Y;

	uiPosX[PICT_PUI_BORDER] = uiPosX[PICT_PUI_PLAYER] + Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_PLAYER]);
	uiPosY[PICT_PUI_BORDER] = UI_POS_ORIGIN_Y + HYPHEN_POS_Y;

	uiPosX[PICT_PUI_HYPHEN] = uiPosX[PICT_PUI_BORDER] + HYPHEN_POS_X;
	uiPosY[PICT_PUI_HYPHEN] = UI_POS_ORIGIN_Y + HYPHEN_POS_Y / 3;

	uiPosX[PICT_PUI_ENEMY] = uiPosX[PICT_PUI_BORDER] + Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_BORDER]);
	uiPosY[PICT_PUI_ENEMY] = UI_POS_ORIGIN_Y;

	if (toServe_ == TO_SERVE_SERVER_COURT)
	{
		uiPosX[PICT_PUI_SERVER] = uiPosX[PICT_PUI_PLAYER];
		uiPosY[PICT_PUI_SERVER] = UI_POS_ORIGIN_Y + Image::GetImageSizeHeight(hPictPlayUI_[PICT_PUI_PLAYER]);
	}
	else if (toServe_ == TO_SERVE_RECEIVER_COURT)
	{
		uiPosX[PICT_PUI_SERVER] = uiPosX[PICT_PUI_ENEMY];
		uiPosY[PICT_PUI_SERVER] = UI_POS_ORIGIN_Y + Image::GetImageSizeHeight(hPictPlayUI_[PICT_PUI_ENEMY]);
	}

	for (int draw = 0; draw < PICT_PUI_MAX; draw++)
	{
		D3DXMatrixTranslation(&uiMatrix[draw], (FLOAT)uiPosX[draw], (FLOAT)uiPosY[draw], 0);

		Image::SetMatrix(hPictPlayUI_[draw], uiMatrix[draw]);
		Image::Draw(hPictPlayUI_[draw]);
	}

	//1桁目と2桁目で処理が少し変わる
	D3DXMATRIX numMatrix[DIGIT_MAX];
	float numPosX[DIGIT_MAX], numPosY[DIGIT_MAX];

	if (serverCourtPoint_ < ONE_DIGIT_BORDER)
	{
		numPosX[DIGIT_ONE] = uiPosX[PICT_PUI_BORDER] + (Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_BORDER]) / DIGIT_ONE_USER_POS_X);
		numPosY[DIGIT_ONE] = UI_POS_ORIGIN_Y + NUMBER_POS_Y;

		D3DXMatrixTranslation(&numMatrix[DIGIT_ONE], (FLOAT)numPosX[DIGIT_ONE], (FLOAT)numPosY[DIGIT_ONE], 0);

		Image::SetMatrix(hPictNumber_[serverCourtPoint_], numMatrix[DIGIT_ONE]);
		Image::Draw(hPictNumber_[serverCourtPoint_]);

	}
	else
	{
		unsigned int userPointNum[DIGIT_MAX];
		unsigned int userPointtemp = serverCourtPoint_;

		for (int digit = 0; digit < DIGIT_MAX; digit++)
		{
			userPointNum[digit] = userPointtemp % 10;
			userPointtemp /= 10;
		}

		numPosX[DIGIT_ONE] = uiPosX[PICT_PUI_BORDER] + (Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_BORDER]) / DIGIT_TWO_USER_POS_X_ONE);
		numPosY[DIGIT_ONE] = UI_POS_ORIGIN_Y + NUMBER_POS_Y;

		numPosX[DIGIT_TWO] = uiPosX[PICT_PUI_BORDER] + (Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_BORDER]) / DIGIT_TWO_USER_POS_X_TWO);
		numPosY[DIGIT_TWO] = UI_POS_ORIGIN_Y + NUMBER_POS_Y;

		for (int draw = 0; draw < DIGIT_MAX; draw++)
		{
			D3DXMatrixTranslation(&numMatrix[draw], (FLOAT)numPosX[draw], (FLOAT)numPosY[draw], 0);

			Image::SetMatrix(hPictNumber_[userPointNum[draw]], numMatrix[draw]);
			Image::Draw(hPictNumber_[userPointNum[draw]]);
		}
	}

	if (receiverCourtPoint_ < ONE_DIGIT_BORDER)
	{
		numPosX[DIGIT_ONE] = uiPosX[PICT_PUI_BORDER] + (Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_BORDER]) / DIGIT_ONE_AI_POS_X);
		numPosY[DIGIT_ONE] = UI_POS_ORIGIN_Y + NUMBER_POS_Y;

		D3DXMatrixTranslation(&numMatrix[DIGIT_ONE], (FLOAT)numPosX[DIGIT_ONE], (FLOAT)numPosY[DIGIT_ONE], 0);

		Image::SetMatrix(hPictNumber_[receiverCourtPoint_], numMatrix[DIGIT_ONE]);
		Image::Draw(hPictNumber_[receiverCourtPoint_]);
	}
	else
	{
		unsigned int aiPointNum[DIGIT_MAX];
		unsigned int aiPointtemp = receiverCourtPoint_;

		for (int digit = 0; digit < DIGIT_MAX; digit++)
		{
			aiPointNum[digit] = aiPointtemp % 10;
			aiPointtemp /= 10;
		}

		numPosX[DIGIT_ONE] = uiPosX[PICT_PUI_BORDER] + (Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_BORDER]) / DIGIT_TWO_AI_POS_X_ONE);
		numPosY[DIGIT_ONE] = UI_POS_ORIGIN_Y + NUMBER_POS_Y;

		numPosX[DIGIT_TWO] = uiPosX[PICT_PUI_BORDER] + (Image::GetImageSizeWidth(hPictPlayUI_[PICT_PUI_BORDER]) / DIGIT_TWO_AI_POS_X_TWO);
		numPosY[DIGIT_TWO] = UI_POS_ORIGIN_Y + NUMBER_POS_Y;

		for (int draw = 0; draw < DIGIT_MAX; draw++)
		{
			D3DXMatrixTranslation(&numMatrix[draw], (FLOAT)numPosX[draw], (FLOAT)numPosY[draw], 0);

			Image::SetMatrix(hPictNumber_[aiPointNum[draw]], numMatrix[draw]);
			Image::Draw(hPictNumber_[aiPointNum[draw]]);
		}
	}
}

