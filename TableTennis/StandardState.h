#pragma once
#include "RacketState.h"

//ラリー時の構えの状態を管理するクラス
class StandardState : public RacketState
{
public:
	~StandardState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};