#include "OperationExplainScene.h"
#include "BackGround2D.h"
#include "OperationExplainLogo.h"

OperationExplainScene::OperationExplainScene(IGameObject *parent) : IGameObject(parent, "OperationExplainScene")
{
}

void OperationExplainScene::Initialize()
{
	CreateGameObject<BackGround2D>(this);
	CreateGameObject<OperationExplainLogo>(this);
}

void OperationExplainScene::Update()
{
}

void OperationExplainScene::Draw()
{
}

void OperationExplainScene::Release()
{
}