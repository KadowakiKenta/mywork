#include "RallyBoundZeroState.h"
#include "RallyBoundOneState.h"
#include "Ball.h"
#include "Referee.h"
#include "Racket.h"

unique_ptr<RefereeState> RallyBoundZeroState::HandleInput(Referee *thisptr) const
{
	const Ball* pBall = thisptr->GetBallPointer();
	if (pBall != nullptr)
	{
		if (pBall->GetIsBound())
		{
			return make_unique<RallyBoundOneState>();
		}
	}
	else
	{
		return PointGetChangeState::HandleInput(thisptr);
	}

	return nullptr;
}

void RallyBoundZeroState::Update(Referee *thisptr)
{
	std::vector<Racket*> pRacketArray = thisptr->GetRacketArray();

	//打ってはいけない場面なのに、打ってしまった人
	for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
	{
		if ((*it)->GetIsBallHitNowFrame())
		{
			if ((*it)->GetMyCourt() == COURT_SERVER)
			{
				thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_RECEIVER_COURT);
			}
			else if ((*it)->GetMyCourt() == COURT_RECEIVER)
			{
				thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_SERVER_COURT);
			}
		}
	}

	//バウンド判定
	const Ball* pBall = thisptr->GetBallPointer();
	if ((pBall != nullptr) && (pBall->GetIsBound()))
	{
		for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
		{
			if (ZeroBoundPointProcess(thisptr, it, pBall))
			{
				break;
			}
		}
	}
}

void RallyBoundZeroState::Enter(Referee *thisptr)
{
	thisptr->SetRefereeState(STATE_RALLY_BOUND_ZERO);
}

bool RallyBoundZeroState::ZeroBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it, const Ball *pBall)
{
	if ((*it)->GetIsBallHit())
	{
		if (((*it)->GetMyCourt() == COURT_SERVER) &&
			(pBall->GetCollisionBall() == COLLISION_BALL_SERVER_COURT || pBall->GetCollisionBall() == COLLISION_BALL_GROUND))
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_RECEIVER_COURT);
		}
		else if (((*it)->GetMyCourt() == COURT_RECEIVER) &&
			(pBall->GetCollisionBall() == COLLISION_BALL_RECEIVER_COURT || pBall->GetCollisionBall() == COLLISION_BALL_GROUND))
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_SERVER_COURT);
		}

		return true;
	}

	return false;
}