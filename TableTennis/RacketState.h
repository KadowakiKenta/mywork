#pragma once
#include <memory>

using namespace std;

class Racket;

const float ANIM_WHITESPACE_FRAME = 10.0f;
const float ANIM_SPEED = 1.0f;

//卓球ラケットの状態を管理するクラス
class RacketState
{
public:
	virtual ~RacketState() {};

	virtual unique_ptr<RacketState> HandleInput(Racket *thisptr) const = 0;

	virtual void Update(Racket *thisptr) = 0;

	virtual void Enter(Racket *thisptr) = 0;
};