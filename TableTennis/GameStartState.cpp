#include "GameStartState.h"
#include "Engine/SceneManager.h"
#include "Referee.h"
#include "WaitBallState.h"

const unsigned int TWO_PROCESS = 60;
const unsigned int ONE_PROCESS = 120;
const unsigned int GAME_START_PROCESS = 180;

GameStartState::GameStartState() :
	countTimer_(0)
{
}

unique_ptr<RefereeState> GameStartState::HandleInput(Referee *thisptr) const
{
	if (countTimer_ == GAME_START_PROCESS)
	{
		return make_unique<WaitBallState>();

	}

	return nullptr;
}

void GameStartState::Update(Referee *thisptr)
{
	countTimer_++;

	switch (countTimer_)
	{
	case TWO_PROCESS:
		thisptr->SetPictCD(PICT_CD_TWO);
		break;
	case ONE_PROCESS:
		thisptr->SetPictCD(PICT_CD_ONE);
		break;
	case GAME_START_PROCESS:
		thisptr->SetPictCD(PICT_CD_GAME_START);
		break;
	}
}

void GameStartState::Enter(Referee *thisptr)
{
	thisptr->SetRefereeState(STATE_POINT_GET);
}