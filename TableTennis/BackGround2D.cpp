#include "BackGround2D.h"
#include "Engine/Image.h"

BackGround2D::BackGround2D(IGameObject *parent)
	:IGameObject(parent, "BackGround")
	, hPict_(ASSERT_ERROR)
{
}

BackGround2D::~BackGround2D()
{
}

void BackGround2D::Initialize()
{
	hPict_ = Image::Load("data/Image/BackGround.png");
	assert(hPict_ >= 0);
}

void BackGround2D::Update()
{
}

void BackGround2D::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

void BackGround2D::Release()
{
}