#pragma once
#include "RacketState.h"

//サーブ時の構えの状態を管理するクラス
class StanceServeState : public RacketState
{
public:
	~StanceServeState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};