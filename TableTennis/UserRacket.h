#pragma once
#include "Racket.h"

//人側のラケットを管理するクラス
class UserRacket : public Racket
{
public:
	UserRacket(IGameObject *parent);
	~UserRacket();

	//trueなら、そのボタンがオンという事を示す
	bool ButtonFrontMove() override;
	bool ButtonBackMove() override;
	bool ButtonLeftMove() override;
	bool ButtonRightMove() override;
	bool ButtonTopSpin() override;
	bool ButtonBackSpin() override;
	bool ButtonLeftSideSpin() override;
	bool ButtonRightSideSpin() override;
	bool ButtonLobbing() override;
	bool ButtonSmash() override;
};