#pragma once
#include "IGameObject.h"

//カメラを管理するクラス
class Camera : public IGameObject
{
	D3DXVECTOR3 target_;

public:
	Camera(IGameObject *parent);

	~Camera();

	void Initialize() override;

	void Update() override;

	void Draw() override;

	void Release() override;

	void SetTarget(const D3DXVECTOR3 target)
	{
		target_ = target;
	}
};