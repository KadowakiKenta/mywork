#pragma once
#include "IGameObject.h"

//�_
class RootJob : public IGameObject
{
public:
	RootJob();
	~RootJob();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};