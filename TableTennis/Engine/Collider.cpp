#include "BoxCollider.h"
#include "SphereCollider.h"
#include "IgameObject.h"
#include "Direct3D.h"

Collider::Collider(const std::string& name) :
	pGameObject_(nullptr), pMesh_(nullptr)
	, rotate_(D3DXVECTOR3(0, 0, 0)), name_(name)
{
}

Collider::Collider() : Collider("")
{
}

Collider::~Collider()
{
	if (pMesh_ != nullptr)
	{
		pMesh_->Release();
	}
}

bool Collider::IsHitBoxVsBox(const BoxCollider* obb1, const BoxCollider* obb2)
{
	//各方向ベクトルの確保
	D3DXVECTOR3 NAe1 = obb1->GetDirect(0), Ae1 = NAe1 * obb1->GetLen_W(0);
	D3DXVECTOR3 NAe2 = obb1->GetDirect(1), Ae2 = NAe2 * obb1->GetLen_W(1);
	D3DXVECTOR3 NAe3 = obb1->GetDirect(2), Ae3 = NAe3 * obb1->GetLen_W(2);
	D3DXVECTOR3 NBe1 = obb2->GetDirect(0), Be1 = NBe1 * obb2->GetLen_W(0);
	D3DXVECTOR3 NBe2 = obb2->GetDirect(1), Be2 = NBe2 * obb2->GetLen_W(1);
	D3DXVECTOR3 NBe3 = obb2->GetDirect(2), Be3 = NBe3 * obb2->GetLen_W(2);
	D3DXVECTOR3 Interval = (obb1->GetPos() + obb1->pGameObject_->GetPosition()) 
						- (obb2->GetPos() + obb2->pGameObject_->GetPosition());

	//分離軸 : Ae1
	FLOAT rA = D3DXVec3Length(&Ae1);
	FLOAT rB = LenSegOnSeparateAxis(&NAe1, &Be1, &Be2, &Be3);
	FLOAT L = fabs(D3DXVec3Dot(&Interval, &NAe1));
	if (L > rA + rB)
		return false;

	//分離軸 : Ae2
	rA = D3DXVec3Length(&Ae2);
	rB = LenSegOnSeparateAxis(&NAe2, &Be1, &Be2, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &NAe2));
	if (L > rA + rB)
		return false;

	//分離軸 : Ae3
	rA = D3DXVec3Length(&Ae3);
	rB = LenSegOnSeparateAxis(&NAe3, &Be1, &Be2, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &NAe3));
	if (L > rA + rB)
		return false;

	//分離軸 : Be1
	rA = LenSegOnSeparateAxis(&NBe1, &Ae1, &Ae2, &Ae3);
	rB = D3DXVec3Length(&Be1);
	L = fabs(D3DXVec3Dot(&Interval, &NBe1));
	if (L > rA + rB)
		return false;

	//分離軸 : Be2
	rA = LenSegOnSeparateAxis(&NBe2, &Ae1, &Ae2, &Ae3);
	rB = D3DXVec3Length(&Be2);
	L = fabs(D3DXVec3Dot(&Interval, &NBe2));
	if (L > rA + rB)
		return false;

	//分離軸 : Be3
	rA = LenSegOnSeparateAxis(&NBe3, &Ae1, &Ae2, &Ae3);
	rB = D3DXVec3Length(&Be3);
	L = fabs(D3DXVec3Dot(&Interval, &NBe3));
	if (L > rA + rB)
		return false;

	//分離軸 : C11
	D3DXVECTOR3 Cross;
	D3DXVec3Cross(&Cross, &NAe1, &NBe1);
	rA = LenSegOnSeparateAxis(&Cross, &Ae2, &Ae3);
	rB = LenSegOnSeparateAxis(&Cross, &Be2, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C12
	D3DXVec3Cross(&Cross, &NAe1, &NBe2);
	rA = LenSegOnSeparateAxis(&Cross, &Ae2, &Ae3);
	rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C13
	D3DXVec3Cross(&Cross, &NAe1, &NBe3);
	rA = LenSegOnSeparateAxis(&Cross, &Ae2, &Ae3);
	rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be2);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C21
	D3DXVec3Cross(&Cross, &NAe2, &NBe1);
	rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae3);
	rB = LenSegOnSeparateAxis(&Cross, &Be2, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C22
	D3DXVec3Cross(&Cross, &NAe2, &NBe2);
	rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae3);
	rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C23
	D3DXVec3Cross(&Cross, &NAe2, &NBe3);
	rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae3);
	rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be2);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C31
	D3DXVec3Cross(&Cross, &NAe3, &NBe1);
	rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae2);
	rB = LenSegOnSeparateAxis(&Cross, &Be2, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C32
	D3DXVec3Cross(&Cross, &NAe3, &NBe2);
	rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae2);
	rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be3);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	//分離軸 : C33
	D3DXVec3Cross(&Cross, &NAe3, &NBe3);
	rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae2);
	rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be2);
	L = fabs(D3DXVec3Dot(&Interval, &Cross));
	if (L > rA + rB)
		return false;

	return true;
}

bool Collider::IsHitBoxVsCircle(const BoxCollider* obb, const SphereCollider* sphere)
{
	D3DXVECTOR3 Vec(0, 0, 0);

	for (int i = 0; i < 3; i++)
	{
		FLOAT L = obb->GetLen_W(i);
		if (L <= 0) continue;	//L=0は計算できない
		FLOAT s = D3DXVec3Dot(&((sphere->GetPos() + sphere->pGameObject_->GetPosition()) 
								- (obb->GetPos() + obb->pGameObject_->GetPosition()))
								, &obb->GetDirect(i)) / L;

		//sの値から、はみ出した部分があればそのベクトルを加算
		s = fabs(s);
		if (s > 1)
			Vec += (1 - s)*L*obb->GetDirect(i);   // はみ出した部分のベクトル算出
	}

	if (D3DXVec3Length(&Vec) > sphere->Getradius())
	{
		return false;
	}

	return true;   //長さを出力
}

bool Collider::IsHitCircleVsCircle(const SphereCollider* circleA, const SphereCollider* circleB)
{
	D3DXVECTOR3 v = (circleA->center_ + circleA->pGameObject_->GetPosition()) 
		- (circleB->center_ + circleB->pGameObject_->GetPosition());

	if (D3DXVec3Length(&v) <= (circleA->radius_ + circleB->radius_))
	{
		return true;
	}

	return false;
}

void Collider::Draw(const D3DXVECTOR3 position)
{
	D3DXMATRIX mat;

	D3DXMATRIX mX;
	D3DXMatrixRotationX(&mX, D3DXToRadian(rotate_.x + pGameObject_->GetRotate().x));
	D3DXMATRIX mY;
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate_.y + pGameObject_->GetRotate().y));
	D3DXMATRIX mZ;
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotate_.z + pGameObject_->GetRotate().z));

	D3DXMatrixTranslation(&mat, position.x + center_.x, position.y + center_.y, position.z + center_.z);
	mat = mX * mY * mZ  * mat;

	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &mat);
	pMesh_->DrawSubset(0);
}

FLOAT Collider::LenSegOnSeparateAxis(const D3DXVECTOR3 * Sep, const D3DXVECTOR3 * e1, const D3DXVECTOR3 * e2, const D3DXVECTOR3 * e3)
{
	//3つの内積の絶対値の和で投影線分長を計算
	//分離軸Sepは標準化されていること
	FLOAT r1 = fabs(D3DXVec3Dot(Sep, e1));
	FLOAT r2 = fabs(D3DXVec3Dot(Sep, e2));
	FLOAT r3 = e3 ? (fabs(D3DXVec3Dot(Sep, e3))) : 0;
	return r1 + r2 + r3;
}
