#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"
#include "SceneManager.h"
#include "Direct3D.h"

IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
		ColliderList_.clear();
}

IGameObject::IGameObject(IGameObject *parent) :
	IGameObject(parent, "")
{
}

IGameObject::IGameObject(IGameObject *parent, const std::string& name) :
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1))
{
	D3DXMatrixIdentity(&localMatrix_);
	D3DXMatrixIdentity(&worldMatrix_);
}

IGameObject::~IGameObject()
{
	for (auto it = ColliderList_.begin(); it != ColliderList_.end(); it++)
	{
		SAFE_DELETE(*it);
	}
	ColliderList_.clear();
}

void IGameObject::Transform()
{
	D3DXMATRIX mX;
	D3DXMatrixRotationX(&mX, D3DXToRadian(rotate_.x));
	D3DXMATRIX mY;
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate_.y));
	D3DXMATRIX mZ;
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotate_.z));

	D3DXMATRIX m;
	D3DXMatrixScaling(&m, scale_.x, scale_.y, scale_.z);

	D3DXMATRIX mT;
	D3DXMatrixTranslation(&mT, position_.x, position_.y, position_.z);

	D3DXMATRIX mW;
	localMatrix_ = m * mX * mY * mZ * mT;

	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}

void IGameObject::PushBackChild(IGameObject *pObj)
{
	childList_.push_back(pObj);
}

void IGameObject::UpdateSub()
{
	if (objState_.isInit == OFF)
	{
		objState_.isInit = ON;
		Initialize();
	}

	Update();
	Transform();

	for (auto i = this->ColliderList_.begin(); i != this->ColliderList_.end(); i++)
	{
		(*i)->SetRotateRelation(rotate_);
		(*i)->InisializeHitCollider();
	}

	if (SceneManager::GetCurrentScene() != nullptr)
	{
		Collision(SceneManager::GetCurrentScene());
	}

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		if ((*it)->objState_.isDead == ON)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}
		else 
		{
			it++;
		}
	}
}

void IGameObject::DrawSub()
{
	Draw();
#ifdef _DEBUG
	CollisionDraw();
#endif
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	//処理が1順する前に消してしまうとおかしくなる可能性がある
	//ここではフラグを立てるだけにする
	objState_.isDead = ON;
}

const IGameObject* IGameObject::FindGameObject(const std::string& name) const
{
	if (childList_.empty())
	{
		return nullptr;
	}

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		if ((*it)->GetName() == name)
			return (*it);

		 const IGameObject* obj = (*it)->FindGameObject(name);
		
		if (obj != nullptr)
		{
			return obj;
		}
	}

	return nullptr;
}

const IGameObject * IGameObject::FindObject(const std::string& name) const
{
	const SceneManager* pSceneManager = (SceneManager*)FindSceneManager();

	const IGameObject* obj = pSceneManager->FindChildObject(name);

	if (obj != nullptr)
	{
		return obj;
	}

	return nullptr;
};

const IGameObject* IGameObject::FindSceneManager() const
{
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		if ((*it)->GetName() == ("SceneManager"))
		{
			return (*it);
		}
	}

	const IGameObject* obj = pParent_->FindSceneManager();

	if (obj != nullptr)
	{
		return obj;
	}

	return nullptr;
};

void IGameObject::AddCollider(Collider *collider)
{
	collider->SetGameObject(this);
	ColliderList_.push_back(collider);
}

void IGameObject::Collision(const IGameObject *pTarget)
{
	if (this == pTarget)
	{
		return;
	}

	//自分とpTargetのコリジョン情報を使って当たり判定
	//1つのオブジェクトが複数のコリジョン情報を持ってる場合もあるので二重ループ
	for (auto i = this->ColliderList_.begin(); i != this->ColliderList_.end(); i++)
	{
		for (auto j = pTarget->ColliderList_.begin(); j != pTarget->ColliderList_.end(); j++)
		{
			if ((*i)->IsHit(*j))
			{
				(*i)->SetHitCollider((*j));
				this->OnCollision(pTarget);	//オーバーライド
			}
		}
	}

	if (pTarget->childList_.empty())
		return;

	for (auto i = pTarget->childList_.begin(); i != pTarget->childList_.end(); i++)
	{
		Collision(*i);
	}
}

void IGameObject::CollisionDraw()
{
	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);
	Direct3D::pDevice_->SetTexture(0, nullptr);

	for (auto i = this->ColliderList_.begin(); i != this->ColliderList_.end(); i++)
	{
		(*i)->Draw(position_);
	}

	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING,TRUE);
}

const D3DXMATRIX& IGameObject::GetLocalMatrix(void) const
{
	return localMatrix_;
}

const D3DXMATRIX& IGameObject::GetWorldMatrix(void) const
{
	return worldMatrix_;
}
