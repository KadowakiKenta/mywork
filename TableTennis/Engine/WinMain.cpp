#include <Windows.h>
#include "Global.h"
#include "Direct3D.h"
#include "Sprite.h"
#include "Fbx.h"
#include "Input.h"
#include "RootJob.h"
#include "Image.h"
#include "Model.h"
#include "Audio.h"

#ifdef _DEBUG
#include <crtdbg.h>
#endif

Global g;
RootJob* pRootJob;

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

const char* WIN_CLASS_NAME = "SampleGame";
const int	WINDOW_WIDTH = 1024;
const int	WINDOW_HEIGHT = 768;

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{

#ifdef _DEBUG
	//メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.hInstance = hInstance;
	wc.lpszClassName = WIN_CLASS_NAME;
	wc.lpfnWndProc = WndProc;
	wc.style = CS_VREDRAW | CS_HREDRAW;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszMenuName = NULL;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	RegisterClassEx(&wc);

	//ウィンドウの上下左右の端っこを切り取る
	RECT winRect = { 0,0,WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW,FALSE);

	//ウィンドウを作成
	HWND hWnd = CreateWindow(
		WIN_CLASS_NAME,					//ウィンドウクラス名
		"卓球ゲーム",					//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,			//スタイル
		CW_USEDEFAULT,					//表示位置左
		CW_USEDEFAULT,					//表示位置上
		winRect.right - winRect.left,	//ウィンドウ幅
		winRect.bottom - winRect.top,	//ウィンドウ高さ
		NULL,							//親ウインドウ
		NULL,							//メニュー
		hInstance,						//インスタンスハンドル
		NULL							//パラメータ
	);
	assert(hWnd != NULL);

	ShowWindow(hWnd, nCmdShow);

	Direct3D::Initialize(hWnd);

	Input::Initialize(hWnd);

	//Audio::Initialize();
	//Audio::LoadWaveBank("Data/BGM/BGM.xwb");
	//Audio::LoadSoundBank("Data/BGM/BGM.xsb");

	pRootJob = new RootJob;

	//メッセージループ
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Input::Update();

			pRootJob->UpdateSub();

			Direct3D::BeginDraw();
			pRootJob->DrawSub();
			Direct3D::EndDraw();

		}
	}

	//Audio::Release();
	Image::AllRelease();
	Model::AllRelease();
	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Input::Release();
	Direct3D::Release();

	return 0;
}

//ウィンドウプロシージャ
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}