#include <assert.h>
#include "Input.h"
#pragma comment(lib,"Xinput.lib")

namespace Input
{
	LPDIRECTINPUT8   pDInput = nullptr;	//Inputでしか使わないのでcpp内で宣言
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	BYTE keyState[256] = { 0 };
	BYTE prevKeyState[256] = { 0 };

	//コントローラー
	const int MAX_PAD_NUM = 4;
	XINPUT_STATE _controllerState[MAX_PAD_NUM];
	XINPUT_STATE _prevControllerState[MAX_PAD_NUM];

	void Initialize(HWND hWnd)
	{
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		assert(pDInput != nullptr);

		pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
		assert(pKeyDevice != nullptr);
		
		//何の操作デバイスか指定
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);

	}

	void Update()
	{
		memcpy(prevKeyState,keyState, sizeof(keyState));

		pKeyDevice->Acquire();	//キーボードを見失う？時にもう一回探す
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		//コントローラー
		for (int i = 0; i < MAX_PAD_NUM; i++)
		{
			memcpy(&_prevControllerState[i], &_controllerState[i], sizeof(_controllerState[i]));
			XInputGetState(i, &_controllerState[i]);
		}
	}

	//キーボード情報取得
	const bool IsKey(const int keyCode)
	{
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	const bool IsKeyDown(const int keyCode)
	{
		if (keyState[keyCode] & 0x80 && !(prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	const bool IsKeyUp(const int keyCode)
	{
		if (!(keyState[keyCode] & 0x80) && prevKeyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//コントローラー情報取得
	const bool IsPadButton(const int buttonCode, const int padID)
	{
		if (_controllerState[padID].Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}

	const bool IsPadButtonDown(const int buttonCode, const int padID)
	{
		if (IsPadButton(buttonCode, padID) && !(_prevControllerState[padID].Gamepad.wButtons & buttonCode))
		{
			return true;
		}
		return false;
	}

	const bool IsPadButtonUp(const int buttonCode, const int padID)
	{
		if (!IsPadButton(buttonCode, padID) && _prevControllerState[padID].Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}

	const float GetAnalogValue(int raw, int max, int deadZone)
	{
		float result = (float)raw;

		if (result > 0)
		{
			if (result < deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result - deadZone) / (max - deadZone);
			}
		}
		else
		{
			if (result > -deadZone)
			{
				result = 0;
			}
			else
			{
				result = (result + deadZone) / (max - deadZone);
			}
		}

		return result;
	}

	const D3DXVECTOR3 GetPadStickL(const int padID)
	{
		float x = GetAnalogValue(_controllerState[padID].Gamepad.sThumbLX, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		float y = GetAnalogValue(_controllerState[padID].Gamepad.sThumbLY, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		return D3DXVECTOR3(x, y, 0);
	}

	const D3DXVECTOR3 GetPadStickR(const int padID)
	{
		float x = GetAnalogValue(_controllerState[padID].Gamepad.sThumbRX, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		float y = GetAnalogValue(_controllerState[padID].Gamepad.sThumbRY, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		return D3DXVECTOR3(x, y, 0);
	}

	const float GetPadTrrigerL(const int padID)
	{
		return GetAnalogValue(_controllerState[padID].Gamepad.bLeftTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	const float GetPadTrrigerR(const int padID)
	{
		return GetAnalogValue(_controllerState[padID].Gamepad.bRightTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	void SetPadVibration(const int l, const int r, const int padID)
	{
		XINPUT_VIBRATION vibration;
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
		vibration.wLeftMotorSpeed = l;
		vibration.wRightMotorSpeed = r;
		XInputSetState(padID, &vibration);
	}

	void Release()
	{
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}
}