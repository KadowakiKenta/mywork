#include "Direct3D.h"

LPDIRECT3D9 Direct3D::pD3d_ = nullptr;
LPDIRECT3DDEVICE9 Direct3D::pDevice_ = nullptr;

void Direct3D::Initialize(const HWND hWnd)
{
	HRESULT hr;

	//Direct3Dオブジェクトの作成
	pD3d_ = Direct3DCreate9(D3D_SDK_VERSION);
	assert(pD3d_ != nullptr);

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;						//falseでフルスクリーン
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = (UINT)g.screenWidth;
	d3dpp.BackBufferHeight = (UINT)g.screenHeight;
	d3dpp.hDeviceWindow = hWnd;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;


	//アンチエイリアシング
	DWORD QualityBackBuffer = 0;
	pD3d_->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, d3dpp.BackBufferFormat,
		d3dpp.Windowed, D3DMULTISAMPLE_4_SAMPLES, &QualityBackBuffer);
	d3dpp.MultiSampleType = D3DMULTISAMPLE_4_SAMPLES;
	d3dpp.MultiSampleQuality = QualityBackBuffer - 1;

	hr = pD3d_->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
		hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice_);
	assert(pDevice_ != nullptr);

	//アルファブレンド
	pDevice_->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice_->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice_->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング
	pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);

	//仮ライト
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));
	lightState.Type = D3DLIGHT_DIRECTIONAL;
	lightState.Direction = D3DXVECTOR3(0, -1, 0);
	lightState.Diffuse.r = 1.0f;
	lightState.Diffuse.g = 1.0f;
	lightState.Diffuse.b = 1.0f;

	//ライトを作成
	pDevice_->SetLight(0, &lightState);
	pDevice_->LightEnable(0, TRUE);

	//仮カメラ
	D3DXMATRIX view, proj;
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (FLOAT)g.screenWidth / (FLOAT)g.screenHeight, 0.5f, 1000.0f);
	pDevice_->SetTransform(D3DTS_VIEW, &view);
	pDevice_->SetTransform(D3DTS_PROJECTION, &proj);
}

void Direct3D::BeginDraw()
{
	pDevice_->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);
	pDevice_->BeginScene();
}

void Direct3D::EndDraw()
{
	pDevice_->EndScene();
	pDevice_->Present(nullptr, nullptr, nullptr, nullptr);
}

void Direct3D::Release()
{
	pDevice_->Release();
	pD3d_->Release();
}
