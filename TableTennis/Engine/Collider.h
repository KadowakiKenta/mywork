#pragma once

#include <d3dx9.h>
#include <string>

class IGameObject;

enum ColliderType
{
	COLLIDER_BOX,
	COLLIDER_CIRCLE
};


//当たり判定を管理するクラス
class Collider
{
	friend class BoxCollider;
	friend class SphereCollider;

protected:
	IGameObject*	pGameObject_;
	ColliderType	type_;
	D3DXVECTOR3		center_;
	D3DXVECTOR3		rotate_;
	LPD3DXMESH		pMesh_;

	Collider* pHitCollider_;

	std::string name_;

public:
	Collider(const std::string& name);
	Collider();

	virtual ~Collider();

	virtual bool IsHit(const Collider* target) = 0;

	virtual void SetRotateRelation(const D3DXVECTOR3 rotate) = 0;

	bool IsHitBoxVsBox(const BoxCollider* boxA, const BoxCollider* boxB);

	bool IsHitBoxVsCircle(const BoxCollider* box, const SphereCollider* sphere);

	bool IsHitCircleVsCircle(const SphereCollider* circleA, const SphereCollider* circleB);

	void Draw(const D3DXVECTOR3 position);

	void SetGameObject(IGameObject* gameObject) { pGameObject_ = gameObject; }

	void SetCenter(const D3DXVECTOR3 center)
	{
		center_ = center;
	}

	const D3DXVECTOR3 GetPos() const
	{
		return center_;
	}

	//分離軸に投影された軸成分から投影線分長を算出
	FLOAT LenSegOnSeparateAxis(const D3DXVECTOR3 *Sep, const D3DXVECTOR3 *e1, const D3DXVECTOR3 *e2, const D3DXVECTOR3 *e3 = 0);

	void SetHitCollider(Collider* hitCollider)
	{
		pHitCollider_ = hitCollider;
	}

	const Collider* GetHitCollider() const
	{
		return pHitCollider_;
	}

	void InisializeHitCollider()
	{
		pHitCollider_ = nullptr;
	}

	const std::string GetName() const
	{
		return name_;
	}
};

