#include <vector>
#include "Image.h"

namespace Image
{
	std::vector<ImageData*> dataList;

	int Load(const std::string fileName)
	{
		ImageData* pData = new ImageData;
		pData->fileName = fileName;

		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName == fileName)
			{
				pData->pSprite = dataList[i]->pSprite;

				isExist = true;

				break;
			}
		}

		if (isExist == false)
		{
			pData->pSprite = new Sprite;

			pData->pSprite->Load(fileName.c_str());
		}

		dataList.push_back(pData);

		return dataList.size() - 1;
	}

	void Draw(const int handle)
	{
		DrawSub(handle, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1));
	}
	void Draw(const int handle, const float alphaBlend)
	{
		DrawSub(handle, D3DXCOLOR(1.0f, 1.0f, 1.0f, alphaBlend));
	}
	void Draw(const int handle, const D3DXCOLOR color)
	{
		DrawSub(handle, color);
	}

	void DrawSub(const int handle, const D3DXCOLOR color)
	{
		dataList[handle]->pSprite->Draw(dataList[handle]->matrix ,color);
	}

	void SetMatrix(const int handle, const D3DXMATRIX &matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	void Release(const int handle)
	{
		{
			bool isExist = false;
			for (unsigned int i = 0; i < dataList.size(); i++)
			{
				if (i != handle && dataList[i] != nullptr &&
					dataList[i]->pSprite == dataList[handle]->pSprite)
				{
					isExist = true;
					break;
				}
			}

			if (isExist == false)
			{
				SAFE_DELETE(dataList[handle]->pSprite);
			}

			SAFE_DELETE(dataList[handle]);
		}
	}

	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}

	const int GetImageSizeWidth(const int hPict)
	{
		return dataList[hPict]->pSprite->GetImageSizeWidth();
	}

	const int GetImageSizeHeight(const int hPict)
	{
		return dataList[hPict]->pSprite->GetImageSizeHeight();
	}

}