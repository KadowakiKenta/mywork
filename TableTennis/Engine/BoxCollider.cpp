#include "BoxCollider.h"
#include "Direct3D.h"

BoxCollider::BoxCollider(const std::string& name, const D3DXVECTOR3 basePos, const D3DXVECTOR3 size) : Collider(name)
{
	center_ = basePos;
	size_ = size;
	type_ = COLLIDER_BOX;

	m_NormaDirect[0] = D3DXVECTOR3(1, 0, 0);
	m_NormaDirect[1] = D3DXVECTOR3(0, 1, 0);
	m_NormaDirect[2] = D3DXVECTOR3(0, 0, 1);

	m_fLength[0] = size.x / 2;
	m_fLength[1] = size.y / 2;
	m_fLength[2] = size.z / 2;

#ifdef _DEBUG
	D3DXCreateBox(Direct3D::pDevice_, size.x, size.y, size.z, &pMesh_, 0);
#endif
}

BoxCollider::BoxCollider(const D3DXVECTOR3 basePos, const D3DXVECTOR3 size) : BoxCollider("", basePos, size)
{
}


bool BoxCollider::IsHit(const Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsBox((BoxCollider*)target, this);
	else
		return IsHitBoxVsCircle(this, (SphereCollider*)target);
}