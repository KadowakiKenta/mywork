#pragma once
#include <d3dx9.h>
#include <list>
#include <string.h>

class Collider;

const unsigned int ON = 1;
const unsigned int OFF = 0;

//全てのゲームオブジェクトの根幹
class IGameObject
{
protected:
	IGameObject *pParent_;
	std::list < IGameObject* > childList_;
	std::string name_;

	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	D3DXMATRIX localMatrix_;
	D3DXMATRIX worldMatrix_;

	struct OBJECT_STATE
	{
		unsigned int isInit = OFF;
		unsigned int isDead = OFF;
	};
	OBJECT_STATE objState_;

	std::list<Collider*> ColliderList_;

	void Transform();

	const IGameObject* FindGameObject(const std::string& name) const;
	const IGameObject* FindSceneManager() const;

public:
	IGameObject();
	IGameObject(IGameObject *parent);
	IGameObject(IGameObject *parent, const std::string& name);

	//仮想デストラクタにしないと子クラスのデストラクタが呼ばれず親クラスのデストラクタが呼ばれる
	virtual ~IGameObject();

	//オーバーライドで実装
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;

	//上の純粋仮想関数とは違い
	//こちらは再帰処理で全てのゲームオブジェクトの処理を呼び出す
	void UpdateSub();
	virtual void DrawSub();
	void ReleaseSub();

	void KillMe();
	
	template<class T>
	T* CreateGameObject(IGameObject *parent)
	{
		T* p = new T(parent);

		parent->PushBackChild(p);
		
		return p;
	};
	void IGameObject::PushBackChild(IGameObject *pObj);

	void SetPosition(const D3DXVECTOR3 position) { position_ = position; };
	void SetScale(const D3DXVECTOR3 scale) { scale_ = scale; };
	void SetRotate(const D3DXVECTOR3 rotate) { rotate_ = rotate; };

	const D3DXVECTOR3 GetPosition() const { return position_; };
	const D3DXVECTOR3 GetRotate() const { return rotate_; };
	const D3DXVECTOR3 GetScale() const { return scale_; }
	const std::string GetName() const { return name_; };
	const IGameObject* GetChild(const int ID) const
	{
		auto it = childList_.begin();
		for (int i = 0; i < ID; i++)
		{
			it++;
		}
		return (*it);
	}
	const Collider* GetColliderList(const int i) const
	{
		auto it = ColliderList_.begin();
		for (int j = 0; j < i; j++)
		{
			it++;
		}
		return (*it);
	}
	virtual const D3DXMATRIX& GetLocalMatrix() const;
	virtual const D3DXMATRIX& GetWorldMatrix() const;

	//名前がわかれば、オブジェクトのポインタが取得できる
	//すげー便利なやつ
	const IGameObject* FindObject(const std::string& name) const;

	void AddCollider(Collider *collider);
	void Collision(const IGameObject *pTarget);
	virtual void OnCollision(const IGameObject *pTarget) {};
	void CollisionDraw();
};				 


