#pragma once

#include <string>
#include "Global.h"
#include "Fbx.h"

//FBXを利用して、3Dモデルを管理する名前空間
namespace Model
{
	struct ModelData
	{
		std::string fileName;
		Fbx*		pFbx;
		D3DXMATRIX	matrix;

		float nowFrame, animSpeed;
		float startFrame, endFrame;

		ModelData() : pFbx(nullptr), nowFrame(0), startFrame(0), endFrame(0), animSpeed(0)
		{
			D3DXMatrixIdentity(&matrix);
		}

		void SetAnimFrame(const float start, const float end, const float speed)
		{
			nowFrame = start;
			startFrame = start;
			endFrame = end;
			animSpeed = speed;
		}
	};

	void Initialize();

	int Load(const std::string fileName);

	void Draw(const int handle, const IGameObject *myObject, const LPD3DXEFFECT pEffect = nullptr);

	void Release(const int handle);

	//シーンが切り替わるときは必ず実行
	void AllRelease();

	void SetAnimFrame(const int handle, const float startFrame, const float endFrame, const float animSpeed);
	const float GetAnimFrame(const int handle);

	const D3DXVECTOR3 GetBonePosition(const int handle, const std::string boneName);

	void SetMatrix(const int handle, const D3DXMATRIX matrix);
	const D3DXMATRIX GetMatrix(const int handle);

	void RayCast(const int handle, RayCastData *data);

}