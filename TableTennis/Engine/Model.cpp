#include "Model.h"
#include <vector>

namespace Model
{
	std::vector<ModelData*>	dataList;

	void Initialize()
	{
		AllRelease();
	}

	int Load(const std::string fileName)
	{
		ModelData* pData = new ModelData;

		bool isExist = false;
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr && dataList[i]->fileName == fileName)
			{
				pData->pFbx = dataList[i]->pFbx;
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			pData->pFbx = new Fbx;
			if (FAILED(pData->pFbx->Load(fileName)))
			{
				SAFE_DELETE(pData->pFbx);
				SAFE_DELETE(pData);
				return -1;
			}

			pData->fileName = fileName;
		}


		//使ってない番号が無いか探す
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			if (dataList[i] == nullptr)
			{
				dataList[i] = pData;
				return i;
			}
		}

		dataList.push_back(pData);
		return dataList.size() - 1;
	}

	void Draw(const int handle, const IGameObject *myObject, const LPD3DXEFFECT pEffect)
	{
		if (handle < 0 || (unsigned int)handle >= dataList.size() || dataList[handle] == nullptr)
		{
			return;
		}

		dataList[handle]->nowFrame += dataList[handle]->animSpeed;

		if (dataList[handle]->nowFrame > dataList[handle]->endFrame)
			dataList[handle]->nowFrame = dataList[handle]->startFrame;

		if (dataList[handle]->pFbx)
		{
			dataList[handle]->pFbx->Draw(dataList[handle]->matrix, (int)dataList[handle]->nowFrame, myObject, pEffect);
		}
	}

	void Release(const int handle)
	{
		if (handle < 0 || (unsigned int)handle >= dataList.size() || dataList[handle] == nullptr)
		{
			return;
		}

		bool isExist = false;
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr && i != handle && dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pFbx);
		}


		SAFE_DELETE(dataList[handle]);
	}

	void AllRelease()
	{
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}

	void SetAnimFrame(const int handle, const float startFrame, const float endFrame, const float animSpeed)
	{
		dataList[handle]->SetAnimFrame(startFrame, endFrame, animSpeed);
	}
	const float GetAnimFrame(const int handle)
	{
		return dataList[handle]->nowFrame;
	}

	const D3DXVECTOR3 GetBonePosition(const int handle, const std::string boneName)
	{
		D3DXVECTOR3 pos = dataList[handle]->pFbx->GetBonePosition(boneName);
		D3DXVec3TransformCoord(&pos, &pos, &dataList[handle]->matrix);
		return pos;
	}

	void SetMatrix(const int handle, const D3DXMATRIX matrix)
	{
		if (handle < 0 || (unsigned int)handle >= dataList.size())
		{
			return;
		}

		dataList[handle]->matrix = matrix;
	}
	const D3DXMATRIX GetMatrix(const int handle)
	{
		return dataList[handle]->matrix;
	}

	void RayCast(const int handle, RayCastData *data)
	{
		D3DXVECTOR3 target = data->start + data->dir;
		D3DXMATRIX matInv;
		D3DXMatrixInverse(&matInv, 0, &dataList[handle]->matrix);
		D3DXVec3TransformCoord(&data->start, &data->start, &matInv);
		D3DXVec3TransformCoord(&target, &target, &matInv);
		data->dir = target - data->start;

		dataList[handle]->pFbx->RayCast(data);
	}
}