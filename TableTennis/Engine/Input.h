#pragma once
#define DIRECTINPUT_VERSION 0x0800

#include <dInput.h>
#include <d3dx9.h>
#include "XInput.h"

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	void Initialize(HWND hWnd);
	
	void Update();
	
	void Release();
	
	//キーボード
	const bool IsKey(const int keyCode);
	const bool IsKeyDown(const int keyCode);
	const bool IsKeyUp(const int keyCode);

	//コントローラー
	const bool IsPadButton(const int buttonCode, const int padID = 0);
	const bool IsPadButtonDown(const int buttonCode, const int padID = 0);
	const bool IsPadButtonUp(const int buttonCode, const int padID = 0);

	const D3DXVECTOR3 GetPadStickL(const int padID = 0);
	const D3DXVECTOR3 GetPadStickR(const int padID = 0);

	const float		GetPadTrrigerL(const int padID = 0);
	const float		GetPadTrrigerR(const int padID = 0);

	void SetPadVibration(const int l, const int r, const int padID = 0);
};