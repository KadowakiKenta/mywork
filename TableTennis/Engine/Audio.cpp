#include "Audio.h"

namespace Audio
{
	IXACT3Engine* pXactEngine;
	IXACT3WaveBank* pWaveBank;
	IXACT3SoundBank* pSoundBank;

	void Initialize()
	{
		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		XACT3CreateEngine(0, &pXactEngine);
		XACT_RUNTIME_PARAMETERS xactParam = { 0 };
		xactParam.lookAheadTime = XACT_ENGINE_LOOKAHEAD_DEFAULT;
		pXactEngine->Initialize(&xactParam);
	}


	void LoadWaveBank(const std::string wFileName)
	{
		HANDLE hFile = CreateFile(wFileName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		DWORD fileSize = GetFileSize(hFile, NULL);

		//ファイルをマッピング
		HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, fileSize, NULL);
		void* mapWaveBank;
		mapWaveBank = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);

		pXactEngine->CreateInMemoryWaveBank(mapWaveBank, fileSize, 0, 0, &pWaveBank);

		CloseHandle(hMapFile);
		CloseHandle(hFile);

	}

	void LoadSoundBank(const std::string sFileName)
	{
		HANDLE hFile = CreateFile(sFileName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		DWORD fileSize = GetFileSize(hFile, NULL);

		//ファイルの中身をいったん配列に入れる
		void* soundBankData;
		soundBankData = new BYTE[fileSize];
		DWORD byteRead;
		ReadFile(hFile, soundBankData, fileSize, &byteRead, NULL);

		pXactEngine->CreateSoundBank(soundBankData, fileSize, 0, 0, &pSoundBank);

		CloseHandle(hFile);
	}

	void Play(const std::string fileName)
	{
		XACTINDEX cueIndex = pSoundBank->GetCueIndex(fileName.c_str());
		pSoundBank->Play(cueIndex, 0, 0, NULL);
	}

	void Stop(const std::string fileName)
	{
		XACTINDEX cueIndex = pSoundBank->GetCueIndex(fileName.c_str());
		pSoundBank->Stop(cueIndex, XACT_FLAG_CUE_STOP_IMMEDIATE);
	}

	void Release()
	{
		pSoundBank->Destroy();
		pWaveBank->Destroy();
		pXactEngine->ShutDown();
		CoUninitialize();
	}

}


