#pragma once
#include "Global.h"
#include <fbxsdk.h>
#include <vector>
#include <map>

#pragma comment(lib,"libfbxsdk-mt.lib")

class FbxParts;

struct RayCastData
{
	D3DXVECTOR3 start;
	D3DXVECTOR3 dir;
	float       dist;
	BOOL        hit;
	D3DXVECTOR3 normal;

	RayCastData() { hit = FALSE; dist = 99999.0f; }
};


//ひとつのFBXファイルを扱うクラス
class Fbx
{
	friend class FbxParts;

	FbxManager*  pManager_;
	FbxImporter* pImporter_;
	FbxScene*    pScene_;

	FbxTime::EMode	frameRate_;

	float animSpeed_;

	int startFrame_, endFrame_;

	std::vector<FbxParts*>	parts_;

	std::map<std::string, LPDIRECT3DTEXTURE9> pTexture_;


	void CheckNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList);

	LPDIRECT3DTEXTURE9 LoadTexture(const std::string fileName);
public:
	Fbx();

	~Fbx();

	HRESULT Load(const std::string fileName);

	void Draw(const D3DXMATRIX matrix, const int frame, const IGameObject *myObject, const LPD3DXEFFECT pEffect);
	
	void SetAnimFrame(const int startFrame, const int endFrame, const float speed);

	const D3DXVECTOR3 GetBonePosition(const std::string boneName) const;

	void RayCast(RayCastData *data);
};
