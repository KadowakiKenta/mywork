#pragma once
#include <d3dx9.h>
#include <string>
#include "Global.h"

//テキストを管理するクラス
class Text
{
	LPD3DXFONT pFont_;
	DWORD color_;

public:
	Text(const std::string fontName, const int size);

	~Text();

	void SetColor(const DWORD r, const DWORD g, const DWORD b, const DWORD alpha);

	void Draw(const int x, const int y, const std::string text);
};
