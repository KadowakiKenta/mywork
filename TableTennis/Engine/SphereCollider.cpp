#include "SphereCollider.h"
#include "BoxCollider.h"
#include "Direct3D.h"

SphereCollider::SphereCollider(const std::string& name, const D3DXVECTOR3 center, const float radius) : Collider(name)
{
	center_ = center;
	radius_ = radius;
	type_ = COLLIDER_CIRCLE;

#ifdef _DEBUG
	D3DXCreateSphere(Direct3D::pDevice_, radius, 8, 4, &pMesh_, 0);
#endif
}

SphereCollider::SphereCollider(const D3DXVECTOR3 center, const float radius) : SphereCollider("", center, radius)
{
}

bool SphereCollider::IsHit(const Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsCircle((BoxCollider*)target, this);
	else
		return IsHitCircleVsCircle((SphereCollider*)target, this);
}