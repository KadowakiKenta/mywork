#include "Text.h"
#include "Direct3D.h"

Text::Text(const std::string fontName, const int size) :pFont_(nullptr), color_(D3DCOLOR_RGBA(210, 176, 113, 255))
{
	D3DXCreateFont(
		Direct3D::pDevice_,			//デバイスオブジェクト
		size,						//文字サイズ（高さ）
		0,							//文字サイズ（幅）
		FW_NORMAL,					//太さ
		1,							//MIPMAPのレベル
		FALSE,						//斜体
		DEFAULT_CHARSET,			//文字セット
		OUT_DEFAULT_PRECIS,			//出力精度
		DEFAULT_QUALITY,			//出力品質
		DEFAULT_PITCH | FF_SWISS,	//ピッチとファミリ
		fontName.c_str(),			//フォント名
		&pFont_);					//フォントオブジェクト

}

Text::~Text()
{
	SAFE_RELEASE(pFont_);
}

void Text::SetColor(const DWORD r, const DWORD g, const DWORD b, const DWORD alpha)
{
	color_ = D3DCOLOR_RGBA(r, g, b, alpha);
}


void Text::Draw(const int x, const int y, const std::string text)
{
	RECT rect = { x, y, 0, 0 };
	//DT_NOCLIP はみだしたらそのまま延長
	pFont_->DrawText(NULL, text.c_str(), -1, &rect, DT_LEFT | DT_NOCLIP, color_);
}