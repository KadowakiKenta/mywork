#include <d3dx9.h>
#include <fbxsdk.h>
#include <vector>
#include "Global.h"


class Fbx;
struct RayCastData;

//FBXの中の１つのパーツを扱うクラス
class FbxParts
{
public:
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
		D3DXVECTOR3 tangent;
	};

	struct Bone
	{
		D3DXMATRIX  bindPose;
		D3DXMATRIX  newPose;
		D3DXMATRIX  diffPose;
	};

	struct Weight
	{
		D3DXVECTOR3 posOrigin;
		D3DXVECTOR3 normalOrigin;
		int*		pBoneIndex;
		float*		pBoneWeight;
	};

private:
	int vertexCount_;
	int polygonCount_;
	int indexCount_;
	int materialCount_;
	int* pPolygonCountOfMaterial_;

	Vertex*						pVertexList_;
	LPDIRECT3DVERTEXBUFFER9		vertexBuffer_;
	LPDIRECT3DINDEXBUFFER9*		pIndexBuffer_;
	D3DMATERIAL9*				pMaterial_;
	LPDIRECT3DTEXTURE9*			pTexture_;
	LPDIRECT3DTEXTURE9*			pNormalMap_;

	FbxSkin*		pSkinInfo_;
	FbxCluster**	ppCluster_;
	int				numBone_;
	Bone*			pBoneArray_;
	Weight*			pWeightArray_;

	FbxNode* pNode_;

	D3DXMATRIX localMatrix_;

	LPDIRECT3DVERTEXDECLARATION9 verDec_;

	std::vector<FbxParts*>	childParts_;

	void Draw(const D3DXMATRIX &matrix, const IGameObject *myObject, const LPD3DXEFFECT pEffect);

	D3DXVECTOR3 FbxParts::CalcTangent(
		D3DXVECTOR3 *pos0, D3DXVECTOR3 *pos1, D3DXVECTOR3 *pos2,
		D3DXVECTOR2 *uv0, D3DXVECTOR2 *uv1, D3DXVECTOR2 *uv2);
public:
	FbxParts();

	~FbxParts();

	void InitMaterial(FbxNode *pNode, Fbx* pFbx);

	void InitVertexBuffer(const FbxMesh *pMesh);

	void InitIndexBuffer(const FbxMesh *pMesh);

	void InitAnimation(const FbxMesh *pMesh);

	void DrawSkinAnime(const D3DXMATRIX &matrix, const FbxTime time, const IGameObject *myObject, const LPD3DXEFFECT pEffect);

	void DrawMeshAnime(const D3DXMATRIX &matrix, const FbxTime time, FbxScene *scene, const IGameObject *myObject, const LPD3DXEFFECT pEffect);

	const bool GetBonePosition(const std::string boneName, D3DXVECTOR3 *position) const;

	void RayCast(RayCastData *data);

	FbxSkin* GetSkinInfo() { return pSkinInfo_; }
};