#pragma once
#include <string>
#include "Sprite.h"

//スプライトを利用して、画像を管理する名前空間
namespace Image
{
	struct ImageData
	{
		std::string fileName;
		Sprite*	pSprite;
		D3DXMATRIX matrix;

		ImageData() : fileName(""), pSprite(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(const std::string fileName);

	void Draw(const int handle);
	void Draw(const int handle, const float alphaBlend);
	void Draw(const int handle, const D3DXCOLOR color);

	void DrawSub(const int handle, const D3DXCOLOR color);

	void SetMatrix(const int handle, const D3DXMATRIX &matrix);

	void Release(const int handle);

	void AllRelease();
	
	const int GetImageSizeWidth(const int hPict);
	const int GetImageSizeHeight(const int hPict);
};


