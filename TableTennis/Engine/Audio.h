#pragma once
#include <xact3.h>
#include <string>

//オーディオ
namespace Audio
{
	void Initialize();

	void LoadWaveBank(const std::string wFileName);

	void LoadSoundBank(const std::string sFileName);

	void Play(const std::string fileName);

	void Stop(const std::string fileName);

	void Release();
}