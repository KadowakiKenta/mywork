#pragma once
#include "Global.h"

//スプライト
class Sprite
{
	LPD3DXSPRITE		pSprite_;
	LPDIRECT3DTEXTURE9	pTexture_;

	struct ImageSize
	{
		int width;
		int height;
	}imageSize_;

public:
	Sprite();
	~Sprite();

	void Load(const char* FileName);
	void Draw(const D3DXMATRIX &matrix, const D3DXCOLOR color);

	const int GetImageSizeWidth() const
	{
		return imageSize_.width;
	}
	const int GetImageSizeHeight() const
	{
		return imageSize_.height;
	}
};

