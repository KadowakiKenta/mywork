#pragma once
#include "Collider.h"


//球体の当たり判定
class SphereCollider : public Collider
{
	friend class Collider;

	float		radius_;

public:
	SphereCollider(const std::string& name, const D3DXVECTOR3 center, const float radius);
	SphereCollider(const D3DXVECTOR3 center, const float radius);

	const float Getradius() const
	{
		return radius_;
	}

	void SetRotateRelation(const D3DXVECTOR3 rotate) override{}

private:
	bool IsHit(const Collider* target) override;
};

