#include "Sprite.h"
#include "Direct3D.h"

Sprite::Sprite() :
	pSprite_(nullptr), pTexture_(nullptr)
{
}

Sprite::~Sprite()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pSprite_);
}

void Sprite::Load(const char* FileName)
{
	D3DXCreateSprite(Direct3D::pDevice_, &pSprite_);
	assert(pSprite_ != nullptr);

	D3DXIMAGE_INFO iinfo;
	D3DXGetImageInfoFromFile(FileName, &iinfo);

	imageSize_.height = iinfo.Height;
	imageSize_.width = iinfo.Width;

	D3DXCreateTextureFromFileEx(Direct3D::pDevice_, FileName,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);
}

void Sprite::Draw(const D3DXMATRIX &matrix, const D3DXCOLOR color)
{
	pSprite_->SetTransform(&matrix);

	pSprite_->Begin(0);	//D3DXSPRITE_ALPHABLEND
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, color);
	pSprite_->End();
}
