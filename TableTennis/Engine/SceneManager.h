#pragma once
#include "IGameObject.h"
#include "SceneID.h"

//シーン管理する天使のクラス
class SceneManager : public IGameObject
{
	static SCENE_ID currentSceneID_;
	static SCENE_ID nextSceneID_;
	static IGameObject* pCurrentScene_;
	static bool sceneReload_;

public:
	SceneManager(IGameObject *parent);
	~SceneManager();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;

	static void ChangeSceneID(const SCENE_ID next);

	static void ReloadScene();

	static const IGameObject* GetCurrentScene()
	{
		return pCurrentScene_;
	}

	//IGameObject内でのFindObject()で使われる
	const IGameObject* FindChildObject(const std::string& name) const
	{
		return FindGameObject(name);
	}
};