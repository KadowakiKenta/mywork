#include "Camera.h"
#include "Direct3D.h"

Camera::Camera(IGameObject *parent)
	:IGameObject(parent, "Camera"), target_(0, 0, 0)
{
}

Camera::~Camera()
{
}

void Camera::Initialize()
{
	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (FLOAT)g.screenWidth / (FLOAT)g.screenHeight, 0.5f, 3000.0f);
	Direct3D::pDevice_->SetTransform(D3DTS_PROJECTION, &proj);
}

void Camera::Update()
{
	Transform();

	D3DXVECTOR3 worldPosition, worldTarget;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	D3DXMATRIX view;
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	Direct3D::pDevice_->SetTransform(D3DTS_VIEW, &view);
}

void Camera::Draw()
{
}

void Camera::Release()
{
}

