#pragma once
#include "Collider.h"
#include <string>

//箱型の当たり判定
class BoxCollider :	public Collider
{
	friend class Collider;

	D3DXVECTOR3 size_;
	D3DXVECTOR3 m_NormaDirect[3];
	float m_fLength[3];

public:
	BoxCollider(const std::string& name, const D3DXVECTOR3 basePos, const D3DXVECTOR3 size);
	BoxCollider(const D3DXVECTOR3 basePos, const D3DXVECTOR3 size);

	//指定軸番号の方向ベクトルを取得
	const D3DXVECTOR3 GetDirect(const int elem) const
	{
		return m_NormaDirect[elem];
	}

	const float GetLen_W(const int elem) const
	{
		return m_fLength[elem];
	}

	void SetRotateRelation(const D3DXVECTOR3 rotate) override
	{
		D3DXVECTOR3 rotateSum;
		
		rotateSum = rotate_ + rotate;

		D3DXMATRIX mat;
		D3DXMatrixIdentity(&mat);

		D3DXMATRIX mX;
		D3DXMatrixRotationX(&mX, D3DXToRadian(rotateSum.x));
		D3DXMATRIX mY;
		D3DXMatrixRotationY(&mY, D3DXToRadian(rotateSum.y));
		D3DXMATRIX mZ;
		D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotateSum.z));
		
		mat = mX * mY * mZ;

		m_NormaDirect[0] = D3DXVECTOR3(1, 0, 0);
		m_NormaDirect[1] = D3DXVECTOR3(0, 1, 0);
		m_NormaDirect[2] = D3DXVECTOR3(0, 0, 1);

		D3DXVec3TransformCoord(&m_NormaDirect[0], &m_NormaDirect[0], &mat);
		D3DXVec3TransformCoord(&m_NormaDirect[1], &m_NormaDirect[1], &mat);
		D3DXVec3TransformCoord(&m_NormaDirect[2], &m_NormaDirect[2], &mat);

		for (int i = 0; i < 3; i++)
		{
			D3DXVec3Normalize(&m_NormaDirect[i], &m_NormaDirect[i]);
		}
	}

	void SetRotate(const D3DXVECTOR3 rotate)
	{
		rotate_ = rotate;
	}

private:
	bool IsHit(const Collider* target) override;

};

