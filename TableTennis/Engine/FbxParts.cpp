#include "FbxParts.h"
#include "Fbx.h"
#include "Direct3D.h"

FbxParts::FbxParts() :
	vertexCount_(0),
	polygonCount_(0),
	indexCount_(0),
	materialCount_(0),
	pPolygonCountOfMaterial_(nullptr),
	pMaterial_(nullptr),
	pTexture_(nullptr),
	pNormalMap_(nullptr),
	pVertexList_(nullptr),
	vertexBuffer_(nullptr),
	pIndexBuffer_(nullptr),
	pSkinInfo_(nullptr),
	ppCluster_(nullptr),
	pBoneArray_(nullptr),
	pWeightArray_(nullptr),
	verDec_(nullptr)
{
}

FbxParts::~FbxParts()
{
	SAFE_RELEASE(verDec_);
	SAFE_DELETE_ARRAY(pBoneArray_);
	SAFE_DELETE_ARRAY(ppCluster_);

	if (pWeightArray_ != nullptr)
	{
		for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
		{
			SAFE_DELETE_ARRAY(pWeightArray_[i].pBoneIndex);
			SAFE_DELETE_ARRAY(pWeightArray_[i].pBoneWeight);
		}
		SAFE_DELETE_ARRAY(pWeightArray_);
	}

	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(pIndexBuffer_[i]);
	}
	SAFE_DELETE_ARRAY(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);
	SAFE_DELETE_ARRAY(pNormalMap_);
	SAFE_DELETE_ARRAY(pMaterial_);

	SAFE_DELETE_ARRAY(pPolygonCountOfMaterial_);

	SAFE_RELEASE(vertexBuffer_);
	SAFE_DELETE_ARRAY(pVertexList_);

	for (int i = 0; (unsigned int)i < childParts_.size(); i++)
	{
		SAFE_DELETE(childParts_[i]);
	}
	childParts_.clear();
}

void FbxParts::InitMaterial(FbxNode *pNode, Fbx* pFbx)
{
	pNode_ = pNode;

	materialCount_ = pNode->GetMaterialCount();
	pMaterial_ = new D3DMATERIAL9[materialCount_];
	pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];
	pNormalMap_ = new LPDIRECT3DTEXTURE9[materialCount_];

	for (int i = 0; i < materialCount_; i++)
	{
		FbxSurfacePhong* surface = (FbxSurfacePhong*)pNode->GetMaterial(i);
		FbxDouble3 diffuse = surface->Diffuse;
		FbxDouble3 ambient = surface->Ambient;

		ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

		pMaterial_[i].Diffuse.r = (float)diffuse[0];
		pMaterial_[i].Diffuse.g = (float)diffuse[1];
		pMaterial_[i].Diffuse.b = (float)diffuse[2];
		pMaterial_[i].Diffuse.a = 1.0f;

		pMaterial_[i].Ambient.r = (float)ambient[0];
		pMaterial_[i].Ambient.g = (float)ambient[1];
		pMaterial_[i].Ambient.b = (float)ambient[2];
		pMaterial_[i].Ambient.a = 1.0f;

		//フォンの場合
		if (surface->GetClassId().Is(FbxSurfacePhong::ClassId))
		{
			FbxDouble3 specular = surface->Specular;
			pMaterial_[i].Specular.r = (float)specular[0];
			pMaterial_[i].Specular.g = (float)specular[1];
			pMaterial_[i].Specular.b = (float)specular[2];
			pMaterial_[i].Specular.a = 1.0f;

			pMaterial_[i].Power = (float)surface->Shininess;
		}

		//普通のテクスチャロード
		{
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			if (textureFile == nullptr)
			{
				pTexture_[i] = nullptr;
			}

			else
			{
				pTexture_[i] = pFbx->LoadTexture(textureFile->GetFileName());
			}
		}

		//ノーマルマップのロード
		{
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sBump);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			if (textureFile == nullptr)
			{
				pNormalMap_[i] = nullptr;
			}

			else
			{
				pNormalMap_[i] = pFbx->LoadTexture(textureFile->GetFileName());
			}
		}
	}
}

void FbxParts::InitVertexBuffer(const FbxMesh *pMesh)
{
	FbxVector4* pVertexPos = pMesh->GetControlPoints();

	vertexCount_ = pMesh->GetControlPointsCount();
	polygonCount_ = pMesh->GetPolygonCount();
	indexCount_ = pMesh->GetPolygonVertexCount();

	pVertexList_ = new FbxParts::Vertex[vertexCount_];


	//頂点の位置
	for (int i = 0; vertexCount_ > i; i++)
	{
		pVertexList_[i].pos.x = (float)pVertexPos[i][0];
		pVertexList_[i].pos.y = (float)pVertexPos[i][1];
		pVertexList_[i].pos.z = (float)pVertexPos[i][2];
	}


	//ポリゴン1枚ずつ調べる
	const FbxLayerElementUV* leUV = pMesh->GetLayer(0)->GetUVs();
	for (int poly = 0; poly < polygonCount_; poly++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(poly);

		for (int vertex = 0; vertex < 3; vertex++)
		{
			int index = pMesh->GetPolygonVertex(poly, vertex);

			//頂点の法線
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(poly, vertex, Normal);
			pVertexList_[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);
			D3DXVec3Normalize(&pVertexList_[index].normal, &pVertexList_[index].normal);


			//頂点のUV
			int lUVIndex = leUV->GetIndexArray().GetAt(poly * 3 + vertex);

			FbxVector2 lVec2 = leUV->GetDirectArray().GetAt(lUVIndex);

			pVertexList_[index].uv.x = (FLOAT)lVec2.mData[0];
			pVertexList_[index].uv.y = 1.0f - (FLOAT)lVec2.mData[1];	//Yは逆

		}
	}

	//タンジェント情報
	for (int i = 0; i < polygonCount_; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);

		D3DXVECTOR3 tangent = CalcTangent(
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 0]].pos,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 1]].pos,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 2]].pos,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 0]].uv,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 1]].uv,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 2]].uv);


		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];
			pVertexList_[index].tangent = tangent;
		}
	}

	D3DVERTEXELEMENT9 vertexElement[] = {
		{0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		{0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0},
		D3DDECL_END()
	};

	//頂点バッファ作成
	Direct3D::pDevice_->CreateVertexDeclaration(vertexElement, &verDec_);
	Direct3D::pDevice_->CreateVertexBuffer(sizeof(FbxParts::Vertex)*vertexCount_, 0, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &vertexBuffer_, 0);
	FbxParts::Vertex *vCopy;
	vertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, pVertexList_, sizeof(FbxParts::Vertex)*vertexCount_);
	vertexBuffer_->Unlock();
}

void FbxParts::InitIndexBuffer(const FbxMesh *pMesh)
{
	pIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	pPolygonCountOfMaterial_ = new int[materialCount_];

	//マテリアルごとにインデックスバッファ作る
	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];

		int count = 0;
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);

			if (materialID == i)
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}

		//頂点3つでポリゴン1つ
		pPolygonCountOfMaterial_[i] = count / 3;

		//インデックスバッファ作成
		Direct3D::pDevice_->CreateIndexBuffer(sizeof(int)* indexCount_, 0, D3DFMT_INDEX32, D3DPOOL_MANAGED, &pIndexBuffer_[i], 0);
		DWORD *iCopy;
		pIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int)* indexCount_);
		pIndexBuffer_[i]->Unlock();

		delete[] indexList;
	}
}

D3DXVECTOR3 FbxParts::CalcTangent(
	D3DXVECTOR3 *pos0, D3DXVECTOR3 *pos1, D3DXVECTOR3 *pos2,
	D3DXVECTOR2 *uv0, D3DXVECTOR2 *uv1, D3DXVECTOR2 *uv2)
{
	D3DXVECTOR3 cp0[3] = {
		D3DXVECTOR3(pos0->x, uv0->x, uv0->y),
		D3DXVECTOR3(pos0->y, uv0->x, uv0->y),
		D3DXVECTOR3(pos0->z, uv0->x, uv0->y)
	};

	D3DXVECTOR3 cp1[3] = {
		D3DXVECTOR3(pos1->x, uv1->x, uv1->y),
		D3DXVECTOR3(pos1->y, uv1->x, uv1->y),
		D3DXVECTOR3(pos1->z, uv1->x, uv1->y)
	};

	D3DXVECTOR3 cp2[3] = {
		D3DXVECTOR3(pos2->x, uv2->x, uv2->y),
		D3DXVECTOR3(pos2->y, uv2->x, uv2->y),
		D3DXVECTOR3(pos2->z, uv2->x, uv2->y)
	};

	float f[3];
	for (int i = 0; i < 3; i++)
	{
		D3DXVECTOR3 v1 = cp1[i] - cp0[i];
		D3DXVECTOR3 v2 = cp2[i] - cp1[i];
		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &v1, &v2);

		f[i] = -cross.y / cross.x;
	}

	D3DXVECTOR3 tangent;
	memcpy(tangent, f, sizeof(float) * 3);
	D3DXVec3Normalize(&tangent, &tangent);
	return tangent;
}

void FbxParts::InitAnimation(const FbxMesh *pMesh)
{
	FbxDeformer *   pDeformer = pMesh->GetDeformer(0);
	if (pDeformer == nullptr)
	{
		return;
	}

	pSkinInfo_ = (FbxSkin *)pDeformer;

	//頂点からポリゴンを逆引きするための情報を作成する
	struct  POLY_INDEX
	{
		int *   polyIndex;
		int *   vertexIndex;
		int     numRef;
	};

	POLY_INDEX * polyTable = new POLY_INDEX[vertexCount_];
	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		//三角形ポリゴンに合わせて、頂点とポリゴンの関連情報を構築する
		polyTable[i].polyIndex = new int[polygonCount_ * 3];
		polyTable[i].vertexIndex = new int[polygonCount_ * 3];
		polyTable[i].numRef = 0;
		ZeroMemory(polyTable[i].polyIndex, sizeof(int)* polygonCount_ * 3);
		ZeroMemory(polyTable[i].vertexIndex, sizeof(int)* polygonCount_ * 3);

		//ポリゴン間で共有する頂点を列挙する
		for (DWORD k = 0; k < (unsigned int)polygonCount_; k++)
		{
			for (int m = 0; m < 3; m++)
			{
				if (pMesh->GetPolygonVertex(k, m) == i)
				{
					polyTable[i].polyIndex[polyTable[i].numRef] = k;
					polyTable[i].vertexIndex[polyTable[i].numRef] = m;
					polyTable[i].numRef++;
				}
			}
		}
	}

	numBone_ = pSkinInfo_->GetClusterCount();
	ppCluster_ = new FbxCluster*[numBone_];
	for (int i = 0; i < numBone_; i++)
	{
		ppCluster_[i] = pSkinInfo_->GetCluster(i);
	}

	//ボーンの数に合わせてウェイト情報を準備する
	pWeightArray_ = new FbxParts::Weight[vertexCount_];
	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		pWeightArray_[i].posOrigin = pVertexList_[i].pos;
		pWeightArray_[i].normalOrigin = pVertexList_[i].normal;
		pWeightArray_[i].pBoneIndex = new int[numBone_];
		pWeightArray_[i].pBoneWeight = new float[numBone_];
		for (int j = 0; j < numBone_; j++)
		{
			pWeightArray_[i].pBoneIndex[j] = -1;
			pWeightArray_[i].pBoneWeight[j] = 0.0f;
		}
	}

	//それぞれのボーンに影響を受ける頂点を調べる
	//そこから逆に、頂点ベースでボーンインデックス・重みを整頓する
	for (int i = 0; i < numBone_; i++)
	{
		int numIndex = ppCluster_[i]->GetControlPointIndicesCount();
		int * piIndex = ppCluster_[i]->GetControlPointIndices();
		double * pdWeight = ppCluster_[i]->GetControlPointWeights();

		//頂点側からインデックスをたどって、頂点サイドで整理する
		for (int k = 0; k < numIndex; k++)
		{
			// 頂点に関連付けられたウェイト情報がボーン５本以上の場合は、重みの大きい順に４本に絞る
			for (int m = 0; m < 4; m++)
			{
				if (m >= numBone_)
					break;

				if (pdWeight[k] > pWeightArray_[piIndex[k]].pBoneWeight[m])
				{
					for (int n = numBone_ - 1; n > m; n--)
					{
						pWeightArray_[piIndex[k]].pBoneIndex[n] = pWeightArray_[piIndex[k]].pBoneIndex[n - 1];
						pWeightArray_[piIndex[k]].pBoneWeight[n] = pWeightArray_[piIndex[k]].pBoneWeight[n - 1];
					}
					pWeightArray_[piIndex[k]].pBoneIndex[m] = i;
					pWeightArray_[piIndex[k]].pBoneWeight[m] = (float)pdWeight[k];
					break;
				}
			}

		}
	}

	//ボーンを生成
	pBoneArray_ = new FbxParts::Bone[numBone_];
	for (int i = 0; i < numBone_; i++)
	{
		FbxAMatrix  matrix;
		ppCluster_[i]->GetTransformLinkMatrix(matrix);

		for (DWORD x = 0; x < 4; x++)
		{
			for (DWORD y = 0; y < 4; y++)
			{
				pBoneArray_[i].bindPose(x, y) = (float)matrix.Get(x, y);
			}
		}
	}

	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		SAFE_DELETE_ARRAY(polyTable[i].polyIndex);
		SAFE_DELETE_ARRAY(polyTable[i].vertexIndex);
	}
	SAFE_DELETE_ARRAY(polyTable);
}

void FbxParts::DrawSkinAnime(const D3DXMATRIX &matrix, const FbxTime time, const IGameObject *myObject, const LPD3DXEFFECT pEffect)
{
	for (int i = 0; i < numBone_; i++)
	{
		FbxAnimEvaluator * evaluator = ppCluster_[i]->GetLink()->GetScene()->GetAnimationEvaluator();
		FbxMatrix mCurrentOrentation = evaluator->GetNodeGlobalTransform(ppCluster_[i]->GetLink(), time);

		for (DWORD x = 0; x < 4; x++)
		{
			for (DWORD y = 0; y < 4; y++)
			{
				pBoneArray_[i].newPose(x, y) = (float)mCurrentOrentation.Get(x, y);
			}
		}

		//オフセット時のポーズの差分を計算する
		D3DXMatrixInverse(&pBoneArray_[i].diffPose, nullptr, &pBoneArray_[i].bindPose);
		pBoneArray_[i].diffPose *= pBoneArray_[i].newPose;
	}

	//各ボーンに対応した頂点の変形制御
	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		//各頂点ごとに、「影響するボーン×ウェイト値」を反映させた関節行列を作成する
		D3DXMATRIX  matrix;
		ZeroMemory(&matrix, sizeof(matrix));
		for (int m = 0; m < numBone_; m++)
		{
			if (pWeightArray_[i].pBoneIndex[m] < 0)
			{
				break;
			}
			matrix += pBoneArray_[pWeightArray_[i].pBoneIndex[m]].diffPose * pWeightArray_[i].pBoneWeight[m];

		}

		//作成された関節行列を使って、頂点を変形する
		D3DXVECTOR3 Pos = pWeightArray_[i].posOrigin;
		D3DXVECTOR3 Normal = pWeightArray_[i].normalOrigin;
		D3DXVec3TransformCoord(&pVertexList_[i].pos, &Pos, &matrix);
		D3DXVec3TransformCoord(&pVertexList_[i].normal, &Normal, &matrix);

	}

	//頂点バッファをロックして、変形させた後の頂点情報で上書きする
	FbxParts::Vertex * pv;
	if (vertexBuffer_ != nullptr && SUCCEEDED(vertexBuffer_->Lock(0, 0, (void**)&pv, 0)))
	{
		memcpy(pv, pVertexList_, sizeof(FbxParts::Vertex)* vertexCount_);
		vertexBuffer_->Unlock();
	}

	Draw(matrix, myObject, pEffect);
}

void FbxParts::DrawMeshAnime(const D3DXMATRIX &matrix, const FbxTime time, FbxScene *scene, const IGameObject *myObject, const LPD3DXEFFECT pEffect)
{
	FbxAnimEvaluator *evaluator = scene->GetAnimationEvaluator();
	FbxMatrix mCurrentOrentation = evaluator->GetNodeGlobalTransform(pNode_, time);

	for (DWORD x = 0; x < 4; x++)
	{
		for (DWORD y = 0; y < 4; y++)
		{
			localMatrix_(x, y) = (float)mCurrentOrentation.Get(x, y);
		}
	}

	Draw(matrix, myObject, pEffect);
}

void FbxParts::Draw(const D3DXMATRIX &matrix, const IGameObject *myObject, const LPD3DXEFFECT pEffect)
{
	Direct3D::pDevice_->SetStreamSource(0, vertexBuffer_, 0, sizeof(FbxParts::Vertex));
	Direct3D::pDevice_->SetVertexDeclaration(verDec_);
	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &matrix);

	//マテリアルごとに描画
	for (int i = 0; i < materialCount_; i++)
	{
		if (pEffect)
		{
			pEffect->Begin(NULL, 0);

			pEffect->BeginPass(0);

			Direct3D::pDevice_->SetIndices(pIndexBuffer_[i]);
			Direct3D::pDevice_->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, pPolygonCountOfMaterial_[i]);

			//hlslにデータを渡す
			pEffect->SetVector("DIFFUSE_COLOR", &D3DXVECTOR4(
				pMaterial_[i].Diffuse.r,
				pMaterial_[i].Diffuse.g,
				pMaterial_[i].Diffuse.b,
				pMaterial_[i].Diffuse.a));

			pEffect->SetVector("AMBIENT_COLOR", &D3DXVECTOR4(
				pMaterial_[i].Ambient.r,
				pMaterial_[i].Ambient.g,
				pMaterial_[i].Ambient.b,
				pMaterial_[i].Ambient.a));

			pEffect->SetVector("SPECULER_COLOR", &D3DXVECTOR4(
				pMaterial_[i].Specular.r,
				pMaterial_[i].Specular.g,
				pMaterial_[i].Specular.b,
				pMaterial_[i].Specular.a));

			pEffect->SetFloat("SPECULER_POWER", pMaterial_[i].Power);
			pEffect->SetTexture("TEXTURE", pTexture_[i]);
			pEffect->SetBool("IS_TEXTURE", pTexture_[i] != nullptr);
			pEffect->SetTexture("NORMAL_MAP", pNormalMap_[i]);

			D3DXMATRIX proj;
			Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);

			D3DXMATRIX view;
			Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);

			D3DXMATRIX matWVP = myObject->GetWorldMatrix() * view * proj;

			//回転行列
			D3DXMATRIX rotateX, rotateY, rotateZ;
			D3DXVECTOR3 myObjectRotate = myObject->GetRotate();
			D3DXMatrixRotationX(&rotateX, D3DXToRadian(myObjectRotate.x));
			D3DXMatrixRotationY(&rotateY, D3DXToRadian(myObjectRotate.y));
			D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(myObjectRotate.z));

			//拡大縮小の逆行列
			D3DXMATRIX scale;
			D3DXVECTOR3 myObjectScale = myObject->GetScale();
			D3DXMatrixScaling(&scale, myObjectScale.x, myObjectScale.y, myObjectScale.z);
			D3DXMatrixInverse(&scale, nullptr, &scale);

			D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;

			pEffect->SetMatrix("WVP", &matWVP);
			pEffect->SetMatrix("RS", &mat);
			pEffect->SetMatrix("W", &myObject->GetWorldMatrix());

			D3DLIGHT9 lightState;
			Direct3D::pDevice_->GetLight(0, &lightState);
			pEffect->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

			pEffect->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0.0f, 60.0f, -135.0f));

			pEffect->EndPass();
			pEffect->End();
		}
		//DirectXのシェーダーにお任せ
		else
		{
			Direct3D::pDevice_->SetIndices(pIndexBuffer_[i]);
			Direct3D::pDevice_->SetTexture(0, pTexture_[i]);
			Direct3D::pDevice_->SetMaterial(&pMaterial_[i]);
			Direct3D::pDevice_->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, pPolygonCountOfMaterial_[i]);
		}
	}
}

const bool FbxParts::GetBonePosition(const std::string boneName, D3DXVECTOR3 *position) const
{
	for (int i = 0; i < numBone_; i++)
	{
		if (boneName == ppCluster_[i]->GetLink()->GetName())
		{
			FbxAMatrix  matrix;
			ppCluster_[i]->GetTransformLinkMatrix(matrix);

			position->x = (float)matrix[3][0];
			position->y = (float)matrix[3][1];
			position->z = (float)matrix[3][2];

			return true;
		}

	}

	return false;
}

void FbxParts::RayCast(RayCastData *data)
{
	Vertex *vCopy;
	vertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	for (DWORD i = 0; i < (unsigned int)materialCount_; i++)
	{
		DWORD *iCopy;
		pIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		for (DWORD j = 0; j < (unsigned int)pPolygonCountOfMaterial_[i]; j++)
		{
			D3DXVECTOR3 ver[3];
			ver[0] = vCopy[iCopy[j * 3 + 0]].pos;
			ver[1] = vCopy[iCopy[j * 3 + 1]].pos;
			ver[2] = vCopy[iCopy[j * 3 + 2]].pos;

			BOOL  hit;
			float dist;

			hit = D3DXIntersectTri(&ver[0], &ver[1], &ver[2],
				&data->start, &data->dir, NULL, NULL, &dist);

			if (hit && dist < data->dist)
			{
				data->hit = TRUE;
				data->dist = dist;
			}
		}

		pIndexBuffer_[i]->Unlock();
	}

	vertexBuffer_->Unlock();
}
