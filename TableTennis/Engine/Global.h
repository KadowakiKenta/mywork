//このヘッダーをインクルードすると下記の物がすべて使える

#pragma once
#include <d3dx9.h>
#include <assert.h>
#include <iostream>
#include "IGameObject.h"
#include "SceneManager.h"
#include "Input.h"
#include "BoxCollider.h"
#include "SphereCollider.h"
#include "Direct3D.h"

using namespace std;

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete [] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

//画像・FBXをロードする際、この値が入ることになる
//この値のままだとエラー扱い
const int ASSERT_ERROR = -1;

struct Global
{
	double screenWidth;
	double screenHeight;
};
extern Global g;
