#include "Fbx.h"
#include "FbxParts.h"
#include "Direct3D.h"

Fbx::Fbx() :
	animSpeed_(0), pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr)
{

}

Fbx::~Fbx()
{
	for (int i = 0; (unsigned int)i < parts_.size(); i++)
	{
		SAFE_DELETE(parts_[i]);
	}
	parts_.clear();

	for (auto it = pTexture_.begin(); it != pTexture_.end(); it++)
	{
		SAFE_RELEASE(it->second);
	}
	pTexture_.clear();

	pScene_->Destroy();
	pManager_->Destroy();
}

HRESULT Fbx::Load(const std::string fileName)
{
	pManager_ = FbxManager::Create();
	pImporter_ = FbxImporter::Create(pManager_, "");
	pScene_ = FbxScene::Create(pManager_, "");

	if (pImporter_->Initialize(fileName.c_str()) == false)
	{
		return E_FAIL;
	}

	pImporter_->Import(pScene_);

	pImporter_->Destroy();

	frameRate_ = pScene_->GetGlobalSettings().GetTimeMode();

	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	char dir[MAX_PATH];
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);

	FbxNode* rootNode = pScene_->GetRootNode();

	int childCount = rootNode->GetChildCount();

	for (int i = 0; childCount > i; i++)
	{
		CheckNode(rootNode->GetChild(i), &parts_);
	}

	SetCurrentDirectory(defaultCurrentDir);

	return S_OK;
}

void Fbx::CheckNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList)
{
	//ノード内のメッシュ情報確認
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr != nullptr && attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		FbxParts* pParts = new FbxParts;

		pParts->InitMaterial(pNode, this);

		pParts->InitVertexBuffer(pNode->GetMesh());

		pParts->InitIndexBuffer(pNode->GetMesh());

		pParts->InitAnimation(pNode->GetMesh());

		pPartsList->push_back(pParts);
	}

	//子ノード
	{
		int childCount = pNode->GetChildCount();

		for (int i = 0; i < childCount; i++)
		{
			CheckNode(pNode->GetChild(i), pPartsList);
		}
	}
}


void Fbx::Draw(const D3DXMATRIX matrix, const int frame, const IGameObject *myObject, const LPD3DXEFFECT pEffect)
{
	for (int k = 0; (unsigned int)k < parts_.size(); k++)
	{
		FbxTime     time;
		time.SetTime(0, 0, 0, frame, 0, 0, frameRate_);


		//スキン
		if (parts_[k]->GetSkinInfo() != nullptr)
		{
			parts_[k]->DrawSkinAnime(matrix, time, myObject, pEffect);
		}

		//メッシュ
		else
		{
			parts_[k]->DrawMeshAnime(matrix, time, pScene_, myObject, pEffect);
		}
	}
}

void Fbx::SetAnimFrame(const int startFrame, const int endFrame, const float speed)
{
	startFrame_ = startFrame;
	endFrame_ = endFrame;
	animSpeed_ = speed;
}

LPDIRECT3DTEXTURE9 Fbx::LoadTexture(const std::string fileName)
{
	auto it = pTexture_.find(fileName);

	if (it == pTexture_.end())
	{
		//ファイル名+拡張
		char name[_MAX_FNAME];
		char ext[_MAX_EXT];
		_splitpath_s(fileName.c_str(), nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
		wsprintf(name, "%s%s", name, ext);

		//画像ファイルを読み込んでテクスチャ作成
		LPDIRECT3DTEXTURE9 pTexture;
		HRESULT hr = D3DXCreateTextureFromFileEx(Direct3D::pDevice_, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE, D3DX_DEFAULT, 0, nullptr, nullptr, &pTexture);

		if (FAILED(hr))
		{
			std::string message = "テクスチャ「";
			message += name;
			message += "」が開けません。\nFBXファイルと同じ場所にありますか？";

			MessageBox(NULL, message.c_str(), "BaseProjDx9エラー", MB_OK);
		}

		pTexture_.insert(std::make_pair(fileName, pTexture));

		return pTexture;
	}

	return it->second;
}

const D3DXVECTOR3 Fbx::GetBonePosition(const std::string boneName) const
{
	D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0);
	for (int i = 0; (unsigned int)i < parts_.size(); i++)
	{
		if (parts_[i]->GetBonePosition(boneName, &position))
			break;
	}


	return position;
}

void Fbx::RayCast(RayCastData * data)
{
	for (int i = 0; (unsigned int)i < parts_.size(); i++)
	{
		parts_[i]->RayCast(data);
	}
}
