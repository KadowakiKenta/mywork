#include "SceneManager.h"
#include "../TitleScene.h"
#include "../PlayScene.h"
#include "../OperationExplainScene.h"

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_TITLE;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_TITLE;
IGameObject *SceneManager::pCurrentScene_ = nullptr;
bool SceneManager::sceneReload_ = false;

SceneManager::SceneManager(IGameObject *parent)
	:IGameObject(parent, "SceneManager")
{
}

SceneManager::~SceneManager()
{
}

void SceneManager::Initialize()
{
	pCurrentScene_ = CreateGameObject<TitleScene>(this);
}

void SceneManager::Update()
{
	if (currentSceneID_ != nextSceneID_ || sceneReload_ == true)
	{
		for (auto scene = childList_.begin(); scene != childList_.end(); scene++)
		{
			(*scene)->ReleaseSub();
			SAFE_DELETE(*scene);
		}
		childList_.clear();

		switch (nextSceneID_)
		{
		case SCENE_ID_TITLE:
			pCurrentScene_ = CreateGameObject<TitleScene>(this);
			break;

		case SCENE_ID_OPERATION_EXPLAIN:
			pCurrentScene_ = CreateGameObject<OperationExplainScene>(this);
			break;

		case SCENE_ID_PLAY:
			pCurrentScene_ = CreateGameObject<PlayScene>(this);
			break;
		}

		currentSceneID_ = nextSceneID_;

		//trueでもう一度通らないように
		sceneReload_ = false;
	}
}

void SceneManager::Draw()
{
}

void SceneManager::Release()
{
}

void SceneManager::ChangeSceneID(const SCENE_ID next)
{
	nextSceneID_ = next;
}

void SceneManager::ReloadScene()
{
	sceneReload_ = true;
}
