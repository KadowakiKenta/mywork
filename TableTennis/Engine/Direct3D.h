#pragma once
#include "Global.h"

//Direct3D�̑匳
namespace Direct3D
{
	extern LPDIRECT3D9 pD3d_;
	extern LPDIRECT3DDEVICE9 pDevice_;

	void Initialize(const HWND hWnd);

	void BeginDraw();

	void EndDraw();

	void Release();
}