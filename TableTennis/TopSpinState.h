#pragma once
#include "RacketState.h"

//上回転の状態を管理するクラス
class TopSpinState : public RacketState
{
public:
	~TopSpinState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};