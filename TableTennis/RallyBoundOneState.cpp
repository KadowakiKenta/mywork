#include "RallyBoundOneState.h"
#include "RallyBoundZeroState.h"
#include "Ball.h"
#include "Referee.h"
#include "Racket.h"

RallyBoundOneState::RallyBoundOneState() :
	isRallyContinue_(false)
{
}

unique_ptr<RefereeState> RallyBoundOneState::HandleInput(Referee *thisptr) const
{
	if (isRallyContinue_)
	{
		return make_unique<RallyBoundZeroState>();
	}
	else
	{
		return PointGetChangeState::HandleInput(thisptr);
	}

	return nullptr;
}

void RallyBoundOneState::Update(Referee *thisptr)
{
	std::vector<Racket*> pRacketArray = thisptr->GetRacketArray();

	//次打つべき人が打つ
	for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
	{
		if ((*it)->GetIsBallHitNowFrame())
		{
			ShouldHitProcess(it, thisptr);
		}
	}

	//バウンド判定
	const Ball* pBall = thisptr->GetBallPointer();
	if ((pBall != nullptr) && (pBall->GetIsBound()))
	{
		for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
		{
			if (TwoBoundPointProcess(thisptr, it, pBall))
			{
				break;
			}
		}
	}
}

void RallyBoundOneState::Enter(Referee *thisptr)
{
	thisptr->SetRefereeState(STATE_RALLY_BOUND_ONE);
}

void RallyBoundOneState::ShouldHitProcess(std::vector<Racket*>::iterator &it, Referee *thisptr)
{
	Racket* pHitRacket = (*it);
	Racket* pShouldHitRacket = thisptr->GetRacketShouldHitArray().front();

	if (pHitRacket == pShouldHitRacket)
	{
		isRallyContinue_ = true;
		thisptr->FromFrontToBackRacketShouldHitArray();
	}
	else
	{
		if ((*it)->GetMyCourt() == COURT_SERVER)
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_RECEIVER_COURT);
		}
		else if ((*it)->GetMyCourt() == COURT_RECEIVER)
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_SERVER_COURT);
		}
	}
}

bool RallyBoundOneState::TwoBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it, const Ball *pBall)
{
	if ((*it)->GetIsBallHit())
	{
		if ((*it)->GetMyCourt() == COURT_SERVER)
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_SERVER_COURT);
		}
		else if ((*it)->GetMyCourt() == COURT_RECEIVER)
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_RECEIVER_COURT);
		}

		return true;
	}

	return false;
}