#include "WaitBallState.h"
#include <vector>
#include "ServeBoundZeroState.h"
#include "Racket.h"
#include "Referee.h"
#include "Ball.h"
#include "HitBallServeState.h"
#include "RacketState.h"

unique_ptr<RefereeState> WaitBallState::HandleInput(Referee *thisptr) const
{
	std::vector<Racket*> pRacketArray = thisptr->GetRacketArray();
	for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
	{
		if (typeid((*it)->GetRacketState()) == typeid(HitBallServeState))
		{
			return make_unique<ServeBoundZeroState>();
		}
	}

	return PointGetChangeState::HandleInput(thisptr);
}

void WaitBallState::Update(Referee *thisptr)
{
	//バウンド判定
	const Ball* pBall = thisptr->GetBallPointer();
	if ((pBall != nullptr) && (pBall->GetIsBound()))
	{
		std::vector<Racket*> pRacketArray = thisptr->GetRacketArray();
		for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
		{
			if (WaitBallBoundPointProcess(thisptr, it))
			{
				break;
			}
		}
	}
}

void WaitBallState::Enter(Referee *thisptr)
{
	thisptr->SetRefereeState(STATE_WAIT_BALL);
	thisptr->SetPointGetChangeState(AFTER_SCORE_NORMAL);	
	thisptr->ResetRacketOfPosToServe();
}

bool WaitBallState::WaitBallBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it)
{
	if ((*it)->GetIsServer())
	{
		if ((*it)->GetMyCourt() == COURT_SERVER)
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_RECEIVER_COURT);
		}
		else if ((*it)->GetMyCourt() == COURT_RECEIVER)
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_SERVER_COURT);
		}

		return true;
	}

	return false;
}