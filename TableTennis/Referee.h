#pragma once
#include <vector>
#include <memory>
#include "Engine/IGameObject.h"
#include "RefereeEnum.h"

class RefereeState;
class Text;
class Racket;
class Ball;

//審判を管理するクラス
class Referee : public IGameObject
{
	int hPictJdge_[PICT_JUDG_MAX];
	int hPictCountDown_[PICT_CD_MAX];
	int hPictPointGet_[PICT_PG_MAX];
	int hPictNumber_[PICT_NUM_MAX];
	int hPictPlayUI_[PICT_PUI_MAX];

	std::vector<Racket*> pRacketArray_;
	std::list<Racket*> pRacketShouldHitArray_;
	std::unique_ptr<RefereeState> pRefereeState_;

	GAME_MODE gameMode_;
	PICT_JUDG pictJudg_;
	TO_SERVE toServe_;
	SCORE_PLAYER_TO scorePlayerTo_;
	AFTER_SCORE_STATE afterScoreState_;
	REFEREE_STATE refereeState_;
	PICT_COUNT_DOWN pictCD_;
	
	unsigned int serverCourtPoint_;
	unsigned int receiverCourtPoint_;
	unsigned int countTimer_;

	bool ballKillFlag_;		//ポイント変動時にボールを削除する

	void LoadToUI();

	void HandleInput();
	
	void ChangeServe();

	void ChangeServeOfProcess();

	void ServeModeDeuceProcess();

	void ServeModeNormalProcess();

	void GameFinishProcess();

	void RetryTableTennis();

	void WinLoseChangePict();

	void PlayUITopDraw();

	void PlayUICenterDraw();

public:
	Referee(IGameObject *parent);
	~Referee();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;

	void ResetRacketOfPosToServe();

	void ScorePlayerToProcess(const SCORE_PLAYER_TO pointGet);

	void FromFrontToBackRacketShouldHitArray();

	void ReloadPlayScene() const;

	const REFEREE_STATE GetRefereeState() const
	{
		return refereeState_;
	}

	const std::vector<Racket*> GetRacketArray() const
	{
		return pRacketArray_;
	}

	const std::list<Racket*> GetRacketShouldHitArray() const
	{
		return pRacketShouldHitArray_;
	}

	const Ball* GetBallPointer() const
	{
		return (Ball*)FindObject("Ball");
	}

	const AFTER_SCORE_STATE GetPointGetChangeState() const
	{
		return afterScoreState_;
	}

	const bool GetBallKillFlag() const
	{
		return ballKillFlag_;
	}

	//Racket.cppのOnCollision()で使われる
	void SetRacketArray(std::vector<Racket*> pRacketArray)
	{
		pRacketArray_ = pRacketArray;
	}

	//WaitBallStateで、ノーマルに変更する
	void SetPointGetChangeState(const AFTER_SCORE_STATE afterScoreState)
	{
		afterScoreState_ = afterScoreState;
	}

	//RefereeStateのステート変更をこっちでも確認できるようにする
	void SetRefereeState(const REFEREE_STATE refereeState)
	{
		refereeState_ = refereeState;
	}

	//試合開始のカウントダウン時に画像を変更する
	void SetPictCD(const PICT_COUNT_DOWN pictCD)
	{
		pictCD_ = pictCD;
	}
};