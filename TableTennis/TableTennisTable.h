#pragma once
#include "Engine/IGameObject.h"

//�싅����Ǘ�����N���X
class TableTennisTable : public IGameObject
{
	int hModel_;

	LPD3DXEFFECT pEffect_;

public:
	TableTennisTable(IGameObject *parent);
	~TableTennisTable();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;

	const int GetModelHandle() const
	{
		return hModel_;
	};
};