#pragma once
#include <chrono>
#include "Engine/IGameObject.h"

class SphereCollider;
class Referee;
class AiRacket;

enum COLLISION_BALL
{
	COLLISION_BALL_NO,
	COLLISION_BALL_USER_RACKET,
	COLLISION_BALL_AI_RACKET,
	COLLISION_BALL_GROUND,
	COLLISION_BALL_SERVER_COURT,
	COLLISION_BALL_RECEIVER_COURT
};

//ボールを管理するクラス
class Ball : public IGameObject
{
	enum MODEL_NAME
	{
		MODEL_BALL,
		MODEL_SHADOW,
		MODEL_MAX
	};

	int hModel_[MODEL_MAX];

	std::chrono::system_clock::time_point afterBoundTime_;
	std::chrono::system_clock::time_point afterFallTime_;
	std::chrono::system_clock::time_point nowTime_;		

	float elapsedTime_;

	bool isCollision_;
	bool isFallReset_;
	bool isGravity_;
	bool isBound_;

	D3DXVECTOR3 ballSpeed_;

	float gravity_;
	float reboundSpeedY_;

	D3DXVECTOR3 beforePos_;
	D3DXVECTOR3 moveBallToNowFrame_;

	D3DXMATRIX shadowMatrix_;

	SphereCollider* BallCollider_;

	COLLISION_BALL frameOnlyCollisionBall_;

	LPD3DXEFFECT pEffect_;

	void MoveBall();

	void GravityClac(const std::chrono::system_clock::time_point beforeTime);

	void CanInFall();

	//審判側にボールを消してもらう
	void CanKillMe();

	void TableTennisTableOnCollision(const IGameObject *pTarget);
	void RacketOnCollision(const IGameObject *pTarget);

	void PosOfShadow();

public:
	Ball(IGameObject *parent);
	~Ball();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
	void OnCollision(const IGameObject *pTarget) override;

	const COLLISION_BALL GetCollisionBall() const
	{
		return frameOnlyCollisionBall_;
	}

	const bool GetIsBound() const
	{
		return isBound_;
	}

	//ボールを打つための超重要な機能
	void BallReboundDirection(const D3DXVECTOR3 racketVector);

	//ThrowBallServeState.cppで使用される
	void ServeBallSpeed()
	{
		reboundSpeedY_ = 2.0f;
	}

	//Referee.cppでサーブの構え中falseにするために使う
	void SetGravityFlag(const bool gravityFlag)
	{
		isGravity_ = gravityFlag;
	}
};