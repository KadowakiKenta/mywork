#pragma once
#include "Engine/Global.h"

//操作説明シーンを管理するクラス
class OperationExplainScene : public IGameObject
{
public:
	OperationExplainScene(IGameObject *parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};