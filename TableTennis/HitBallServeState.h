#pragma once
#include "RacketState.h"

//サーブ時にボールを打つ際の状態を管理するクラス
class HitBallServeState : public RacketState
{
public:
	~HitBallServeState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};