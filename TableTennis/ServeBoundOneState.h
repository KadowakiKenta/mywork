#pragma once
#include "PointGetChangeState.h"

class Racket;

//サーブの1バウンド目の状態を管理するクラス
class ServeBoundOneState : public PointGetChangeState
{
	bool OneBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it, const Ball *pBall);

public:
	~ServeBoundOneState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;
};