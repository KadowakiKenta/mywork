#pragma once
#include "Engine/IGameObject.h"

//操作説明のロゴを管理するクラス
class OperationExplainLogo : public IGameObject
{
	enum PICTURE
	{
		PICT_OPERATION_EXPLAIN,
		PICT_SPACE_PUT_PLAY,
		PICT_NOW_LOADING,
		PICT_MAX
	};
	int hPict_[PICT_MAX];

	enum STATE
	{
		STATE_OPERATION_EXPLAIN_DRAW,
		STATE_SPACE_PUT_PLAY_DRAW,
		STATE_NOW_LOADING_DRAW
	} state_;

	float logoOperationExplainAlpha_;
	float logoPushSpaceButtonAlpha_;

	double timeRecord_;
	bool timeResetFlag_;

	void AlgorithmProcess();
	
	void SpaceKeyPushProcess();

public:
	OperationExplainLogo(IGameObject *parent);
	~OperationExplainLogo();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};