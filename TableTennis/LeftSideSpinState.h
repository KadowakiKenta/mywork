#pragma once
#include "RacketState.h"

//左回転の状態を管理するクラス
class LeftSideSpinState : public RacketState
{
public:
	~LeftSideSpinState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};