#include "LobbingState.h"
#include "Engine/Model.h"
#include "Racket.h"
#include "StandardState.h"

const float FIRST_ANIMATION = 120.0f;
const float LAST_ANIMATION = 149.0f;

unique_ptr<RacketState> LobbingState::HandleInput(Racket *thisptr) const
{
	//アニメーションが再生しきったら
	float frame = Model::GetAnimFrame(thisptr->GetModelHandle());
	if (LAST_ANIMATION < frame)
	{
		return make_unique<StandardState>();
	}

	return nullptr;
}

void LobbingState::Update(Racket *thisptr)
{
}

void LobbingState::Enter(Racket *thisptr)
{
	Model::SetAnimFrame(thisptr->GetModelHandle(), FIRST_ANIMATION, LAST_ANIMATION + ANIM_WHITESPACE_FRAME, ANIM_SPEED);

	thisptr->Lobbing();
}
