#include "TitleScene.h"
#include "BackGround2D.h"
#include "TitleText.h"

TitleScene::TitleScene(IGameObject *parent):
	IGameObject(parent, "TitleScene")
{
}

void TitleScene::Initialize()
{
	CreateGameObject<BackGround2D>(this);
	CreateGameObject<TitleText>(this);
}

void TitleScene::Update()
{
}

void TitleScene::Draw()
{
}

void TitleScene::Release()
{
}