#pragma once
#include "Engine/Global.h"

class Camera;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	Camera* pCamera_;

public:
	PlayScene(IGameObject *parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};