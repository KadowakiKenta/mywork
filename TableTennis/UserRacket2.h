#pragma once
#include "Racket.h"

//人側(向こう側)のラケットを管理するクラス
//AI側を開発するまではお世話になります
//対人戦形式にしてもいいと思いますが・・・時間がないので出来なさそうです
class UserRacket2 : public Racket
{
public:
	UserRacket2(IGameObject *parent);
	~UserRacket2();

	//trueなら、そのボタンがオンという事を示す
	bool ButtonFrontMove() override;
	bool ButtonBackMove() override;
	bool ButtonLeftMove() override;
	bool ButtonRightMove() override;
	bool ButtonTopSpin() override;
	bool ButtonBackSpin() override;
	bool ButtonLeftSideSpin() override;
	bool ButtonRightSideSpin() override;
	bool ButtonLobbing() override;
	bool ButtonSmash() override;
};