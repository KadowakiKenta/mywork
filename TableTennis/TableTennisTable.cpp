#include "TableTennisTable.h"
#include "Engine/Model.h"

const D3DXVECTOR3 TABLE_COLLIDER_SIZE = D3DXVECTOR3(150.0f, 5.0f, 275.0f);
const D3DXVECTOR3 GROUND_POS = D3DXVECTOR3(0, -140.0f, 0);
const D3DXVECTOR3 GROUND_SIZE = D3DXVECTOR3(10000.0f, 50.0f, 10000.0f);

TableTennisTable::TableTennisTable(IGameObject *parent):
	IGameObject(parent, "TableTennisTable"),
	hModel_(-1),
	pEffect_(nullptr)
{
}

TableTennisTable::~TableTennisTable()
{
}

void TableTennisTable::Initialize()
{
	hModel_ = Model::Load("data/Model/Table.fbx");
	assert(hModel_ > ASSERT_ERROR);

	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "CommonShader.hlsl", NULL, NULL, D3DXSHADER_DEBUG,
		NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}
	assert(pEffect_ != nullptr);

	//特典判定に使うため、2つのテーブルコライダーを生成しています
	BoxCollider* receiverCourtCollider = new BoxCollider("ReceiverCourtCollider", D3DXVECTOR3(0, 0, TABLE_COLLIDER_SIZE.z / 4), D3DXVECTOR3(
		TABLE_COLLIDER_SIZE.x, TABLE_COLLIDER_SIZE.y, TABLE_COLLIDER_SIZE.z / 2));
	AddCollider(receiverCourtCollider);

	BoxCollider* serverCourtCollider = new BoxCollider("ServerCourtCollider", D3DXVECTOR3(0, 0, -(TABLE_COLLIDER_SIZE.z / 4)), D3DXVECTOR3(
		TABLE_COLLIDER_SIZE.x, TABLE_COLLIDER_SIZE.y, TABLE_COLLIDER_SIZE.z / 2));
	AddCollider(serverCourtCollider);

	BoxCollider* groundCollider = new BoxCollider("GroundCollider", GROUND_POS, GROUND_SIZE);
	AddCollider(groundCollider);
}

void TableTennisTable::Update()
{
}

void TableTennisTable::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_, this, pEffect_);
}

void TableTennisTable::Release()
{
}