#include "PointGetState.h"
#include "Engine/SceneManager.h"
#include "Referee.h"
#include "WaitBallState.h"
#include "GameFinishState.h"

const unsigned int GAME_START_PROCESS = 120;

PointGetState::PointGetState() :
	countTimer_(0)
{
}

unique_ptr<RefereeState> PointGetState::HandleInput(Referee *thisptr) const
{
	if (countTimer_ == GAME_START_PROCESS)
	{
		switch (thisptr->GetPointGetChangeState())
		{
		case AFTER_SCORE_CONTINUE:
			return make_unique<WaitBallState>();
			break;
		case AFTER_SCORE_FINISH:
			return make_unique<GameFinishState>();
			break;
		}
		
	}

	return nullptr;
}

void PointGetState::Update(Referee *thisptr)
{
	countTimer_++;
}

void PointGetState::Enter(Referee *thisptr)
{
	thisptr->SetRefereeState(STATE_POINT_GET);
}