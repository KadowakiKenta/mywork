#include "PointGetChangeState.h"
#include "Referee.h"
#include "PointGetState.h"

unique_ptr<RefereeState> PointGetChangeState::HandleInput(Referee *thisptr) const
{
	if (thisptr->GetPointGetChangeState() != AFTER_SCORE_NORMAL)
	{
		return make_unique<PointGetState>();
	}

	return nullptr;
}