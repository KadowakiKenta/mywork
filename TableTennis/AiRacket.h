#pragma once
#include "Racket.h"

//AIのラケットを管理するクラス
class AiRacket : public Racket
{
public:
	AiRacket(IGameObject *parent);
	~AiRacket();
};