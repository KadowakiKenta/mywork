#pragma once
#include "PointGetChangeState.h"

class Racket;

//サーブの0バウンド目の状態を管理するクラス
class ServeBoundZeroState : public PointGetChangeState
{
	bool ZeroBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it, const Ball *pBall);

public:
	~ServeBoundZeroState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;

};