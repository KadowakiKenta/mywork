#include "Racket.h"
#include "RacketState.h"
#include "StandardState.h"
#include "StanceServeState.h"
#include "Referee.h"
#include "Ball.h"
#include "Engine/Model.h"
#include "Ball.h"

const float SERVE_MOVE = 1.5f;
const float POSITION_MOVE = 1.5f;

const D3DXVECTOR3 COLLIDER_SCALE = D3DXVECTOR3(50.0f, 150.0f, 50.0f);
const D3DXVECTOR3 RACKET_FRONT_VEC = D3DXVECTOR3(0, 0, 50.0f);

const D3DXVECTOR3 POWER_TABLE_HEAD_BALL = D3DXVECTOR3(0, 0.15f, 0.05f);
const D3DXVECTOR3 LOBBING = D3DXVECTOR3(0, 3.0f, 2.0f);
const D3DXVECTOR3 SMASH = D3DXVECTOR3(0, 0.0f, 5.0f);

const float FRONT_BALL_POS = 10.0f;

const float HIT_FRONT = 0.1f;
const float HIT_BACK = 0.1f;
const float HIT_LEFT = -0.3f;
const float HIT_CENTER = 0.0f;
const float HIT_RIGHT = 0.3f;

const float THAT_WAY_ROTATE = 0.0f;
const float THIS_WAY_ROTATE = 180.0f;

const D3DXVECTOR3 INIT_SERVER_COURT_POSITION = D3DXVECTOR3(0, 15.0f, -150.0f);
const D3DXVECTOR3 INIT_RECEIVER_COURT_POSITION = D3DXVECTOR3(0, 15.0f, 170.0f);

Racket::Racket(IGameObject *parent, const COURT courtPos):
	Racket(parent, "Racket", courtPos)
{
}

Racket::Racket(IGameObject *parent, const std::string name, const COURT courtPos) :
	IGameObject(parent, name),
	hModel_(ASSERT_ERROR),
	pRacketState_(make_unique<StandardState>()),
	pEnemyRacket_(nullptr), hitLeftRightVec_(0.0f),
	pEffect_(nullptr),
	isBallHit_(false),
	isBallHitFrameOnly_(false),
	isServer_(false),
	myCourt_(courtPos),
	savePosHitBallBefore_(D3DXVECTOR3(0, 0, 0)),
	swingPow_(D3DXVECTOR3(0, 0, 0)),
	hitFrontBackVec_(0.0f)
{
}

Racket::~Racket()
{
	SAFE_RELEASE(pEffect_);
}

void Racket::Initialize()
{
	hModel_ = Model::Load("data/Model/Racket.fbx");
	assert(hModel_ > ASSERT_ERROR);

	BoxCollider* RacketCollider = new BoxCollider("RacketCollider", D3DXVECTOR3(0, 0, 0), COLLIDER_SCALE);
	AddCollider(RacketCollider);

	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "CommonShader.hlsl", NULL, NULL, D3DXSHADER_DEBUG,
		NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}
	assert(pEffect_ != nullptr);

	pRacketState_->Enter(this);

	InitRacketOfPosAndRot();
}


void Racket::Update()
{
	//0にして、衝突判定に通らないようにする
	swingPow_ = D3DXVECTOR3(0, 0, 0);

	IHitBallProcess();

	HitDivide();

	pRacketState_->Update(this);
	HandleInput();
}

void Racket::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_, this, pEffect_);
}

void Racket::Release()
{
}

void Racket::InitRacketOfPosAndRot()
{
	if (myCourt_ == COURT_SERVER)
	{
		position_ = INIT_SERVER_COURT_POSITION;
		rotate_.y = THAT_WAY_ROTATE;
	}
	else if (myCourt_ == COURT_RECEIVER)
	{
		position_ = INIT_RECEIVER_COURT_POSITION;
		rotate_.y = THIS_WAY_ROTATE;
	}
}

void Racket::IHitBallProcess()
{
	if (isBallHitFrameOnly_)
	{
		Referee* pReferee = (Referee*)FindObject("Referee");
		if (pReferee != nullptr)
		{
			vector<Racket*> pRacketArray = pReferee->GetRacketArray();

			for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
			{
				(*it)->SetIsBallHitToFalse();
			}

			pReferee->SetRacketArray(pRacketArray);
		}

		isBallHit_ = true;

		isBallHitFrameOnly_ = false;
	}
}

void Racket::OnCollision(const IGameObject *pTarget)
{
	if ( (pTarget->GetName() == "Ball") && (swingPow_ != D3DXVECTOR3(0, 0, 0)))
	{
		Ball *pBall = (Ball*)pTarget;

		pBall->BallReboundDirection(swingPow_);

		isBallHitFrameOnly_ = true;

		SetSavePosition();
		SetPositionToBallPos();
	}
}

void Racket::HandleInput()
{
	unique_ptr<RacketState> pRacketState = pRacketState_->HandleInput(this);

	if (pRacketState != nullptr)
	{
		pRacketState_ = move(pRacketState);

		pRacketState_->Enter(this);
	}
}

void Racket::HitDivide()
{
	if (ButtonFrontMove())
	{
		hitFrontBackVec_ = HIT_FRONT;
	}
	else if (ButtonBackMove())
	{
		hitFrontBackVec_ = HIT_BACK;
	}

	if (ButtonLeftMove())
	{
		hitLeftRightVec_ = HIT_LEFT;
	}
	else if (ButtonRightMove())
	{
		hitLeftRightVec_ = HIT_RIGHT;
	}
	else
	{
		hitLeftRightVec_ = HIT_CENTER;
	}
}

void Racket::ServeFrontMove()
{
	position_.z += SERVE_MOVE;
}

void Racket::ServeBackMove()
{
	position_.z -= SERVE_MOVE;
}

void Racket::ServeLeftMove()
{
	position_.x -= SERVE_MOVE;
}

void Racket::ServeRightMove()
{
	position_.x += SERVE_MOVE;
}

void Racket::ServeBallHitSpeed()
{
	Ball *pBall = (Ball*)FindObject("Ball");
	if (pBall != nullptr)
	{
		//前方向のベクトル - ボールへ向かうベクトル 
		// = 卓球台に自動的に向かうベクトルなのでは？
		D3DXVECTOR3 racketFrontVec = RACKET_FRONT_VEC;

		D3DXVECTOR3 ballHeadRacketVec = pBall->GetPosition() - position_;

		D3DXVECTOR3 tableHeadBallVec = racketFrontVec - ballHeadRacketVec;

		tableHeadBallVec.y *= POWER_TABLE_HEAD_BALL.y;
		tableHeadBallVec.z *= POWER_TABLE_HEAD_BALL.z;

		pBall->BallReboundDirection(D3DXVECTOR3(hitLeftRightVec_, tableHeadBallVec.y,
			(tableHeadBallVec.z + hitFrontBackVec_) * myCourt_));

		isBallHitFrameOnly_ = true;
	}
}

void Racket::RallyFrontMove()
{
	position_.z += POSITION_MOVE;
}

void Racket::RallyBackMove()
{
	position_.z -= POSITION_MOVE;
}

void Racket::RallyLeftMove()
{
	position_.x -= POSITION_MOVE;
}

void Racket::RallyRightMove()
{
	position_.x += POSITION_MOVE;
}

void Racket::Lobbing()
{
	//ボールの打つ強さの書き方はコレを基準
	swingPow_ = D3DXVECTOR3(hitLeftRightVec_, LOBBING.y,
		(LOBBING.z + hitFrontBackVec_) * myCourt_);
}

void Racket::Smash()
{
	swingPow_ = D3DXVECTOR3(hitLeftRightVec_, SMASH.y,
		(SMASH.z + hitFrontBackVec_) * myCourt_);
}

void Racket::BallCreate()
{
	CreateGameObject<Ball>(pParent_);
}

void Racket::FrontBallPos()
{
	Ball *pBall = (Ball*)FindObject("Ball");
	if (pBall != nullptr)
	{
		pBall->SetPosition(D3DXVECTOR3(position_.x, position_.y, position_.z + (FRONT_BALL_POS * myCourt_)));

		pBall->SetGravityFlag(false);
	}
}

void Racket::BallServe()
{
	Ball *pBall = (Ball*)FindObject("Ball");
	if (pBall != nullptr)
	{
		pBall->ServeBallSpeed();
	}
}

void Racket::SetServerRacketState()
{
	pRacketState_ = make_unique<StanceServeState>();
	pRacketState_->Enter(this);

	isServer_ = true;

	InitRacketOfPosAndRot();
}

void Racket::SetReceiverRacketState()
{
	pRacketState_ = make_unique<StandardState>();
	pRacketState_->Enter(this);

	isServer_ = false;

	InitRacketOfPosAndRot();
}

void Racket::SetPositionToBallPos()
{
	const Ball* pBall = (Ball*)FindObject("Ball");
	if (pBall != nullptr)
	{
		position_ = pBall->GetPosition();
	}
}