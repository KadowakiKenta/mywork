#pragma once
#include "PointGetChangeState.h"

class Racket;

//サーブの2バウンド目の状態を管理するクラス
class ServeBoundTwoState : public PointGetChangeState
{
	bool isRallyContinue_;

	void ShouldHitProcess(std::vector<Racket*>::iterator &it, Referee *thisptr);
	bool TwoBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it, const Ball *pBall);

public:
	ServeBoundTwoState();
	~ServeBoundTwoState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;

};