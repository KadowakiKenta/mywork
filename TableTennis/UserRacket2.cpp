#include "UserRacket2.h"
#include "PlayScene.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Ball.h"
#include "Referee.h"

UserRacket2::UserRacket2(IGameObject *parent):
	Racket(parent, "UserRacket2", COURT_RECEIVER)
{
}

UserRacket2::~UserRacket2()
{
}

bool UserRacket2::ButtonFrontMove()
{
	if (Input::IsKey(DIK_W))
	{
		return true;
	}

	return false;
}

bool UserRacket2::ButtonBackMove()
{
	if (Input::IsKey(DIK_S))
	{
		return true;
	}

	return false;
}

bool UserRacket2::ButtonLeftMove()
{
	if (Input::IsKey(DIK_A))
	{
		return true;
	}

	return false;
}

bool UserRacket2::ButtonRightMove()
{
	if (Input::IsKey(DIK_D))
	{
		return true;
	}

	return false;
}

bool UserRacket2::ButtonTopSpin()
{
	return false;
}

bool UserRacket2::ButtonBackSpin()
{
	//if (Input::IsKeyDown(DIK_DOWN))
	//{
	//	return true;
	//}

	return false;
}

bool UserRacket2::ButtonLeftSideSpin()
{
	if (Input::IsKeyDown(DIK_LEFT))
	{
		return true;
	}

	return false;
}

bool UserRacket2::ButtonRightSideSpin()
{
	if (Input::IsKeyDown(DIK_RIGHT))
	{
		return true;
	}

	return false;
}

bool UserRacket2::ButtonLobbing()
{
	if (Input::IsKeyDown(DIK_DOWN))
	{
		return true;
	}

	return false;
}

bool UserRacket2::ButtonSmash()
{
	if (Input::IsKeyDown(DIK_UP))
	{
		return true;
	}

	return false;
}
