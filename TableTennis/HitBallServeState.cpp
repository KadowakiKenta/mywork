#include "HitBallServeState.h"
#include "Engine/Model.h"
#include "Racket.h"
#include "StandardState.h"

const float FIRST_ANIMATION = 80.0f;
const float LAST_ANIMATION = 109.0f;

unique_ptr<RacketState> HitBallServeState::HandleInput(Racket *thisptr) const
{
	//アニメーションが再生しきったら
	float frame = Model::GetAnimFrame(thisptr->GetModelHandle());
	if (LAST_ANIMATION < frame + 5)
	{
		return make_unique<StandardState>();
	}

	return nullptr;
}

void HitBallServeState::Update(Racket *thisptr)
{
}

void HitBallServeState::Enter(Racket *thisptr)
{
	Model::SetAnimFrame(thisptr->GetModelHandle(), FIRST_ANIMATION, LAST_ANIMATION + ANIM_WHITESPACE_FRAME, ANIM_SPEED);

	thisptr->ServeBallHitSpeed();

	thisptr->SetPositionToBallPos();
}