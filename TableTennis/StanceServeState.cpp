#include "StanceServeState.h"
#include "Racket.h"
#include "Ball.h"
#include "Engine/Model.h"
#include "ThrowBallServeState.h"

const float FIRST_ANIMATION = 40.0f;
const float LAST_ANIMATION = 49.0f;

unique_ptr<RacketState> StanceServeState::HandleInput(Racket *thisptr) const
{
	if (thisptr->ButtonTopSpin() || thisptr->ButtonBackSpin() ||
		thisptr->ButtonRightSideSpin() || thisptr->ButtonLeftSideSpin() ||
		thisptr->ButtonLobbing() || thisptr->ButtonSmash())
	{
		return make_unique<ThrowBallServeState>();
	}

	return nullptr;
}

void StanceServeState::Update(Racket *thisptr)
{
	if (thisptr->ButtonFrontMove())
	{
		thisptr->ServeFrontMove();
	}
	if (thisptr->ButtonBackMove())
	{
		thisptr->ServeBackMove();
	}
	if (thisptr->ButtonLeftMove())
	{
		thisptr->ServeLeftMove();
	}
	if (thisptr->ButtonRightMove())
	{
		thisptr->ServeRightMove();
	}

	thisptr->FrontBallPos();
}

void StanceServeState::Enter(Racket *thisptr)
{
	Model::SetAnimFrame(thisptr->GetModelHandle(), FIRST_ANIMATION, LAST_ANIMATION, ANIM_SPEED);

	thisptr->BallCreate();
}