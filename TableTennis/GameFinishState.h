#pragma once
#include "RefereeState.h"

//試合終了の状態を管理するクラス
class GameFinishState : public RefereeState
{
	unsigned int countTimer_;
public:
	GameFinishState();
	~GameFinishState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;
};