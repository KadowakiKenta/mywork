#include <string>
#include "TitleText.h"
#include "Engine/Image.h"

const float MAX_ALPHA = 1.0f;
const float MIN_ALPHA = 0.0f;
const float SLOWLY_LIGHT = 0.01f;
const float SLOWLY_DARK = 0.03f;
const float FADE_OUT = 0.02f;

TitleText::TitleText(IGameObject *parent):
	IGameObject(parent, "TitleText"),
	lightAlpha_(MAX_ALPHA),
	flashFlag_(false),
	darkAlpha_(MAX_ALPHA),
	state_(STATE_TITLE_LOGO_DRAW)
{
	for (int i = 0; i < PICT_MAX; i++)
	{
		hPict_[i] = ASSERT_ERROR;
	}
}

TitleText::~TitleText()
{
}

void TitleText::Initialize()
{
	const string fileName[]
	{
		"TitleLogo",
		"PressSpaceButton"
	};

	for (int i = 0; i < PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("data/Image/Title/" + fileName[i] + ".png");
		assert(hPict_[i] > ASSERT_ERROR);
	}
}

void TitleText::Update()
{
	PushPromptLogoOfFadeInOutProcess();

	PushSpaceProcess();
}

void TitleText::PushPromptLogoOfFadeInOutProcess()
{
	switch (state_)
	{
	case STATE_TITLE_LOGO_DRAW:

		if (lightAlpha_ >= MAX_ALPHA)
		{
			flashFlag_ = false;
		}
		else if (lightAlpha_ <= MIN_ALPHA)
		{
			flashFlag_ = true;
		}

		if (flashFlag_)
		{
			lightAlpha_ += SLOWLY_LIGHT;
		}
		else
		{
			lightAlpha_ -= SLOWLY_DARK;
		}
		break;

	case STATE_FADE_OUT:

		darkAlpha_ -= FADE_OUT;

		if (darkAlpha_ < MIN_ALPHA)
		{
			SceneManager* pSceneManager = (SceneManager*)FindSceneManager();
			pSceneManager->ChangeSceneID(SCENE_ID_OPERATION_EXPLAIN);
		}
		break;
	}
}

void TitleText::PushSpaceProcess()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		switch (state_)
		{
		case STATE_TITLE_LOGO_DRAW:

			state_ = STATE_FADE_OUT;
			break;

		case STATE_FADE_OUT:

			SceneManager* pSceneManager = (SceneManager*)FindSceneManager();
			pSceneManager->ChangeSceneID(SCENE_ID_OPERATION_EXPLAIN);
			break;
		}
	}
}

void TitleText::Draw()
{
	switch (state_)
	{
	case STATE_TITLE_LOGO_DRAW:
		
		Image::SetMatrix(hPict_[PICT_TITLE], worldMatrix_);
		Image::Draw(hPict_[PICT_TITLE]);

		Image::SetMatrix(hPict_[PICT_START], worldMatrix_);
		Image::Draw(hPict_[PICT_START], lightAlpha_);
		break;

	case STATE_FADE_OUT:

		Image::SetMatrix(hPict_[PICT_TITLE], worldMatrix_);
		Image::Draw(hPict_[PICT_TITLE], darkAlpha_);

		Image::SetMatrix(hPict_[PICT_START], worldMatrix_);
		Image::Draw(hPict_[PICT_START], darkAlpha_);
		break;
	}
}

void TitleText::Release()
{
}