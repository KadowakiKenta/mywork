#pragma once
#include "RefereeState.h"

//ポイントゲットの状態を管理するクラス
class PointGetState : public RefereeState
{
	unsigned int countTimer_;
public:
	PointGetState();
	~PointGetState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;
};