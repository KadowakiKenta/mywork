#pragma once
#include <memory>
#include <vector>

using namespace std;

namespace std
{
	template < class T, class Allocator > class vector;
}
#define VECTOR_FWD(x_type)  vector<x_type, std::allocator<x_type>>

class Ball;
class Referee;
enum AFTER_SCORE_STATE;

//審判の状態を管理するクラス
class RefereeState
{
public:
	virtual ~RefereeState() {};

	virtual unique_ptr<RefereeState> HandleInput(Referee *thisptr) const = 0;

	virtual void Update(Referee *thisptr) = 0;

	virtual void Enter(Referee *thisptr) = 0;
};