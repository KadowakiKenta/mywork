#include "ServeBoundZeroState.h"
#include <vector>
#include "ServeBoundOneState.h"
#include "Referee.h"
#include "Racket.h"
#include "Ball.h"

unique_ptr<RefereeState> ServeBoundZeroState::HandleInput(Referee *thisptr) const
{
	const Ball* pBall = thisptr->GetBallPointer();
	if (pBall != nullptr)
	{
		if (pBall->GetIsBound())
		{
			return make_unique<ServeBoundOneState>();
		}
	}
	else
	{
		return PointGetChangeState::HandleInput(thisptr);
	}

	return nullptr;
}

void ServeBoundZeroState::Update(Referee *thisptr)
{
	std::vector<Racket*> pRacketArray = thisptr->GetRacketArray();

	//打ってはいけない場面なのに、打ってしまった人
	for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
	{
		if ((*it)->GetIsBallHitNowFrame())
		{
			if ((*it)->GetMyCourt() == COURT_SERVER)
			{
				thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_RECEIVER_COURT);
			}
			else if ((*it)->GetMyCourt() == COURT_RECEIVER)
			{
				thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_SERVER_COURT);
			}
		}
	}
	
	//バウンド判定
	const Ball* pBall = thisptr->GetBallPointer();
	if ((pBall != nullptr) && (pBall->GetIsBound()))
	{
		for (auto it = pRacketArray.begin(); it < pRacketArray.end(); it++)
		{
			if (ZeroBoundPointProcess(thisptr, it, pBall))
			{
				break;
			}
		}
	}
}

void ServeBoundZeroState::Enter(Referee *thisptr)
{
	thisptr->SetRefereeState(STATE_SERVE_BOUND_ZERO);
}

bool ServeBoundZeroState::ZeroBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it, const Ball *pBall)
{
	if ((*it)->GetIsServer())
	{
		if (((*it)->GetMyCourt() == COURT_SERVER) &&
			(pBall->GetCollisionBall() == COLLISION_BALL_RECEIVER_COURT || pBall->GetCollisionBall() == COLLISION_BALL_GROUND))
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_RECEIVER_COURT);
		}
		else if (((*it)->GetMyCourt() == COURT_RECEIVER) && 
			(pBall->GetCollisionBall() == COLLISION_BALL_SERVER_COURT || pBall->GetCollisionBall() == COLLISION_BALL_GROUND))
		{
			thisptr->ScorePlayerToProcess(SCORE_PLAYER_TO_SERVER_COURT);
		}

		return true;
	}

	return false;
}