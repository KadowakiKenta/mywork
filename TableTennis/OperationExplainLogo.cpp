#include <string>
#include <time.h>
#include "OperationExplainLogo.h"
#include "Engine/Image.h"

const float MAX_ALPHA = 1.0f;
const float MIN_ALPHA = 0.0f;
const float FADE_IN = 0.03f;
const float PROVISION_TIME = 2000.0f;

OperationExplainLogo::OperationExplainLogo(IGameObject *parent) : IGameObject(parent, "OperationExplainLogo"),
	logoOperationExplainAlpha_(MIN_ALPHA),
	logoPushSpaceButtonAlpha_(MIN_ALPHA),
	state_(STATE_OPERATION_EXPLAIN_DRAW),
	timeRecord_(NULL),
	timeResetFlag_(true)
{
	for (int i = 0; i < PICT_MAX; i++)
	{
		hPict_[i] = ASSERT_ERROR;
	}
}

OperationExplainLogo::~OperationExplainLogo()
{
}

void OperationExplainLogo::Initialize()
{
	const string fileName[]
	{
		"OperationExplain",
		"SpacePutPlay",
		"NowLoading"
	};

	for (int i = 0; i < PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("data/Image/OperationExplain/" + fileName[i] + ".png");
		assert(hPict_[i] > ASSERT_ERROR);
	}
}

void OperationExplainLogo::Update()
{
	AlgorithmProcess();

	SpaceKeyPushProcess();
}

void OperationExplainLogo::AlgorithmProcess()
{
	switch (state_)
	{
	case STATE_OPERATION_EXPLAIN_DRAW:

		logoOperationExplainAlpha_ += FADE_IN;

		if ((logoOperationExplainAlpha_ > MAX_ALPHA) && (timeResetFlag_ == true))
		{
			timeRecord_ = clock();

			timeResetFlag_ = false;
		}
		else if ((clock() - timeRecord_  > PROVISION_TIME) && (timeResetFlag_ == false))
		{
			state_ = STATE_SPACE_PUT_PLAY_DRAW;
		}
		break;

	case STATE_SPACE_PUT_PLAY_DRAW:

		logoPushSpaceButtonAlpha_ += FADE_IN;
		break;

	case STATE_NOW_LOADING_DRAW:

		SceneManager* pSceneManager = (SceneManager*)FindSceneManager();
		pSceneManager->ChangeSceneID(SCENE_ID_PLAY);
		break;
	}
}

void OperationExplainLogo::SpaceKeyPushProcess()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		switch (state_)
		{
		case STATE_OPERATION_EXPLAIN_DRAW:

			if (logoOperationExplainAlpha_  < MAX_ALPHA)
			{
				logoOperationExplainAlpha_ = MAX_ALPHA;
			}
			else
			{
				state_ = STATE_SPACE_PUT_PLAY_DRAW;
			}
			break;

		case STATE_SPACE_PUT_PLAY_DRAW:

			if (logoPushSpaceButtonAlpha_ < MAX_ALPHA)
			{
				logoPushSpaceButtonAlpha_ = MAX_ALPHA;
			}
			else
			{
				state_ = STATE_NOW_LOADING_DRAW;
			}
			break;
		}
	}
}

void OperationExplainLogo::Draw()
{
	switch (state_)
	{
	case STATE_OPERATION_EXPLAIN_DRAW:

		Image::SetMatrix(hPict_[PICT_OPERATION_EXPLAIN], worldMatrix_);
		Image::Draw(hPict_[PICT_OPERATION_EXPLAIN], logoOperationExplainAlpha_);
		break;

	case STATE_SPACE_PUT_PLAY_DRAW:

		Image::SetMatrix(hPict_[PICT_OPERATION_EXPLAIN], worldMatrix_);
		Image::Draw(hPict_[PICT_OPERATION_EXPLAIN]);

		Image::SetMatrix(hPict_[PICT_SPACE_PUT_PLAY], worldMatrix_);
		Image::Draw(hPict_[PICT_SPACE_PUT_PLAY], logoPushSpaceButtonAlpha_);
		break;

	case STATE_NOW_LOADING_DRAW:

		Image::SetMatrix(hPict_[PICT_OPERATION_EXPLAIN], worldMatrix_);
		Image::Draw(hPict_[PICT_OPERATION_EXPLAIN]);

		Image::SetMatrix(hPict_[PICT_NOW_LOADING], worldMatrix_);
		Image::Draw(hPict_[PICT_NOW_LOADING]);
		break;
	}
}

//�J��
void OperationExplainLogo::Release()
{
}