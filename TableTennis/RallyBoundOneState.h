#pragma once
#include "PointGetChangeState.h"

class Racket;

//ラリーの1バウンド目の状態を管理するクラス
class RallyBoundOneState : public PointGetChangeState
{
	bool isRallyContinue_;

	void ShouldHitProcess(std::vector<Racket*>::iterator &it, Referee *thisptr);
	bool TwoBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it, const Ball *pBall);

public:
	RallyBoundOneState();
	~RallyBoundOneState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;

};