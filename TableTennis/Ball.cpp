#include <string>
#include "Ball.h"
#include "Engine/Model.h"
#include "UserRacket.h"
#include "AiRacket.h"
#include "Referee.h"
#include "BackGround3D.h"
#include "TableTennisTable.h"

enum HIT_RAY_CAST
{
	HIT_TABLE_TENNIS_TABLE,
	HIT_BACK_GROUND_3D,
	HIT_MAX
};

const float BALL_MASS_KILOGRAM = 0.0027f;

const float BALL_SHADOW_SCALE = 50.0f;

const float REBOUND_FACTOR_BALL = 0.75f;
const float FRICTION_FACTOR_BALL = 1.4f;

const float G = 4.9f;

Ball::Ball(IGameObject *parent):
	IGameObject(parent, "Ball"),
	afterBoundTime_(chrono::system_clock::now()),
	afterFallTime_(chrono::system_clock::now()),
	nowTime_(chrono::system_clock::now()),
	elapsedTime_(0.0f),
	gravity_(0.0f),
	reboundSpeedY_(0.0f),
	isCollision_(true),
	isFallReset_(true),
	isBound_(false),
	beforePos_(D3DXVECTOR3(0, 0, 0)),
	moveBallToNowFrame_(D3DXVECTOR3(0, 0, 0)),
	ballSpeed_(D3DXVECTOR3(0, 0, 0)),
	BallCollider_(nullptr),
	frameOnlyCollisionBall_(COLLISION_BALL_NO),
	pEffect_(nullptr)
{
	for (int i = 0; i < MODEL_MAX; i++)
	{
		hModel_[i] = ASSERT_ERROR;
	}
	D3DXMatrixIdentity(&shadowMatrix_);
}

Ball::~Ball()
{
}

void Ball::Initialize()
{
	const string fileName[] =
	{
		"Ball",
		"BallShadow"
	};

	for (int i = 0; i < MODEL_MAX; i++)
	{
		hModel_[i] = Model::Load("data/Model/" + fileName[i] + ".fbx");
		assert(hModel_[i] > ASSERT_ERROR);
	}

	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "CommonShader.hlsl", NULL, NULL, D3DXSHADER_DEBUG,
		NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}
	assert(pEffect_ != nullptr);

	BallCollider_ = new SphereCollider("BallCollider", D3DXVECTOR3(0, 0, 0), 2.0f);
	AddCollider(BallCollider_);

	afterBoundTime_ = chrono::system_clock::now();
}

void Ball::Update()
{
	//OnCollision()内でtrueにしている（そのフレーム内だけtrueにしたいから）
	isBound_ = false;

	CanKillMe();

	//Updateの後にCollision判定する
	frameOnlyCollisionBall_ = COLLISION_BALL_NO;

	if (isGravity_)
	{
		//バウンドした後の時間で計算
		GravityClac(afterBoundTime_);

		MoveBall();
		
		CanInFall();
	}
	else
	{
		afterBoundTime_ = chrono::system_clock::now();
		ballSpeed_ = D3DXVECTOR3(0, 0, 0);

		//falseのままだと重力が働かないので、とりあえずtrue
		isGravity_ = true;
	}

	PosOfShadow();
}

void Ball::Draw()
{
	Model::SetMatrix(hModel_[MODEL_BALL], worldMatrix_);
	Model::Draw(hModel_[MODEL_BALL], this, pEffect_);

	Model::SetMatrix(hModel_[MODEL_SHADOW], shadowMatrix_);
	Model::Draw(hModel_[MODEL_SHADOW], this);
}

void Ball::Release()
{
}

void Ball::CanKillMe()
{
	Referee* pReferee = (Referee*)FindObject("Referee");
	if (pReferee != nullptr)
	{
		if (pReferee->GetBallKillFlag())
		{
			KillMe();
		}
	}
}

void Ball::PosOfShadow()
{
	TableTennisTable* pTableTennisTable = (TableTennisTable*)FindObject("TableTennisTable");
	assert(pTableTennisTable != nullptr);

	BackGround3D* pBackGround3D = (BackGround3D*)FindObject("BackGround3D");
	assert(pBackGround3D != nullptr);

	int tableTennisTableModelHandle = pTableTennisTable->GetModelHandle();
	int backGround3DModelHandle = pBackGround3D->GetModelHandle();

	RayCastData data[HIT_MAX];
	for (int i = 0; i < HIT_MAX; i++)
	{
		data[i].start = position_;
		data[i].dir = D3DXVECTOR3(0, -1, 0);
	}
	
	Model::RayCast(tableTennisTableModelHandle, &data[HIT_TABLE_TENNIS_TABLE]);
	Model::RayCast(backGround3DModelHandle, &data[HIT_BACK_GROUND_3D]);

	if (data[HIT_TABLE_TENNIS_TABLE].hit)
	{
		D3DXMATRIX scale, position;

		D3DXMatrixScaling(&scale, scale_.x + (data[HIT_TABLE_TENNIS_TABLE].dist / BALL_SHADOW_SCALE),
			scale_.y, scale_.z + (data[HIT_TABLE_TENNIS_TABLE].dist / BALL_SHADOW_SCALE));

		D3DXMatrixTranslation(&position, position_.x, 
			position_.y - data[HIT_TABLE_TENNIS_TABLE].dist, position_.z);

		shadowMatrix_ = scale * position;
	}
	else if (data[HIT_BACK_GROUND_3D].hit)
	{
		D3DXMATRIX scale, position;

		D3DXMatrixScaling(&shadowMatrix_, scale_.x + (data[HIT_BACK_GROUND_3D].dist / BALL_SHADOW_SCALE),
			scale_.y, scale_.z + (data[HIT_BACK_GROUND_3D].dist / BALL_SHADOW_SCALE));

		D3DXMatrixTranslation(&shadowMatrix_, position_.x,
			position_.y - data[HIT_BACK_GROUND_3D].dist, position_.z);

		shadowMatrix_ = scale * position;
	}
	else
	{
		D3DXMatrixTranslation(&shadowMatrix_, -9999.9f, -9999.9f, -9999.9f);
	}

}

void Ball::MoveBall()
{
	//落下している時の判定に使われる
	beforePos_ = position_;

	position_.x += ballSpeed_.x;
	position_.y += -gravity_ + ballSpeed_.y + reboundSpeedY_;
	position_.z += ballSpeed_.z;

	moveBallToNowFrame_ = position_ - beforePos_;
}

void Ball::GravityClac(const chrono::system_clock::time_point beforeTime)
{
	nowTime_ = chrono::system_clock::now();
	elapsedTime_ = (float)chrono::duration_cast<chrono::nanoseconds>(nowTime_ - beforeTime).count();
	elapsedTime_ /= 1000000000;

	gravity_ = G * elapsedTime_;
}

void Ball::CanInFall()
{
	if (position_.y - beforePos_.y < 0 && isFallReset_)
	{
		afterFallTime_ = chrono::system_clock::now();

		//1度バウンドした後に落下してるから
		//地面に衝突してもおかしくない…はず
		isCollision_ = true;

		isFallReset_ = false;
	}
}

void Ball::OnCollision(const IGameObject *pTarget)
{
	TableTennisTableOnCollision(pTarget);
	RacketOnCollision(pTarget);
}

void Ball::TableTennisTableOnCollision(const IGameObject *pTarget)
{
	if ( (pTarget->GetName() == "TableTennisTable") && (isCollision_) )
	{
		if (BallCollider_->GetHitCollider()->GetName() == "GroundCollider")
		{
			frameOnlyCollisionBall_ = COLLISION_BALL_GROUND;
		}
		else
		{
			if (BallCollider_->GetHitCollider()->GetName() == "ServerCourtCollider")
			{
				frameOnlyCollisionBall_ = COLLISION_BALL_SERVER_COURT;

			}
			else if (BallCollider_->GetHitCollider()->GetName() == "ReceiverCourtCollider")
			{
				frameOnlyCollisionBall_ = COLLISION_BALL_RECEIVER_COURT;
			}
		}

		//正確にバウンドさせるため、落下後の時間で計算
		GravityClac(afterFallTime_);

		//下方向にボールが移動している時、きちんとバウンドさせたい
		if (ballSpeed_.y < 0.0f)
		{
			reboundSpeedY_ = (gravity_ + (ballSpeed_.y * -1)) * REBOUND_FACTOR_BALL;
		}
		else
		{
			reboundSpeedY_ = gravity_ * REBOUND_FACTOR_BALL;
		}
		ballSpeed_.y = 0.0f;

		//二回目以降の衝突はさせない
		//そうしないと稀に二回地面の衝突してしまうから
		isCollision_ = false;

		//落下中一度のみ処理をしたい為true
		isFallReset_ = true;

		afterBoundTime_ = chrono::system_clock::now();
		afterFallTime_ = chrono::system_clock::now();

		isBound_ = true;
	}
}

void Ball::RacketOnCollision(const IGameObject *pTarget)
{
	//ここいらないかもしれない
	//でも必要かもしれない
	if (pTarget->GetName() == "UserRacket")
	{
		frameOnlyCollisionBall_ = COLLISION_BALL_USER_RACKET;
	}
	if (pTarget->GetName() == "AiRacket")
	{
		frameOnlyCollisionBall_ = COLLISION_BALL_AI_RACKET;
	}
}

void Ball::BallReboundDirection(const D3DXVECTOR3 racketVector)
{
	D3DXVECTOR3 ballViewPointRacket;

	//ラケットから見たボールの速度
	ballViewPointRacket.x = moveBallToNowFrame_.x - racketVector.x;
	ballViewPointRacket.y = moveBallToNowFrame_.y - racketVector.y;
	ballViewPointRacket.z = moveBallToNowFrame_.z - racketVector.z;

	//反射（Z軸のみ）
	ballViewPointRacket.x *= REBOUND_FACTOR_BALL;
	ballViewPointRacket.y *= REBOUND_FACTOR_BALL;
	ballViewPointRacket.z = -ballViewPointRacket.z * REBOUND_FACTOR_BALL;

	ballViewPointRacket *= BALL_MASS_KILOGRAM;
	
	ballSpeed_ = racketVector + ballViewPointRacket;

	//Yが落下してる際にリセットできるように
	//リセットしないと思ったように重力が働かなくなる
	isFallReset_ = true;
}
