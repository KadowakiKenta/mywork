#pragma once
#include "Engine/IGameObject.h"

//3D�̔w�i���Ǘ�����N���X
class BackGround3D : public IGameObject
{
	int hModel_;

public:
	BackGround3D(IGameObject *parent);
	~BackGround3D();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;

	const int GetModelHandle() const
	{
		return hModel_;
	};
};