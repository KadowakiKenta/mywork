#pragma once
#include "RacketState.h"

//スマッシュの状態を管理するクラス
class SmashState : public RacketState
{
public:
	~SmashState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};