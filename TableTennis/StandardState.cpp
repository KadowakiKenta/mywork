#include "StandardState.h"
#include "Engine/Model.h"
#include "TopSpinState.h"
#include "BackSpinState.h"
#include "LeftSideSpinState.h"
#include "RightSideSpinState.h"
#include "LobbingState.h"
#include "SmashState.h"
#include "Racket.h"

const float FIRST_ANIMATION = 0.0f;
const float LAST_ANIMATION = 29.0f;

unique_ptr<RacketState> StandardState::HandleInput(Racket *thisptr) const
{
	if (thisptr->ButtonTopSpin())
	{
		//return make_unique<TopSpinState>();
	}
	else if (thisptr->ButtonBackSpin())
	{
		//return make_unique<BackSpinState>();
	}
	else if (thisptr->ButtonLeftSideSpin())
	{
		//return make_unique<LeftSideSpinState>();
	}
	else if (thisptr->ButtonRightSideSpin())
	{
		//return make_unique<RightSideSpinState>();
	}
	else if (thisptr->ButtonLobbing())
	{
		return make_unique<LobbingState>();
	}
	else if (thisptr->ButtonSmash())
	{
		return make_unique<SmashState>();
	}

	return nullptr;
}

void StandardState::Update(Racket *thisptr)
{
	if (thisptr->ButtonFrontMove())
	{
		thisptr->RallyFrontMove();
	}
	if (thisptr->ButtonBackMove())
	{
		thisptr->RallyBackMove();
	}
	if (thisptr->ButtonLeftMove())
	{
		thisptr->RallyLeftMove();
	}
	if (thisptr->ButtonRightMove())
	{
		thisptr->RallyRightMove();
	}

	thisptr->SetSavePosition();
}

void StandardState::Enter(Racket *thisptr)
{
	Model::SetAnimFrame(thisptr->GetModelHandle(), FIRST_ANIMATION, LAST_ANIMATION, ANIM_SPEED);

	thisptr->SetLoadPosition();
}
