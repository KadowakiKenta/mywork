#pragma once
#include "RacketState.h"

//サーブでボールを投げる際の状態を管理するクラス
class ThrowBallServeState : public RacketState
{
	bool ballHit_;	//ボールを打ったらtrue
public:
	ThrowBallServeState() : ballHit_(false) {};
	~ThrowBallServeState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};