#pragma once
#include "RacketState.h"

//右回転の状態を管理するクラス
class RightSideSpinState : public RacketState
{
public:
	~RightSideSpinState() override {};
	unique_ptr<RacketState> HandleInput(Racket *thisptr) const override;
	void Update(Racket *thisptr) override;
	void Enter(Racket *thisptr) override;
};