#pragma once
#include <memory>
#include "Engine/IGameObject.h"

class RacketState;
class Ball;

enum COURT
{
	COURT_SERVER = 1,
	COURT_RECEIVER = -1
};

//ラケットを管理するクラス
class Racket : public IGameObject
{
	int hModel_;

	float hitLeftRightVec_;
	float hitFrontBackVec_;

	bool isBallHit_;
	bool isBallHitFrameOnly_;
	bool isServer_;
	
	COURT myCourt_;

	std::unique_ptr<RacketState> pRacketState_;

	Racket* pEnemyRacket_;

	LPD3DXEFFECT pEffect_;

	D3DXVECTOR3 savePosHitBallBefore_;
	D3DXVECTOR3 swingPow_;

	void HandleInput();

public:
	Racket(IGameObject *parent, const COURT courtPos);
	Racket(IGameObject *parent, const std::string name, const COURT courtPos);
	virtual ~Racket();
	void Initialize() override;
	void InitRacketOfPosAndRot();
	void Update() override;
	void IHitBallProcess();
	void Draw() override;
	void Release() override;
	void OnCollision(const IGameObject *pTarget) override;

	//trueなら、そのボタンがオンという事を示す
	virtual bool ButtonFrontMove() = 0;
	virtual bool ButtonBackMove() = 0; 
	virtual bool ButtonLeftMove() = 0;
	virtual bool ButtonRightMove() = 0;
	virtual bool ButtonTopSpin() = 0;
	virtual bool ButtonBackSpin() = 0;
	virtual bool ButtonLeftSideSpin() = 0;
	virtual bool ButtonRightSideSpin() = 0;
	virtual bool ButtonLobbing() = 0;
	virtual bool ButtonSmash() = 0;

	void HitDivide();

	void ServeFrontMove();
	void ServeBackMove();
	void ServeLeftMove();
	void ServeRightMove();

	void ServeBallHitSpeed();

	void RallyFrontMove();
	void RallyBackMove();
	void RallyLeftMove();
	void RallyRightMove();

	void Lobbing();
	void Smash();

	void BallCreate();

	void FrontBallPos();

	void BallServe();

	const int GetModelHandle() const
	{
		return hModel_;
	}

	const D3DXVECTOR3 GetBallVec() const
	{
		return swingPow_;
	}

	const COURT GetMyCourt() const
	{
		return myCourt_;
	}

	const bool GetIsBallHit() const
	{
		return isBallHit_;
	}

	const bool GetIsBallHitNowFrame() const
	{
		return isBallHitFrameOnly_;
	}

	const bool GetIsServer() const
	{
		return isServer_;
	}

	//unique_ptrのゲッター（生ポインタ）
	const RacketState& GetRacketState() const
	{
		return *pRacketState_;
	}

	//自分とは他のラケットを打っていない判定にする
	void SetIsBallHitToFalse()
	{
		isBallHit_ = false;
	}

	//審判側で得点後に行われる
	void SetServerRacketState();
	void SetReceiverRacketState();

	//ボールの位置へ移動
	void SetPositionToBallPos();

	//ボールを打ったあとに戻れるようにする
	void SetSavePosition()
	{
		savePosHitBallBefore_ = position_;
	}

	//ボールを打ち終わったあとに代入
	void SetLoadPosition()
	{
		position_ = savePosHitBallBefore_;
	}
};