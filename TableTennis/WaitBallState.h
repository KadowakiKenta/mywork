#pragma once
#include "PointGetChangeState.h"

class Racket;

//ボール待ちの状態を管理するクラス
class WaitBallState : public PointGetChangeState
{
	bool WaitBallBoundPointProcess(Referee *thisptr, const vector<Racket*>::iterator &it);

public:
	~WaitBallState() override {};
	unique_ptr<RefereeState> HandleInput(Referee *thisptr) const override;
	void Update(Referee *thisptr) override;
	void Enter(Referee *thisptr) override;
};