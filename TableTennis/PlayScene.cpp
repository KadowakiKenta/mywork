#include "PlayScene.h"
#include "TableTennisTable.h"
#include "Ball.h"
#include "Engine/Camera.h"
#include "UserRacket.h"
#include "AiRacket.h"
#include "Referee.h"
#include "BackGround3D.h"

const float CAMERA_POS_X = 0.0f;
const float CAMERA_POS_Y = 80.0f;
const float CAMERA_POS_Z = -150.0f;

PlayScene::PlayScene(IGameObject *parent):
	IGameObject(parent, "PlayScene"),
	pCamera_(nullptr)
{
}

void PlayScene::Initialize()
{
	CreateGameObject<BackGround3D>(this);
	CreateGameObject<TableTennisTable>(this);
	CreateGameObject<Referee>(this);

	pCamera_ = CreateGameObject<Camera>(this);
	pCamera_->SetPosition(D3DXVECTOR3(CAMERA_POS_X, CAMERA_POS_Y, CAMERA_POS_Z));
	pCamera_->SetTarget(D3DXVECTOR3(0.0f, 10.0f, 0.0f));
}

void PlayScene::Update()
{
}

void PlayScene::Draw()
{
}

void PlayScene::Release()
{
}