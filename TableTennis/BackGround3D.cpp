#include <string>
#include "BackGround3D.h"
#include "Engine/Model.h"

BackGround3D::BackGround3D(IGameObject *parent):
	IGameObject(parent, "BackGround3D"),
	hModel_(ASSERT_ERROR)
{
}

BackGround3D::~BackGround3D()
{
}

void BackGround3D::Initialize()
{
	hModel_ = Model::Load("data/Model/BackGround.fbx");
	assert(hModel_ > ASSERT_ERROR);
}

void BackGround3D::Update()
{
}

void BackGround3D::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_, this);
}

void BackGround3D::Release()
{
}