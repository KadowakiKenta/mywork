#pragma once
#include "Engine/IGameObject.h"

//タイトルのテキストを管理するクラス
class TitleText : public IGameObject
{
	enum PICTURE
	{
		PICT_TITLE,
		PICT_START,
		PICT_MAX
	};
	int hPict_[PICT_MAX];

	enum STATE
	{
		STATE_TITLE_LOGO_DRAW,
		STATE_FADE_OUT
	} state_;

	float lightAlpha_;
	float darkAlpha_;
	bool flashFlag_;

	void PushPromptLogoOfFadeInOutProcess();
	void PushSpaceProcess();

public:
	TitleText(IGameObject *parent);
	~TitleText();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};