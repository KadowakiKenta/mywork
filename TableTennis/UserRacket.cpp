#include "UserRacket.h"
#include "PlayScene.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Ball.h"
#include "Referee.h"

UserRacket::UserRacket(IGameObject *parent):
	Racket(parent, "UserRacket", COURT_SERVER)
{
}

UserRacket::~UserRacket()
{
}

bool UserRacket::ButtonFrontMove()
{
	if (Input::IsKey(DIK_W))
	{
		return true;
	}

	return false;
}

bool UserRacket::ButtonBackMove()
{
	if (Input::IsKey(DIK_S))
	{
		return true;
	}

	return false;
}

bool UserRacket::ButtonLeftMove()
{
	if (Input::IsKey(DIK_A))
	{
		return true;
	}

	return false;
}

bool UserRacket::ButtonRightMove()
{
	if (Input::IsKey(DIK_D))
	{
		return true;
	}

	return false;
}

bool UserRacket::ButtonTopSpin()
{
	return false;
}

bool UserRacket::ButtonBackSpin()
{
	//if (Input::IsKeyDown(DIK_DOWN))
	//{
	//	return true;
	//}

	return false;
}

bool UserRacket::ButtonLeftSideSpin()
{
	if (Input::IsKeyDown(DIK_LEFT))
	{
		return true;
	}

	return false;
}

bool UserRacket::ButtonRightSideSpin()
{
	if (Input::IsKeyDown(DIK_RIGHT))
	{
		return true;
	}

	return false;
}

bool UserRacket::ButtonLobbing()
{
	if (Input::IsKeyDown(DIK_DOWN))
	{
		return true;
	}

	return false;
}

bool UserRacket::ButtonSmash()
{
	if (Input::IsKeyDown(DIK_UP))
	{
		return true;
	}

	return false;
}
