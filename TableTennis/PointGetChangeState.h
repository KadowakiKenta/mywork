#pragma once
#include "RefereeState.h"

//ポイントを取った時の状態を管理するクラス
class PointGetChangeState : public RefereeState
{
public:
	virtual ~PointGetChangeState() {};
	virtual unique_ptr<RefereeState> HandleInput(Referee *thisptr) const;
	virtual void Update(Referee *thisptr) = 0;
	virtual void Enter(Referee *thisptr) = 0;
};